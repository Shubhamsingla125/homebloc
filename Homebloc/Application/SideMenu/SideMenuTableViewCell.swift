//
//  SideMenuTableViewCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 31/01/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imgBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(indexpath:IndexPath){
        if let sideMenuData = SideMenuSection(rawValue: indexpath.row){
            self.titleLbl.text = sideMenuData.getTitleAndImage()
        }
    }

}
