//
//  ForgotPassword.swift
//  BaseProject
//
//  Created by TpSingh on 17/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

class ForgotPasswordController: DIBaseController {
  
  //MARK:IBOutlets
  @IBOutlet weak var textfieldEmail: UITextField!
  @IBOutlet weak var textfieldOTP: UITextField!
  @IBOutlet weak var submitButton: UIButton!
  @IBOutlet weak var textfieldPassword: UITextField!
  
  
  //MARK:- ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  //MARK:- IBActions
  @IBAction func submitButtonTapped(_ sender: UIButton) {
    if isEmailValid(){      
      var forgotPasswordKey = ForgotPasswordKey()
      forgotPasswordKey.email = textfieldEmail.text ?? ""
      print(forgotPasswordKey)
      showLoader()
      
      DIWebLayerUserAPI().forgotPassword(parameters: forgotPasswordKey.getDictionary(), success: { (responseMsg) in
        DILog.print(items: responseMsg)
        self.hideLoader()
               
        self.showAlert(message: responseMsg,okCall: {
         _ = self.navigationController?.popToRootViewController(animated: true)
          
        })
        
      }, failure: { (error) in
        DILog.print(items: error)
        self.hideLoader()

      })
      
    }
  }
  
  @IBAction func resentOTPButtonTapped(_ sender: UIBarButtonItem) {
    
  }
  
  @IBAction func showPasswordButtonTapped(_ sender: UIButton) {
    if textfieldPassword.isSecureTextEntry{
      textfieldPassword.isSecureTextEntry = false
    }else{
      textfieldPassword.isSecureTextEntry = true
    }
  }

  //MARK:- Private Methods
  fileprivate func isEmailValid() -> Bool {
    var message : String?
    
    if (textfieldEmail.text?.isBlank)! {
      message = AppMessages.Email.emptyEmail
    }else if !(textfieldEmail.text?.isValidEmail)! {
      message = AppMessages.Email.invalidEmail
    }
    
    if message != nil {
      self.showAlert( message: message)
      return false
    }

    
    return true
  }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
