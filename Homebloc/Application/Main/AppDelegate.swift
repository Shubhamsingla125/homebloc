//
//  AppDelegate.swift
//  BaseProject
//
//  Created by narinder on 28/02/17.
//  varyright © 2017 openkey. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FBSDKCoreKit
import CoreData
import Google
import GoogleSignIn
import SideMenu
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        STPPaymentConfiguration.shared().publishableKey = "pk_test_yZeDuiNZzVbXxpuFnNjb1inW"
        FBSDKApplicationDelegate.sharedInstance().application(application,didFinishLaunchingWithOptions:launchOptions)
        // Override point for customization after application launch.
        
        //For not printing logs
        //DILog.enableLog = false
        //print(appName)
        
        //    let branch: Branch = Branch.getInstance()
        //    branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {parmas, error in
        //      if error == nil{
        //        print("params: ", parmas?.description as AnyObject)
        //      }
        //    })
        //navigateToCity()
        //Check user session
        if UserManager.isUserLoggedIn() {
            if let user = UserManager.getCurrentUser() {
                User.sharedInstance = user 
            }
            if Password.lockSequence != nil || Password.isUserLogin != nil{
                navigateToPattern()
            }
            //Navigate to dashboard.
        } else {
            //Navigate to login screen.
            navigateToSplash()
            //        navigateToCity()
        }
        
        AppManager.shared.isUpgradeAvailable { (status, url, error) in
            switch status {
            case .available : DILog.print(items: "New Update is available for application")
                break
            case .none :	DILog.print(items: "There is no update available for your application")
                break
            case .error :DILog.print(items: "Update Error occured >>>>>>>>>>>", error?.message ?? "Something happen wrong")
                break
            }
        }
        
        //IQKeyboard Manager Implementation
        self.addKeyboardManager()
        FIRApp.configure()
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAnimationFadeStrength = 0.5
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 40
        return true
    }
  
  func applicationWillResignActive(_ application: UIApplication) {
    FBSDKAppEvents.activateApp()
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    return true
  }
  

  // Respond to Universal Links
  func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
    // pass the url to the handle deep link call
    //Branch.getInstance().continue(userActivity)
    
    return true
  }
  
  // Respond to URI scheme links
  func application(_ application: UIApplication,
                   open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool{
    print("Back")
//    Branch.getInstance().application(application,
//                                     open: url,
//                                     options:options
//    )
//
//    
    
    if #available(iOS 9.0, *) {
        let facebookDidhandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
  
      let googleInDidHandle = GIDSignIn.sharedInstance().handle(url,
                                                                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                annotation: options[UIApplicationOpenURLOptionsKey.annotation] as? String)
      return facebookDidhandle || googleInDidHandle
    }
    return true
  }
    
    
  func navigateToCity() {
    let profileVC :HotelViewController = UIStoryboard(storyboard: .hotel).initVC()
    setNavigationToRoot(viewContoller: profileVC)
  }
    func navigateToPattern() {
        let patternVC :PatternViewController = UIStoryboard(storyboard: .main).initVC()
        setNavigationToRoot(viewContoller: patternVC)
    }
  
  func navigateToLogin() {
    let loginVC: LoginViewController = UIStoryboard(storyboard: .main).initVC()
   self.setNavigationToRoot(viewContoller: loginVC)
  }
    func navigateToSplash() {
        let splashVC: SplashScreenViewController = UIStoryboard(storyboard: .main).initVC()
        self.setNavigationToRoot(viewContoller: splashVC)
    }
    
  
  func setNavigationToRoot(viewContoller: UIViewController) {
    //Setting Login to Root Controller
    let navController = UINavigationController()
    //App Theming
    navController.navigationBar.barTintColor = Constant.AppColor.navigationColor
    navController.navigationBar.tintColor = Constant.AppColor.navigationBarTintColor
    navController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Constant.AppColor.navigationColorTextColor]
    navController.setNavigationBarHidden(true, animated: true)
    navController.pushViewController(viewContoller, animated: true)
    self.window?.rootViewController = navController
    self.window?.makeKeyAndVisible()
  }
  
  func addKeyboardManager() {
    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.shouldResignOnTouchOutside = true
  }

  /*func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
*/
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }

}
