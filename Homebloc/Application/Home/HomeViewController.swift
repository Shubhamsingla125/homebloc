//
//  HomeViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 28/03/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class HomeViewController: DIBaseController {

    @IBOutlet weak var internationalBtn: UIButton!
    @IBOutlet weak var domesticBtn: UIButton!
    @IBOutlet weak var keywordTxtFld: UITextField!
    @IBOutlet weak var cityTxtFld: UITextField!
    
    @IBOutlet weak var profitgeneratingLbl: UILabel!
    @IBOutlet weak var endDateBtn: UIButton!
    @IBOutlet weak var startDateBtn: UIButton!
    @IBOutlet weak var startDayLbl: UILabel!
    @IBOutlet weak var endDayLbl: UILabel!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var luckyPoolBtn: UIButton!
    var selectedCity:City?
    var datesRange: [Date]?
    var languageModel : LanguageLabel?
    var selectedDateCount  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLanguage()
     
        // Do any additional setup after loading the view.
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            cityTxtFld.placeholder = lblsValue.lBL_SELECT_CITY
            internationalBtn.setTitle(lblsValue.lBL_INTERNATIONAL, for: .normal)
            domesticBtn.setTitle(lblsValue.lBL_DOMESTIC, for: .normal)
            endDateBtn.setTitle(languageModel?.lBL_END_DATE, for: .normal)
            startDateBtn.setTitle(languageModel?.lBL_START_DATE, for: .normal)
            keywordTxtFld.placeholder = languageModel?.lBL_SELECT_HINT_TEXT
            profitgeneratingLbl.text = languageModel?.lBL_PROFIT_GENERATING
            searchBtn.setTitle(languageModel?.lBL_SEARCH, for: .normal)
            luckyPoolBtn.setTitle(languageModel?.lBL_LUCKY_POOL, for: .normal)
        }
    }
    
    func isValidSearch()->Bool{
        var message : String?
        if datesRange == nil || datesRange?.count ?? 0 <= 1{
            message = languageModel?.lBL_SELECT_DATE
        }
        if message != nil {
            self.showAlert(message:message)
            return false
        }
        return true
    }
    
    @IBAction func domesticTapped(_ sender: UIButton) {
        let citiesVc : CitiesViewController = UIStoryboard(storyboard: .hotel).initVC()
        citiesVc.selectedCityType = CitiesType.domestic.rawValue
        citiesVc.delegate = self
        citiesVc.selectedCIty = self.selectedCity
        self.navigationController?.pushViewController(citiesVc, animated: true)
    }
    
    @IBAction func internationalTapped(_ sender: UIButton) {
        let citiesVc : CitiesViewController = UIStoryboard(storyboard: .hotel).initVC()
        citiesVc.selectedCityType = CitiesType.international.rawValue
        citiesVc.delegate = self
        citiesVc.selectedCIty = self.selectedCity
        self.navigationController?.pushViewController(citiesVc, animated: true)
    }
    
    @IBAction func startDateTapped(_ sender: UIButton) {
        let calendarVc : CalendarViewController = UIStoryboard(storyboard: .hotel).initVC()
        calendarVc.delegate = self
        calendarVc.datesRange = self.datesRange
        self.present(calendarVc, animated: true, completion: nil)
    }
    
    @IBAction func endDateTapped(_ sender: UIButton) {
        let calendarVc : CalendarViewController = UIStoryboard(storyboard: .hotel).initVC()
        calendarVc.delegate = self
        calendarVc.datesRange = self.datesRange
        self.present(calendarVc, animated: true, completion: nil)
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        if isValidSearch() {
            let hotelVC:HotelViewController = UIStoryboard(storyboard: .hotel).initVC()
            hotelVC.firstDate = self.datesRange?.first
            hotelVC.lastDate = self.datesRange?.last
            hotelVC.selectedDateCount = self.selectedDateCount
            hotelVC.city = self.selectedCity
            hotelVC.searchtext = self.keywordTxtFld.text ?? ""
            self.navigationController?.pushViewController(hotelVC, animated: true)
        }
    }
    
    @IBAction func luckyPoolTapped(_ sender: UIButton) {
        if isValidSearch() {
            let hotelVC:LuckPoolHotelListViewController = UIStoryboard(storyboard: .hotel).initVC()
            hotelVC.firstDate = self.datesRange?.first
            hotelVC.lastDate = self.datesRange?.last
            hotelVC.selectedDateCount = self.selectedDateCount
            hotelVC.searchtext = self.keywordTxtFld.text ?? ""
            hotelVC.city = self.selectedCity
            self.navigationController?.pushViewController(hotelVC, animated: true)
        }
    }
    
    func setUpDate(_ date:Date?,btn:UIButton){
        let dateFortr = self.getDateFormatter(format:"MMM,dd yyyy",timeZone:NSTimeZone.local as NSTimeZone)
        btn.setTitle(dateFortr.string(from: date ?? Date()), for: .normal)
    }
    
}
extension HomeViewController:CalendarDelegate{
    func getCalendarRange(range: [Date]?,selectedDates: Int) {
        datesRange = range
        if range?.count == 0 || datesRange == nil {
            startDayLbl.text = ""
            endDayLbl.text = ""
            endDateBtn.setTitle(languageModel?.lBL_END_DATE, for: .normal)
            startDateBtn.setTitle(languageModel?.lBL_START_DATE, for: .normal)
            return
        }
        let dateFortr = getDateFormatter(format:"EEEE",timeZone:NSTimeZone.local as NSTimeZone)
        setUpDate(range?.first, btn: startDateBtn)
        startDayLbl.text = dateFortr.string(from: range?.first ?? Date())
        if range?.count ?? 0 > 1 {
            setUpDate(range?.last, btn: endDateBtn)
            endDayLbl.text = dateFortr.string(from: range?.last ?? Date())
        }
        selectedDateCount = selectedDates 
    }
}
extension HomeViewController : CitiesDelegate{
    
    func selectCity(city: City) {
        selectedCity = city
        cityTxtFld.text = city.cityname
    }
    
}
