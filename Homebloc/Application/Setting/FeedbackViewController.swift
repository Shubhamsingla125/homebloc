//
//  FeedbackViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 19/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CountryPicker
class FeedbackViewController: DIBaseController {
    
    var langugaeModel:LanguageLabel?

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var customerCareLbl: UILabel!
    @IBOutlet weak var dividendValueLbl: UILabel!
    @IBOutlet weak var dividendHeaderLbl: UILabel!
    @IBOutlet weak var valueTitleLbl: UILabel!
    @IBOutlet weak var valueHeaderLbl: UILabel!
    @IBOutlet weak var creditValueLbl: UILabel!
    @IBOutlet weak var creditHeaderLbl: UILabel!
    @IBOutlet weak var aboutHomeBlocLbl: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var countryPickerBtn: UIButton!
    @IBOutlet weak var phoneNumberTxtFld: UITextField!
    @IBOutlet weak var feedbackTxtVw: IQTextView!
    @IBOutlet weak var headerTitleLbl: UILabel!
     @IBOutlet weak var picker: CountryPicker!
    var countryCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getLanguage()
        pickerView.isHidden = true
        setUpCountryPicker()
        // Do any additional setup after loading the view.
    }
    
    func setUpCountryPicker(){
        //get current country
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
        pickerView.isHidden = true
    }
    
    func getLanguage(){
        if let language = Utility.getLanguageLabelModel() {
            langugaeModel = language
            customerCareLbl.text = (language.lBL_CUSTOMER_SERVICE ?? "")
            dividendValueLbl.text = language.lBL_DEVIDE_END_INFO ?? ""
            dividendHeaderLbl.text = language.lBL_DEVIDE_END ?? ""
            valueTitleLbl.text = language.lBL_VALUE_INFO ?? ""
            valueHeaderLbl.text = language.lBL_VALUE ?? ""
            creditValueLbl.text = language.lBL_CREDIT_INFO ?? ""
            creditHeaderLbl.text = language.lBL_CREDIT ?? ""
            aboutHomeBlocLbl.text =  language.lBL_ABOUT_HOMEBLOC ?? ""
            submitBtn.setTitle(language.lBL_SUBMIT ?? "", for: .normal)
            feedbackTxtVw.placeholder = language.lBL_COMMENT_FOR_REWARDS
            headerTitleLbl.text = language.lBL_FEEDBACK ?? ""
            
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: UIButton) {
        if feedbackTxtVw.text.isValidInRange(minLength: 3, maxLength: 301){
            self.submitFeedbackApi()
        } else {
            self.showAlert(message:langugaeModel?.lBL_COMMENT_FOR_REWARDS)
        }
        
    }
    
    @IBAction func countryDoneTapped(_ sender: UIButton) {
         pickerView.isHidden = true
    }
    
    @IBAction func selectCountryTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        pickerView.isHidden = false
    }
    
    func submitFeedbackApi(){
        self.showLoader()
        DIWebLayerUserAPI().submitFeedback(parameters: nil, language: self.getLangugaeFromDefault(), feedback: feedbackTxtVw.text ?? "", success: { (response) in
            self.hideLoader()
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
}
extension FeedbackViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView.isHidden = true
        return true
    }
}

extension FeedbackViewController : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
         pickerView.isHidden = true
    }
}


extension FeedbackViewController : CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        self.countryCode = countryCode
        countryPickerBtn.setTitle(phoneCode, for: .normal)
    }
}
