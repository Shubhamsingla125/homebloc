
//
//  OfficalAccountViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 19/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class OfficalAccountViewController: DIBaseController {

    @IBOutlet weak var headerTitleLbl:UILabel!
    @IBOutlet weak var officialAntLbl: UILabel!
    @IBOutlet weak var officalChatLbl: UILabel!
    @IBOutlet weak var officialAccountLbl: UILabel!
    @IBOutlet weak var scanTheQRLbl: UILabel!
    
    var languageModel : LanguageLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLanguage()
        // Do any additional setup after loading the view.
    }
    
    func setupLanguage(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
            headerTitleLbl.text = language.lBL_OFFICIAL_ACCOUNT ?? ""
            officialAntLbl.text = language.lBL_OFFICIAL_ACCOUNT_INFO ?? ""
            officalChatLbl.text = language.lBL_OFFICIAL_WE_CHAT_GROUP ?? ""
            officialAccountLbl.text = language.lBL_OFFICIAL_ACCOUNT ?? ""
            scanTheQRLbl.text = language.lBL_SCAN_QR_TEXT ?? ""
        }
    }
    
    @IBAction func backTapped(_ target:UIButton){
        self.navigationController?.popViewController(animated: true)
    }


}
