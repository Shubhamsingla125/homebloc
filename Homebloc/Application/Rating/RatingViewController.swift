//
//  RatingViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 12/03/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Cosmos
import IQKeyboardManagerSwift
protocol RatingDelegate {
    func ratingSucess()
}
class RatingViewController: DIBaseController {
    @IBOutlet weak var postReviewBtn: UIButton!
    @IBOutlet weak var commentTxtView: IQTextView!
    @IBOutlet weak var pleaseReviewLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    var orderId = "174"
    var delegate : RatingDelegate?
    var languageModel : LanguageLabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        ratingView.settings.fillMode = .precise
        setUpLanguage()
        // Do any additional setup after loading the view.
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            postReviewBtn.setTitle(lblsValue.lBL_POST_REVIEW, for: .normal)
            commentTxtView.placeholder = lblsValue.lBL_WRITE_COMMENT
            pleaseReviewLbl.text = lblsValue.lBL_REVIEW
        }
    }

 
     @IBAction func cancelTapped(_ sender: UIButton) {
        self.delegate?.ratingSucess()
        self.dismiss(animated: true, completion: nil)
     }
 
    @IBAction func postReviewTapped(_ sender: UIButton) {
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader() 
            let params = [
                "orderId":orderId,
                "describeLevel":"\(ratingView.rating)",
                "experienceComment":commentTxtView.text ?? "",
                "commentImgLocation":"",
                "anonymity":0,
                "token": User.sharedInstance.oauthToken ?? "",
                "lang":languageCode
                ] as [String : Any]
            
            DIWebLayerUserAPI().rateHotel(parameters: params, success: { (response) in
                self.hideLoader()
                
                self.dismiss(animated: true, completion: {
                    self.delegate?.ratingSucess()
                })
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
}
