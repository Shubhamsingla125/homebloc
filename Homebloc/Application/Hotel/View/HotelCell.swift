//
//  HotelCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 06/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos
class HotelCell: UITableViewCell {

    @IBOutlet weak var returnLbl: UIButton!
    @IBOutlet weak var saleLbl: UIButton!
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var hotelPriceLbl: UILabel!
    @IBOutlet weak var hotelAddresslbl: UILabel!
    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var hotelImgVw: UIImageView!
    @IBOutlet weak var coolectionView: UICollectionView!
    @IBOutlet weak var hotelTypeLbl: UILabel!
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var chenInOutDatesLbl: UILabel!
    var hotel : Hotel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    
    func initCell(_ hotel:Hotel){
        if let hotelImgUrl = URL(string:hotel.hotelLogo ?? ""){
            self.hotelImgVw.kf.setImage(with: hotelImgUrl)
        }
        hotelNameLbl.text = hotel.hotelName ?? ""
        hotelPriceLbl.text = "\(hotel.currency_text ?? "") \(hotel.price ?? "")"
        hotelAddresslbl.text =  hotel.locationArea ?? ""
         if let lblsValue = Utility.getLanguageLabelModel(){
            commentLbl.text = "\(hotel.commentCount ?? "0") \(lblsValue.lBL_COMMENTS ?? "")"
        }
        ratingView.rating = Double(hotel.score ?? "") ?? 0.0
        ratingView.settings.fillMode = .precise
        self.hotel = hotel
        self.coolectionView.reloadData()
    }
    
    func initCellForDetail(_ hotel:HotelRoomList){
        if let hotelImgUrl = URL(string:hotel.roomimage ?? ""){
            self.hotelImgVw.kf.setImage(with: hotelImgUrl)
        }
        hotelNameLbl.text = hotel.houseName ?? ""
        hotelPriceLbl.text = "\(hotel.predeterminedPrice ?? 0) \(hotel.currency ?? "")"
        if let lblsValue = Utility.getLanguageLabelModel(){
            reserveBtn.setTitle(lblsValue.lBL_RESERVE, for: .normal)
            switch Int(hotel.bedType ?? ""){
            case 1:
                hotelTypeLbl.text = lblsValue.lBL_SINGLE_BED
            case 2:
                hotelTypeLbl.text = lblsValue.lBL_DOUBLE_BED
            case 3:
                hotelTypeLbl.text = lblsValue.lBL_BIG_BED
            default:
                hotelTypeLbl.text = lblsValue.lBL_MULTIPLE_BED
            }
            saleLbl.setTitle(lblsValue.lBL_SALE, for: .normal)
            returnLbl.setTitle(lblsValue.lBL_RETURN, for: .normal)
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            if let roomExists =  hotel.roomExist {
                if roomExists > 0 {
                    self.reserveBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1)
                    self.reserveBtn.isEnabled = true
                    self.reserveBtn.isUserInteractionEnabled = true
                } else {
                    self.reserveBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1).withAlphaComponent(0.2)
                    self.reserveBtn.isEnabled = false
                    self.reserveBtn.isUserInteractionEnabled = false
                }
            }
        }
    }
    
    func initOrderCell(order:Order){
        if let languageLbl = Utility.getLanguageLabelModel(),let dayslbl = languageLbl.lBL_DAYS,let checkIn = languageLbl.lBL_CHECK_IN,let checkout = languageLbl.lBL_CHECK_OUT,let arrivalTime = order.arrival_time,let departureTime = order.departure_time,let address = order.seller_address,let name = order.seller_name{
            self.daysLbl.text = (order.days ?? "1") + " " + dayslbl
            self.hotelPriceLbl.text = (order.currencytext ?? "") + (order.total_cost ?? "")
            self.hotelImgVw.backgroundColor = UIColor.white
            if let url = URL(string:order.room_logo ?? ""){
                self.hotelImgVw.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "logo"), options: nil, progressBlock: nil, completionHandler: nil)
            }
            self.hotelNameLbl.text = name + "\n" + address
            let checkInText = checkIn + " " + arrivalTime
            let checkOutText = checkout + " " + departureTime
            self.chenInOutDatesLbl.text = checkInText + "\n" + checkOutText
        }
    }
    
   
}
extension HotelCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hotel.services_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as? CategoryCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.categoryBtn.setTitle(hotel.services_list[indexPath.item], for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: collectionView.frame.width/2-5, height: 22)
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
