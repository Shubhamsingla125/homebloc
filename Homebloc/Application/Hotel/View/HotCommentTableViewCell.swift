//
//  HotCommentTableViewCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 05/03/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Cosmos
class HotCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var UnLikeBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(hotelDetail:HotelDesc){
        self.commentLbl.text = ""
        self.ratingView.rating = Double(hotelDetail.score ?? "") ?? 0.0
        self.ratingView.settings.fillMode = .precise
        likeBtn.setTitle("\(hotelDetail.likes ?? 0)", for: .normal)
        UnLikeBtn.setTitle("\(hotelDetail.dislikes ?? 0)", for: .normal)
        self.ratingLbl.text = hotelDetail.score ?? "0.0"
    }

}
