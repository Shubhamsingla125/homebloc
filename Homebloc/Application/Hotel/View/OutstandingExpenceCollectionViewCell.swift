//
//  OutstandingExpenceCollectionViewCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 10/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class OutstandingExpenceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var logoImgVw: UIImageView!
    @IBOutlet weak var serviceLbl: UILabel!
    
    func initCell(_ service:Services){
        if let url = URL(string: service.service_icon ?? ""){
            logoImgVw.kf.setImage(with: url)
        }
        serviceLbl.text = service.service_name
    }
}
