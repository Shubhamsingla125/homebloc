//
//  HotelTagTableViewCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 16/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
protocol HotelTagDelegate {
    func tagSelect(hotelTag:HotelTag)
}

class HotelTagTableViewCell: UITableViewCell {

    var tagList : [HotelTag] = []
    var city : City?
    var delegate:HotelTagDelegate?
    @IBOutlet weak var collectionView:UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension HotelTagTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tagList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HoteltagCollectionViewCell", for: indexPath) as? HoteltagCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.initCell(self.tagList[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2.5-5, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let hotelTag = self.tagList[indexPath.item]
        delegate?.tagSelect(hotelTag: hotelTag)
        
//        let hotelVC : HotelViewController = UIStoryboard(storyboard: .hotel).initVC()
//        hotelVC.hoteltag = hotelTag
//        hotelVC.city = self.city
//        self.navigationController?.pushViewController(hotelVC, animated: true)
    }
}

