//
//  CommentTableViewCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 12/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Cosmos
class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentStatus: UITextField!
    @IBOutlet weak var unLikeBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var nameLbl: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(commentList:CommentList){
        self.ratingView.rating = Double(commentList.userScore ?? "") ?? 0.0
        self.commentLbl.text = commentList.commentContent ?? ""
        self.nameLbl.text = commentList.userName
        self.likeBtn.setTitle(commentList.commentSupport ?? "0", for: .normal)
        self.unLikeBtn.setTitle(commentList.commentObject ?? "0", for: .normal)
        if let lblsValue = Utility.getLanguageLabelModel(){
            
            switch Int(commentList.userScore ?? "") ?? 0 {
            case 1,2,3:commentStatus.text = lblsValue.lBL_AVERAGE
            case 4:commentStatus.text = lblsValue.lBL_GOOD
            case 5:commentStatus.text = lblsValue.lBL_VERY_GOOD
            default:
                commentStatus.text = ""
            }
        }
        
    }
    
    
}
