//
//  HotelDetailViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 10/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher
class HotelDetailViewController: DIBaseController {
    
    @IBOutlet weak var hotelLbl: UILabel!
    @IBOutlet weak var restnView: UIView!
    @IBOutlet weak var userCmntView: UIView!
    @IBOutlet weak var hotelImgVw: UIImageView!
    @IBOutlet weak var ratingVw: CosmosView!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var mapImgVw: UIImageView!
    @IBOutlet weak var userCmntBtn: UIButton!
    @IBOutlet weak var reservtnBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hotelAddressLbl: UILabel!
    @IBOutlet weak var featureListLbl: UILabel!
    @IBOutlet weak var addtionalinfoLbl: UILabel!
    @IBOutlet weak var amenitiesLbl: UILabel!
    @IBOutlet weak var checkInOutPlaceholderLbl: UILabel!
    @IBOutlet weak var petAllowancePlaceholderLbl: UILabel!
    @IBOutlet weak var langugaeServiceLbl: UILabel!
    @IBOutlet weak var checkInLbl: UILabel!
    @IBOutlet weak var langugaeServiceValueLbl: UILabel!
    @IBOutlet weak var petAllowanceLbl: UILabel!
    @IBOutlet weak var fvtBtn: UIButton!
    
    var languageModel : LanguageLabel?
    var startDate = ""
    var endDate = ""
    var hotel:Hotel?
    var commentList: [CommentList]?
    var hotelDetail: HotelDetail?
    var selectedCity:City?
    var paymentStartDate = Date()
    var paymentEndDate = Date()
    var nightLbl = ""
    var hotelDesc : HotelDesc?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentList = []
        // Do any additional setup after loading the view.
        createUserExperience()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        if collectionViewHeight.constant != collectionView.contentSize.height{
//            collectionViewHeight.constant = collectionView.contentSize.height
//        }
//        tableView.updateFooterViewHeight()
        UIView.animate(withDuration: 0.1, animations: {
            self.view.layoutIfNeeded()
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            hotelLbl.text = lblsValue.lBL_HOTEL
            userCmntBtn.setTitle(lblsValue.lBL_USER_COMMENTS?.uppercased(), for: .normal)
            reservtnBtn.setTitle(lblsValue.lBL_RESERVATION?.uppercased(), for: .normal)
            featureListLbl.text = lblsValue.lBL_HOTEL_FEATURED
            addtionalinfoLbl.text = lblsValue.lBL_ADDITIONAL_INFO
            amenitiesLbl.text = lblsValue.lBL_AMEN
            checkInOutPlaceholderLbl.text = lblsValue.lBL_CHECK_IN_OUT
            petAllowancePlaceholderLbl.text = lblsValue.lBL_PET_ALLWANCE
            langugaeServiceLbl.text = lblsValue.lBL_LANGUAGE_SERVICE
        }
    }
    
    func createUserExperience(){
        setUpLanguage()
        if let hotelImgUrl = URL(string:hotel?.hotelLogo ?? ""){
            self.hotelImgVw.kf.setImage(with: hotelImgUrl)
        }
        userCmntView.isHidden = false
        restnView.isHidden = true
        hotelAddressLbl.text = hotel?.locationArea ?? ""
        hotelNameLbl.text = hotel?.hotelName ?? ""
        ratingLbl.text = hotel?.score ?? ""
        commentLbl.text = "\(hotel?.commentCount ?? "") \(languageModel?.lBL_COMMENTS ?? "")"
        ratingVw.rating = Double(hotel?.score ?? "") ?? 0.0
        ratingVw.settings.fillMode = .precise
        userCmntBtn.isSelected = true
        reservtnBtn.isSelected = false
        DispatchQueue.main.async {
            self.getRatingList()
        }
    }
    
    func getRatingList(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String,let selectedHotel = hotel {
            self.showLoader()
            DIWebLayerUserAPI().getCommentList(parameters: nil, language: languageCode, city: self.selectedCity, hotel: selectedHotel, success: { (response) in
                self.hideLoader()
                self.commentList = response
                self.tableView.reloadData()
                self.getHotelDesc()
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    func getHotelDesc(){
        let params = [
            "token":User.sharedInstance.oauthToken ?? "",
            "lang":self.getLangugaeFromDefault()
        ]
        DIWebLayerUserAPI().getHotelDisc(parameters: params,hotelId:hotel?.hotelId ?? "", success: { (response) in
            self.hideLoader()
            self.hotelDesc = response
            self.tableView.reloadData()
            self.collectionView.reloadData()
            self.updateAdditionalInfo()
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    func getHotelDetail(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String,let selectedHotel = hotel{
            self.showLoader()
            DIWebLayerUserAPI().getHotelReservationDetail(parameters: nil, language: languageCode, hotel: selectedHotel, city: self.selectedCity, startDate: self.startDate, endDate: self.endDate, success: { (response) in
                self.hideLoader()
                self.hotelDetail = response
                self.tableView.reloadData()
                self.checkInLbl.text = self.hotelDetail?.stayDate
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    func updateAdditionalInfo(){
        checkInLbl.text = self.hotelDesc?.stayDate
        langugaeServiceValueLbl.text = languageModel?.lBL_YES
        petAllowanceLbl.text = self.hotelDesc?.bringPets != nil ? languageModel?.lBL_YES:languageModel?.lBL_NO
        fvtBtn.setImage(self.hotelDesc?.collect == 1 ? #imageLiteral(resourceName: "fillStar") : #imageLiteral(resourceName: "star"), for: .normal)
    }
    @objc func reserveTapped(_ sender: UIButton){
        let paymentvc : PaymentViewController = UIStoryboard(storyboard: .hotel).initVC()
        paymentvc.hotel = self.hotel
        paymentvc.startDate = paymentStartDate
        paymentvc.endDate = paymentEndDate
        paymentvc.nights = nightLbl
        paymentvc.hotelDetail = self.hotelDetail
        if let hotelRoom = self.hotelDetail?.hotelRoomList?[sender.tag] {
            paymentvc.hotelRoomList = hotelRoom
        }
        self.navigationController?.pushViewController(paymentvc, animated: true)
    }

    @IBAction func userCmntTapped(_ sender: UIButton) {
        userCmntBtn.isSelected = true
        reservtnBtn.isSelected = false
        userCmntView.isHidden = false
        restnView.isHidden = true
        self.tableView.reloadData()
    }
    
    @IBAction func hotelFvtTapped(_ sender: UIButton) {
        let params = [
            "hotelId":self.hotel?.hotelId ?? "",
            "token":User.sharedInstance.oauthToken ?? "",
            "lang":self.getLangugaeFromDefault()
        ]
        self.showLoader()
        DIWebLayerUserAPI().markFvtHotel(parameters: params, success: { (response) in
            self.fvtBtn.setImage(#imageLiteral(resourceName: "fillStar"), for: .normal)
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    @IBAction func hotelShareTapped(_ sender: UIButton) {
        let text = hotel?.hotelName ?? ""
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func reservtnTapped(_ sender: UIButton) {
        reservtnBtn.isSelected = true
        userCmntBtn.isSelected = false
        userCmntView.isHidden = true
        restnView.isHidden = false
        self.tableView.reloadData()
        if hotelDetail == nil {
           getHotelDetail()
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func likeComment(_ sender: UIButton ){
        if (self.commentList?[sender.tag]) != nil {
            likeDislikeComment(type:0,tag:sender.tag)
        }
    }
    
    @objc func unlikeComment(_ sender: UIButton){
            likeDislikeComment(type:1,tag:sender.tag)
    }
    
    func likeDislikeComment(type:Int,tag:Int){
        if let comment = self.commentList?[tag]{
            let params = [
                "commentId":comment.id ?? "",
                "likeProperty":type,
                "token":User.sharedInstance.oauthToken ?? "",
                "lang":getLangugaeFromDefault()
                ] as [String : Any]
            self.showLoader()
            DIWebLayerUserAPI().rateComment(parameters: params, success: { (response) in
                self.hideLoader()
                if type == 0 {
                    if let like = Int(comment.commentSupport ?? ""){
                    comment.commentSupport = "\(like+1)"
                    }
                } else {
                    if let unlike = Int(comment.commentObject ?? ""){
                        comment.commentObject = "\(unlike+1)"
                    }
                }
                self.tableView.reloadData()
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
        
    }
}
extension HotelDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userCmntBtn.isSelected {
            if let commentList = self.commentList{
                return commentList.count + 1
            }
            return 1
        }
        return self.hotelDetail?.hotelRoomList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userCmntBtn.isSelected {
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotCommentTableViewCell") as? HotCommentTableViewCell else {
                    return UITableViewCell()
                }
                if let detail = self.hotelDesc {
                    cell.initCell(hotelDetail: detail)
                }
                cell.selectionStyle = .none
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell else {
                    return UITableViewCell()
                }
                if let comment = self.commentList?[indexPath.row - 1] {
                    cell.initCell(commentList:comment)
                }
                cell.likeBtn.tag = indexPath.row - 1
                cell.unLikeBtn.tag = indexPath.row - 1
                
                cell.likeBtn.addTarget(self, action: #selector(likeComment(_:)), for: .touchUpInside)
                cell.unLikeBtn.addTarget(self, action: #selector(unlikeComment(_:)), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
            }
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelCell") as? HotelCell else {
                return UITableViewCell()
            }
            if let hotelRoom = self.hotelDetail?.hotelRoomList?[indexPath.row] {
                cell.initCellForDetail(hotelRoom)
            }
            cell.reserveBtn.tag = indexPath.row
            cell.reserveBtn.addTarget(self, action: #selector(reserveTapped(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return userCmntBtn.isSelected ? UITableViewAutomaticDimension : 140.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return userCmntBtn.isSelected ? UITableViewAutomaticDimension : 140.0
    }
}
extension HotelDetailViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.hotelDesc?.services?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OutstandingExpenceCollectionViewCell", for: indexPath) as?  OutstandingExpenceCollectionViewCell else {
            return UICollectionViewCell()
        }
        if let service = self.hotelDesc?.services?[indexPath.item]{
            cell.initCell(service)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3-10, height: 100)
    }
}

extension UITableView {
    func updateHeaderViewHeight(){
        if let headerView = self.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            //Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                DispatchQueue.main.async {
                    self.tableHeaderView?.layoutIfNeeded()
                    self.tableHeaderView = headerView
                    self.layoutIfNeeded()
                }
            }
            headerView.translatesAutoresizingMaskIntoConstraints = true
        }
    }
    
    func updateFooterViewHeight(){
        if let footerView = self.tableFooterView {
            let height = footerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var footerFrame = footerView.frame
            //Comparison necessary to avoid infinite loop
            if height != footerFrame.size.height {
                footerFrame.size.height = height
                footerView.frame = footerFrame
                DispatchQueue.main.async {
                    self.tableFooterView?.layoutIfNeeded()
                    self.tableFooterView = footerView
                    self.layoutIfNeeded()
                }
            }
            footerView.translatesAutoresizingMaskIntoConstraints = true
        }
    }
}
