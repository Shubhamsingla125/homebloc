//
//  HotelViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 06/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import FSCalendar
import RangeSeekSlider
import Alamofire

class HotelByTagViewController: DIBaseController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hotelLbl:UILabel!
    
    var languageModel : LanguageLabel?
    var hotelList : [Hotel] = []
    var firstDate: Date?
    var lastDate: Date?
    var selectedDateCount = 0
    var searchtext = ""
    var hoteltag : HotelTag?
    var city:City?
    var startDateString = ""
    var hotelOrderBy : HotelSuggestion?
    var endDateString = ""
    var minPrice = 0
    var maxPrice = 0
    var rating = 0
    var nightLbl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpLanguage()
        getHotelList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            hotelLbl.text = lblsValue.lBL_HOTEL
        }
    }
    
    func getHotelList(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader()
            DIWebLayerUserAPI().getHotelListByTag(parameters: nil, tagId: hoteltag?.tag_id ?? "", language: languageCode, searchKey : searchtext,city : self.city, startDate: startDateString, endDate: endDateString, orderBy : hotelOrderBy,star : rating,minPrice : minPrice,maxPrice : maxPrice, success: { (response) in
                self.hideLoader()
                if let hotels = response{
                    self.hotelList = hotels
                }
                self.tableView.reloadData()
               
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }

    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HotelByTagViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.hotelList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelCell") as? HotelCell else {
            return UITableViewCell()
        }
        cell.initCell(self.hotelList[indexPath.section])
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let hotelDetailVC:HotelDetailViewController = UIStoryboard(storyboard: .hotel).initVC()
        hotelDetailVC.startDate = self.startDateString
        hotelDetailVC.endDate = self.endDateString
        hotelDetailVC.hotel = self.hotelList[indexPath.section]
        hotelDetailVC.selectedCity = self.city
        hotelDetailVC.paymentStartDate = self.firstDate ?? Date()
        hotelDetailVC.paymentEndDate = self.lastDate ?? Date()
        hotelDetailVC.nightLbl = nightLbl
        self.navigationController?.pushViewController(hotelDetailVC, animated: true)
    }
}


