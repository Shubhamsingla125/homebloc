//
//  HotelViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 06/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import FSCalendar
import RangeSeekSlider
import Alamofire
enum HotelSuggestion:Int, CaseIterable{
    case recommended=0,lowtohigh,hightolow,highlyrated,nearby
}
enum RatingSection:Int{
    case two = 2,three = 3,four = 4,five = 5
}

class HotelViewController: DIBaseController {
   
    @IBOutlet weak var searchTextLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hotelLbl:UILabel!
    @IBOutlet weak var suggestedBtn: UIButton!
    @IBOutlet weak var areaBtn: UIButton!
    @IBOutlet weak var priceBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var startLbl: UILabel!
    @IBOutlet weak var endLbl: UILabel!
    @IBOutlet weak var nightLbl: UILabel!
    @IBOutlet weak var starRatingLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var maxLbl: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceBgView: UIView!
    @IBOutlet weak var priceRangeBar: RangeSeekSlider!
    
    var languageModel : LanguageLabel?
    var hotelList : [Hotel] = []
    var firstDate: Date?
    var lastDate: Date?
    var selectedDateCount = 0
    var searchtext = ""
    var hoteltag : HotelTag?
    var city:City?
    var startDateString = ""
    var hotelOrderBy : HotelSuggestion?
    var endDateString = ""
    var minPrice = 0
    var maxPrice = 0
    var rating = 0
    var tagList:[HotelTag] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpLanguage()
        initilizer()
        suggestedBtn.isSelected = false
        getHotelList()
        getHotelTag()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            hotelLbl.text = lblsValue.lBL_HOTEL
            suggestedBtn.setTitle(lblsValue.lBL_SUGGESTED, for: .normal)
            areaBtn.setTitle(lblsValue.lBL_AREA, for: .normal)
            priceBtn.setTitle(lblsValue.lBL_PRICE, for: .normal)
            allBtn.setTitle(lblsValue.lBL_ALL, for: .normal)
        }
    }
    
    func getHotelTag(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String, let selectedCity = city {
            DIWebLayerUserAPI().getHotelTags(parameters: nil, language: languageCode, city: selectedCity, success: { (response) in
                if let hoteltagList = response {
                    self.tagList = hoteltagList
                }
                self.tableView.reloadData()
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    func initilizer(){
        let dateformatter = getDateFormatter(format:"MMM,dd yyyy",timeZone:NSTimeZone.local as NSTimeZone)
        startLbl.text = dateformatter.string(from: firstDate ?? Date())
        endLbl.text = dateformatter.string(from: lastDate ?? Date())
        let requestDateFormatter = getDateFormatter(format:"yyyy-MM-dd",timeZone:NSTimeZone.local as NSTimeZone)
        startDateString = requestDateFormatter.string(from: firstDate ?? Date())
        endDateString = requestDateFormatter.string(from: lastDate ?? Date())
        nightLbl.text = "\(self.selectedDateCount) \(languageModel?.lBL_NIGHT ?? "")"
        searchTextLbl.text = "  \(searchtext)"
        priceView.isHidden = true
        priceBgView.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        priceBgView.addGestureRecognizer(tap)
        priceBgView.isUserInteractionEnabled = true
        priceRangeBar.minValue = 0
        priceRangeBar.maxValue = 1000
        priceRangeBar.selectedMinValue = 0
        priceRangeBar.selectedMaxValue = 1000
        priceRangeBar.delegate = self
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        priceView.isHidden = true
        priceBgView.isHidden = true
    }
    
   func getHotelList(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader()
            DIWebLayerUserAPI().getHotelList(parameters: nil, language: languageCode, searchKey : searchtext,city : self.city, startDate: startDateString, endDate: endDateString, orderBy : hotelOrderBy,star : rating,minPrice : minPrice,maxPrice : maxPrice, success: { (response) in
                self.hideLoader()
                if let hotels = response{
                    self.hotelList = hotels
                }
                self.tableView.reloadData()
                self.priceView.isHidden = true
                self.priceBgView.isHidden = true
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    @IBAction func selectPriceTapped(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            minPrice = 0
            maxPrice = 100
        case 2:
            minPrice = 100
            maxPrice = 200
        case 3:
            minPrice = 200
            maxPrice = 300
        case 4:
            minPrice = 300
            maxPrice = 400
        default:
            minPrice = 50
            maxPrice = 1000
        }
        self.getHotelList()
    }
    
    @IBAction func starTapped(_ sender: UIButton) {
        rating = sender.tag + 1
        self.getHotelList()
    }
    
    
    @IBAction func suggestionTapped(_ sender: UIButton) {
        suggestedBtn.isSelected = !suggestedBtn.isSelected
        tableView.reloadData()
    }
    
    @IBAction func areaTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func priceTapped(_ sender: UIButton) {
        priceView.isHidden = false
        priceBgView.isHidden = false
    }
    
    @IBAction func allTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HotelViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if suggestedBtn.isSelected {
            return HotelSuggestion.allCases.count + self.hotelList.count + 1
        }
        return self.hotelList.count + 1
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if  (suggestedBtn.isSelected && section >= HotelSuggestion.allCases.count) || (!suggestedBtn.isSelected && section >= 0){
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.font = UIFont(name: "Roboto", size: 20)
            headerLabel.textColor = UIColor.black
            headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            return headerView
        }
        return nil
       
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (suggestedBtn.isSelected && section == HotelSuggestion.allCases.count) || (!suggestedBtn.isSelected && section == 0){
            return 40
        } else if (suggestedBtn.isSelected && section == HotelSuggestion.allCases.count + 1) || (!suggestedBtn.isSelected && section == 1){
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (suggestedBtn.isSelected && section == HotelSuggestion.allCases.count) || (!suggestedBtn.isSelected && section == 0){
            return languageModel?.lBL_HOTEL_FEATURED
        } else if (suggestedBtn.isSelected && section == HotelSuggestion.allCases.count + 1) || (!suggestedBtn.isSelected && section == 1){
            return languageModel?.lBL_SEARCH_RESULT
        }
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if suggestedBtn.isSelected  && indexPath.section < HotelSuggestion.allCases.count{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelSuggestionCell") as? HotelSuggestionCell else {
                return UITableViewCell()
            }
            if let hotelSuggestion = HotelSuggestion(rawValue: indexPath.section){
                switch hotelSuggestion{
                case .recommended:
                    cell.titleLbl.text = languageModel?.lBL_RECOMENDED
                case .lowtohigh:
                    cell.titleLbl.text = languageModel?.lBL_PRICE_LOW_TO_HIGH
                case .hightolow:
                     cell.titleLbl.text = languageModel?.lBL_PRICE_HIGH_TO_LOW
                case .highlyrated:
                    cell.titleLbl.text = languageModel?.lBL_PRICE_HIGHLY_RATED
                case .nearby:
                    cell.titleLbl.text = languageModel?.lBL_PRICE_NEARBY
                }
            }
            cell.selectionStyle = .none
            return cell
        }else{
            if (suggestedBtn.isSelected  && indexPath.section == HotelSuggestion.allCases.count) || (!suggestedBtn.isSelected  && indexPath.section == 0){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelTagTableViewCell") as? HotelTagTableViewCell else {
                    return UITableViewCell()
                }
                cell.tagList = tagList
                cell.city = self.city
                cell.delegate = self
                cell.collectionView.reloadData()
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelCell") as? HotelCell else {
                    return UITableViewCell()
                }
                if suggestedBtn.isSelected {
                    cell.initCell(self.hotelList[indexPath.section - HotelSuggestion.allCases.count - 1])
                } else {
                    cell.initCell(self.hotelList[indexPath.section - 1])
                }
                
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if suggestedBtn.isSelected  && section < HotelSuggestion.allCases.count{
            return 0
        }
        print(section)
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if suggestedBtn.isSelected  && indexPath.section < HotelSuggestion.allCases.count{
           return 45
        }else{
            if (suggestedBtn.isSelected  && indexPath.section == HotelSuggestion.allCases.count) || (!suggestedBtn.isSelected  && indexPath.section == 0){
                return self.tagList.count == 0 ? 0 : 140
            } else {
                return 200
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if suggestedBtn.isSelected  && indexPath.section < HotelSuggestion.allCases.count{
            hotelOrderBy = HotelSuggestion(rawValue: indexPath.section) ?? HotelSuggestion.recommended
            suggestedBtn.isSelected = false
            self.tableView.reloadData()
            getHotelList()
            return
        }
        let hotelDetailVC:HotelDetailViewController = UIStoryboard(storyboard: .hotel).initVC()
        hotelDetailVC.startDate = self.startDateString
        hotelDetailVC.endDate = self.endDateString
        var hotel : Hotel!
        if suggestedBtn.isSelected {
            hotel = self.hotelList[indexPath.section - HotelSuggestion.allCases.count-1]
        } else {
            hotel = self.hotelList[indexPath.section-1]
        }
        hotelDetailVC.hotel = hotel
        hotelDetailVC.selectedCity = self.city
        hotelDetailVC.paymentStartDate = self.firstDate ?? Date()
        hotelDetailVC.paymentEndDate = self.lastDate ?? Date()
        hotelDetailVC.nightLbl = nightLbl.text ?? ""
        self.navigationController?.pushViewController(hotelDetailVC, animated: true)
    }
}

class HotelSuggestionCell:UITableViewCell{
    @IBOutlet weak var titleLbl:UILabel!
}
extension HotelViewController:RangeSeekSliderDelegate{
    func stopAllSessions() {
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach { $0.cancel() }
            uploadTasks.forEach { $0.cancel() }
            downloadTasks.forEach { $0.cancel() }
        }
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now()+0.6) {
            self.minPrice = Int(minValue)
            self.maxPrice = Int(maxValue)
            self.stopAllSessions()
            self.getHotelList()
        }
    }
}
extension HotelViewController:HotelTagDelegate{
    func tagSelect(hotelTag: HotelTag) {
        let hotelVC : HotelByTagViewController = UIStoryboard(storyboard: .hotel).initVC()
        hotelVC.hoteltag = hotelTag
        hotelVC.city = self.city
        hotelVC.startDateString = self.startDateString
        hotelVC.endDateString = self.endDateString
        hotelVC.firstDate = self.firstDate ?? Date()
        hotelVC.lastDate = self.lastDate ?? Date()
        hotelVC.nightLbl = nightLbl.text ?? ""
        self.navigationController?.pushViewController(hotelVC, animated: true)
    }
}
