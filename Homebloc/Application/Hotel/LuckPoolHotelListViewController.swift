//
//  LuckPoolHotelListViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 12/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class LuckPoolHotelListViewController: DIBaseController{
    
    @IBOutlet weak var searchTextLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hotelLbl:UILabel!
    @IBOutlet weak var startLbl: UILabel!
    @IBOutlet weak var endLbl: UILabel!
    @IBOutlet weak var nightLbl: UILabel!

    
    var languageModel : LanguageLabel?
    var hotelList : [Hotel] = []
    var firstDate: Date?
    var lastDate: Date?
    var selectedDateCount = 0
    var searchtext = ""
    var hoteltag : HotelTag?
    var city:City?
    var startDateString = ""
    var hotelOrderBy : HotelSuggestion?
    var endDateString = ""
    var minPrice = 0
    var maxPrice = 0
    var rating = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpLanguage()
        initilizer()
        getHotelList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            hotelLbl.text = lblsValue.lBL_HOTEL
        }
    }
    
    func initilizer(){
        let dateformatter = getDateFormatter(format:"MMM,dd yyyy",timeZone:NSTimeZone.local as NSTimeZone)
        startLbl.text = dateformatter.string(from: firstDate ?? Date())
        endLbl.text = dateformatter.string(from: lastDate ?? Date())
        let requestDateFormatter = getDateFormatter(format:"yyyy-MM-dd",timeZone:NSTimeZone.local as NSTimeZone)
        startDateString = requestDateFormatter.string(from: firstDate ?? Date())
        endDateString = requestDateFormatter.string(from: lastDate ?? Date())
        nightLbl.text = "\(self.selectedDateCount) \(languageModel?.lBL_NIGHT ?? "")"
        searchTextLbl.text = "  \(searchtext)"
      
    }
    
    
    func getHotelList(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader()
            DIWebLayerUserAPI().getLuckyPoolHotelList(parameters: nil, language: languageCode, searchKey : searchtext,city : self.city, startDate: startDateString, endDate: endDateString, success: { (response) in
                self.hideLoader()
                if let hotels = response{
                    self.hotelList = hotels
                }
                self.tableView.reloadData()
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    

    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension LuckPoolHotelListViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return self.hotelList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
                tableView.bounds.size.width, height: tableView.bounds.size.height))
            headerLabel.font = UIFont(name: "Roboto", size: 20)
            headerLabel.textColor = UIColor.black
            headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
            headerLabel.sizeToFit()
            headerView.addSubview(headerLabel)
            return headerView
        }
        return nil
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if  section == 0 {
            return 40
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return languageModel?.lBL_SEARCH_RESULT
        }
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelCell") as? HotelCell else {
                return UITableViewCell()
            }
        
            cell.initCell(self.hotelList[indexPath.section])
            cell.selectionStyle = .none
            return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let paymentvc : LuckyPoolPaymentViewController = UIStoryboard(storyboard: .hotel).initVC()
        paymentvc.hotel = hotelList[indexPath.section]
//        paymentvc.startDate = paymentStartDate
//        paymentvc.endDate = paymentEndDate
        paymentvc.nights = nightLbl.text ?? ""
//        paymentvc.hotelDetail = self.hotelDetail
//        if let hotelRoom = self.hotelDetail?.hotelRoomList?[sender.tag] {
//            paymentvc.hotelRoomList = hotelRoom
//        }
        self.navigationController?.pushViewController(paymentvc, animated: true)
    }
}

