//
//  FooterCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 10/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class FooterCell: UITableViewCell {

    @IBOutlet weak var readyToCheckInLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initOrderCell(order:Order){
        if let lang = Utility.getLanguageLabelModel(){
            readyToCheckInLbl.text = lang.lBL_CONFIRMED_READY ?? ""
        }
        
        if let status = Int(order.status ?? ""){
            if status == 1 || status == 2{
                cancelBtn.isHidden = false
                readyToCheckInLbl.isHidden = false
                commentBtn.isHidden = true
            } else if status == 4 {
                commentBtn.isHidden = false
                cancelBtn.isHidden = true
                readyToCheckInLbl.isHidden = true
            } else {
                cancelBtn.isHidden = true
                readyToCheckInLbl.isHidden = true
                commentBtn.isHidden = true
            }
        }
    }

}
