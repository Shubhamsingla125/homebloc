//
//  BookingDetailViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 11/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class BookingDetailViewController: DIBaseController {
    
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var hotelNameLbl: UILabel!
    @IBOutlet weak var hotelXCountLbl: UILabel!
    @IBOutlet weak var scanToCheckInLbl: UILabel!
    @IBOutlet weak var checkInLbl: UILabel!
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var headerAddressLbl: UILabel!
    @IBOutlet weak var rsvnPlaceholderLbl: UILabel!
    @IBOutlet weak var rsvnLbl: UILabel!
    @IBOutlet weak var namePlaceholderLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var contactPlaceholderLbl: UILabel!
    @IBOutlet weak var headerContactLbl: UILabel!
    @IBOutlet weak var durationPlaceholderLbl: UILabel!
    @IBOutlet weak var durationlbl: UILabel!
    @IBOutlet weak var amountPlaceholderLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var contactHomeBlocBtn: UIButton!
    @IBOutlet weak var reservationResellBtn: UIButton!
    
    @IBOutlet weak var resellInputView: UIView!
    @IBOutlet weak var resellBGView: UIView!
    @IBOutlet weak var resellViewCancelBtn: UIButton!
    @IBOutlet weak var resellViewConfirmBtn: UIButton!
    @IBOutlet weak var completedYouHaveLbl: UILabel!
    @IBOutlet weak var moneySymbolLbl: UILabel!
    @IBOutlet weak var amountTxtFld: UITextField!
    @IBOutlet weak var reselltxtLbl: UILabel!
    
    @IBOutlet weak var resellConfirmationView: UIView!
    @IBOutlet weak var confirmationCancelBtn: UIButton!
    @IBOutlet weak var confirmationConfirmBtn: UIButton!
    @IBOutlet weak var wouldLikeResellLbl: UILabel!
    @IBOutlet weak var checkinAfterResellLbl: UILabel!
    
    
     var languageModel:LanguageLabel?
    var order : Order?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLangugae()
        setuUpValue()
        // Do any additional setup after loading the view.
    }
    
    func setUpLangugae(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
            headerLbl.text = (language.lBL_RESERVATION ?? "") + " " + (language.lBL_DETAIL ?? "")
            rsvnPlaceholderLbl.text = "Rsvn#:"
            namePlaceholderLbl.text = (language.lBL_NAME ?? "") + ":"
            contactPlaceholderLbl.text = (language.lBL_CONTACT ?? "") + ":"
            durationPlaceholderLbl.text = (language.lBL_DURATION ?? "") + ":"
            amountPlaceholderLbl.text = (language.lBL_AMOUNT ?? "") + ":"
            checkInLbl.text = (language.lBL_CHECK_IN ?? "") + ":" + (order?.arrival_time ?? "")
            scanToCheckInLbl.text = "Scan to check in"
            contactHomeBlocBtn.setTitle("Contact HomeBloc", for: .normal)
           reservationResellBtn.setTitle("Reservation Resell", for: .normal)
            
            resellViewCancelBtn.setTitle(language.lBL_CANCEL ?? "", for: .normal)
            resellViewConfirmBtn.setTitle(language.lBL_CONFIRM ?? "", for: .normal)
            
            completedYouHaveLbl.text = "* Completed, you have received ¥ \(order?.total_cost ?? "")"
             moneySymbolLbl.text = "¥"
           amountTxtFld.text = "\(order?.total_cost ?? "")"
            reselltxtLbl.text  = "Resell"
            
            wouldLikeResellLbl.text = "Would you like to resell?"
            checkinAfterResellLbl.text = "You will not be able to check-in after resell"
            confirmationCancelBtn.setTitle(language.lBL_CANCEL ?? "", for: .normal)
            confirmationConfirmBtn.setTitle(language.lBL_CONFIRM ?? "", for: .normal)
        }
        setUpReservationBtn()
    }
    
    func setUpReservationBtn(){
        if let status = Int(self.order?.status ?? ""){
            reservationResellBtn.isHidden = status == 4 ? false : true
        }
    }
    
    func setuUpValue(){
        hotelNameLbl.text = order?.seller_name ?? ""
        addressLbl.text = order?.seller_address ?? ""
        headerAddressLbl.text = order?.seller_address ?? ""
        nameLbl.text = order?.name ?? ""
        contactLbl.text = order?.tel ?? ""
        durationlbl.text = (order?.arrival_time ?? "") + " " + (order?.departure_time ?? "")
        amountLbl.text = (order?.currencytext ?? "") + (order?.total_cost ?? "")
        headerContactLbl.text = order?.tel ?? ""
        rsvnLbl.text = order?.order_no ?? ""
        hotelXCountLbl.text = (order?.num ?? "") + "X"
    
        resellInputView.isHidden = true
        resellBGView.isHidden = true
        resellConfirmationView.isHidden = true
    }
    
    @IBAction func reservationResellTapped(_ sender: UIButton) {
        let orderDateFormatter = self.getDateFormatter(format: "yyyy-MM-dd", timeZone: NSTimeZone(abbreviation: "UTC"))
        let orderArrivalDate = orderDateFormatter.date(from: order?.arrival_time ?? "")
        let diff = getDateDiff(startDate: Date(), endDate: orderArrivalDate ?? Date())
        if diff > 9 {
            resellInputView.isHidden = false
            resellBGView.isHidden = false
        } else {
            self.showAlert(message:"Booking can be transferred only before 10 days of arrival date")
        }
    }
    
    func getDateDiff(startDate:Date,endDate:Date)->Int{
        let calendar = NSCalendar.current as NSCalendar
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: startDate)
        let date2 = calendar.startOfDay(for: endDate)
        let flags = NSCalendar.Unit.day
        let components = calendar.components(flags, from: date1, to: date2, options: [])
        return components.day!
    }
    
    func resellBooking(){
        self.showLoader()
        let params = [
            "lang":self.getLangugaeFromDefault(),
            "orderId":self.order?.id ?? "",
            "token":User.sharedInstance.oauthToken ?? "",
            "userProfileLocation" : User.sharedInstance.profilePicUrl ?? "",
            "resellPrice":self.amountTxtFld.text ?? ""
            
        ]
        DIWebLayerUserAPI().resellHotel(parameters: params, success: { (response) in
            self.hideLoader()
            let resellVc : ResellConfirmedViewController = UIStoryboard(storyboard: .profile).initVC()
            self.navigationController?.pushViewController(resellVc, animated: true)
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    @IBAction func confirmationConfirmTapped(_ sender: UIButton) {
        resellConfirmationView.isHidden = true
        resellBGView.isHidden = true
        resellBooking()
    }
    
    @IBAction func confirmationCancelTapped(_ sender: UIButton) {
        resellConfirmationView.isHidden = true
        resellBGView.isHidden = true
    }
    
    @IBAction func confirmResellTapped(_ sender: UIButton) {
        resellInputView.isHidden = true
        resellConfirmationView.isHidden = false
    }
    
    @IBAction func cancelResellTapped(_ sender: UIButton) {
        resellInputView.isHidden = true
        resellBGView.isHidden = true
    }
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

