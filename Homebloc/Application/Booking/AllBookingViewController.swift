//
//  AllBookingViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 09/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
enum BookingStatus:Int{
    case all=0,pending=2,processing=4,comments=3
}

class AllBookingViewController: DIBaseController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentsLbl: UIButton!
    @IBOutlet weak var processingBtn: UIButton!
    @IBOutlet weak var pendingBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var headerTitleLbl: UILabel!
    
    var languageModel:LanguageLabel?
    var bookingStatus:BookingStatus = BookingStatus.all
    var orderList : [Order] = []
    var filteredOrderList : [Order] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLangugae()
        // Do any additional setup after loading the view.
    }
    
    func setUpLangugae(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
            if let all = languageModel?.lBL_ALL,let booking = languageModel?.lBL_BOOKING{
                headerTitleLbl.text = all + " " + booking
            }
            commentsLbl.setTitle(language.lBL_COMMENTS, for: .normal)
            processingBtn.setTitle(language.lBL_PROCESSING, for: .normal)
            pendingBtn.setTitle(language.lBL_PENDING, for: .normal)
            allBtn.setTitle(language.lBL_ALL, for: .normal)
        }
        intializer()
        getOrder()
    }
    
    func intializer(){
        switch bookingStatus {
        case .all:
            setBackgroundAndTextColor(button:commentsLbl,isSelected:false)
            setBackgroundAndTextColor(button:processingBtn,isSelected:false)
            setBackgroundAndTextColor(button:pendingBtn,isSelected:false)
            setBackgroundAndTextColor(button:allBtn,isSelected:true)
        case .pending:
            setBackgroundAndTextColor(button:commentsLbl,isSelected:false)
            setBackgroundAndTextColor(button:processingBtn,isSelected:false)
            setBackgroundAndTextColor(button:pendingBtn,isSelected:true)
            setBackgroundAndTextColor(button:allBtn,isSelected:false)
        case .processing:
            setBackgroundAndTextColor(button:commentsLbl,isSelected:false)
            setBackgroundAndTextColor(button:processingBtn,isSelected:true)
            setBackgroundAndTextColor(button:pendingBtn,isSelected:false)
            setBackgroundAndTextColor(button:allBtn,isSelected:false)
        case .comments:
            setBackgroundAndTextColor(button:commentsLbl,isSelected:true)
            setBackgroundAndTextColor(button:processingBtn,isSelected:false)
            setBackgroundAndTextColor(button:pendingBtn,isSelected:false)
            setBackgroundAndTextColor(button:allBtn,isSelected:false)
            bookingStatus = .pending
        }
    }
    
    func setBackgroundAndTextColor(button:UIButton,isSelected:Bool){
        button.backgroundColor = isSelected ? #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1) : .white
        button.setTitleColor(isSelected ? .white : #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1), for: .normal)
        if isSelected{
            if let buttonTitle = button.titleLabel?.text,let booking = languageModel?.lBL_BOOKING{
                headerTitleLbl.text = buttonTitle + " " + booking
            }
        }
    }
    
    func getOrder(){
        let params =
            [
                "token":User.sharedInstance.oauthToken ?? "",
                "lang":self.getLangugaeFromDefault()
        ]
        self.showLoader()
        DIWebLayerUserAPI().getOrderList(parameters: params,page:bookingStatus.rawValue, success: { (response) in
            self.hideLoader()
            self.orderList = response ?? []
            self.filteredOrderList = response ?? []
            self.filterOrders()
            self.tableView.reloadData()
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    
    @IBAction func backtapped(_ sender: UIButton) {
        if self.navigationController?.viewControllers.count ?? 0 > 2 {
            navigateToTabBarVC()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func allTapped(_ sender: UIButton) {
        setBackgroundAndTextColor(button:commentsLbl,isSelected:false)
        setBackgroundAndTextColor(button:processingBtn,isSelected:false)
        setBackgroundAndTextColor(button:pendingBtn,isSelected:false)
        setBackgroundAndTextColor(button:allBtn,isSelected:true)
        bookingStatus = .all
        filterOrders()
    }
    
    @IBAction func pendingTapped(_ sender: UIButton) {
        setBackgroundAndTextColor(button:commentsLbl,isSelected:false)
        setBackgroundAndTextColor(button:processingBtn,isSelected:false)
        setBackgroundAndTextColor(button:pendingBtn,isSelected:true)
        setBackgroundAndTextColor(button:allBtn,isSelected:false)
        bookingStatus = .pending
        filterOrders()
    }
    
    @IBAction func processingTapped(_ sender: UIButton) {
        setBackgroundAndTextColor(button:commentsLbl,isSelected:false)
        setBackgroundAndTextColor(button:processingBtn,isSelected:true)
        setBackgroundAndTextColor(button:pendingBtn,isSelected:false)
        setBackgroundAndTextColor(button:allBtn,isSelected:false)
        bookingStatus = .processing
        filterOrders()
    }
    
    @IBAction func commentTapped(_ sender: UIButton) {
        setBackgroundAndTextColor(button:commentsLbl,isSelected:true)
        setBackgroundAndTextColor(button:processingBtn,isSelected:false)
        setBackgroundAndTextColor(button:pendingBtn,isSelected:false)
        setBackgroundAndTextColor(button:allBtn,isSelected:false)
        bookingStatus = .pending
        filterOrders()
    }
    
    func filterOrders(){
        if bookingStatus == .all {
            filteredOrderList = self.orderList
        } else {
            filteredOrderList = orderList.filter({$0.status == "\(bookingStatus.rawValue)"})
        }
        tableView.reloadData()
    }
    
    @objc func cancelOrder(_ sender: UIButton){
        
    }
    
    @objc func commentOrder(_ sender: UIButton){
        let order = filteredOrderList[sender.tag]
        let commentVC:RatingViewController = UIStoryboard(storyboard: .hotel).initVC()
        commentVC.modalPresentationStyle = .overCurrentContext
        commentVC.orderId = order.id ?? ""
        self.present(commentVC, animated: true, completion: nil)
    }
    
}
extension AllBookingViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.filteredOrderList.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelCell") as? HotelCell else {
            return UITableViewCell()
        }
        cell.initOrderCell(order: filteredOrderList[indexPath.section])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as? FooterCell else {
            return UIView()
        }
        cell.cancelBtn.setTitle("  " + (languageModel?.lBL_CANCEL?.uppercased() ?? "") + "  ", for: .normal)
        cell.commentBtn.setTitle("  " + (languageModel?.lBL_COMMENTS?.uppercased() ?? "") + "  ", for: .normal)
        cell.initOrderCell(order:filteredOrderList[section])
        cell.cancelBtn.tag = section
        cell.commentBtn.tag = section
        cell.cancelBtn.addTarget(self, action: #selector(cancelOrder(_:)), for: .touchUpInside)
        cell.commentBtn.addTarget(self, action: #selector(commentOrder(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookingDetailVC:BookingDetailViewController = UIStoryboard(storyboard: .profile).initVC()
        bookingDetailVC.order = filteredOrderList[indexPath.section]
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    
}
