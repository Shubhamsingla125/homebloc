//
//  ResellConfirmedViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 18/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class ResellConfirmedViewController: DIBaseController {

    @IBOutlet weak var headerTitleLbl: UILabel!
    @IBOutlet weak var reservationTransferLbl: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var languageModel:LanguageLabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLangugae()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmTapped(_ sender: UIButton) {
        navigateToTabBarVC()
    }
    
    func setUpLangugae(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
            headerTitleLbl.text = "RESELL CONFIRMED"
            reservationTransferLbl.text = "RESELLABLE RESERVATION HAS BEEN TRANSFERED TO LUCKY POOL"
            confirmBtn.setTitle(language.lBL_CONFIRM ?? "", for: .normal)
        }
    }

}
