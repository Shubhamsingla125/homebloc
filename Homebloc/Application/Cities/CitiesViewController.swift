//
//  CitiesViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 01/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
enum CitiesType:Int {
    case domestic,international
}
enum CitiesSection:Int,CaseCountable{
    case currentCity,history,popularCities
}
protocol CitiesDelegate {
    func selectCity(city:City)
}
class CitiesViewController: DIBaseController {
    
    @IBOutlet weak var internationalView: UIView!
    @IBOutlet weak var domesticView: UIView!
    @IBOutlet weak var internationalBtn: UIButton!
    @IBOutlet weak var domesticBtn: UIButton!
    @IBOutlet weak var collectionview: UICollectionView!
    
    var citiesList : [City] = []
    var filteredList : [City] = []
    var selectedCityType = 0
    var selectedCityList : [City] = []
    var delegate : CitiesDelegate?
    var selectedCIty : City?
     var languageModel : LanguageLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLanguage()
        internationalView.isHidden = true
        domesticView.isHidden = true
        getCitiesList()
        if let selectedCityList = Utility.getSelectedCities() {
            self.selectedCityList = selectedCityList
        }
        // Do any additional setup after loading the view.
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            internationalBtn.setTitle(lblsValue.lBL_INTERNATIONAL, for: .normal)
            domesticBtn.setTitle(lblsValue.lBL_DOMESTIC, for: .normal)
        }
    }
    
    func getCitiesList(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader()
            DIWebLayerUserAPI().getCities(parameters: nil, language: languageCode, success: { (response) in
                self.hideLoader()
                self.citiesList = []
                if let list = response{
                    self.citiesList = list
                }
                self.filterDomesticCity()
                self.collectionview.reloadData()
                self.selectBtn(tag:self.selectedCityType)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    func filterDomesticCity(){
        if let countryCode = Defaults.shared.get(forKey: .countryCode) as? String {
          filteredList = citiesList.filter({$0.country_code?.lowercased() == countryCode.lowercased()})
            collectionview.reloadData()
        }
    }
    
    func filterInternationalCity(){
        if let countryCode = Defaults.shared.get(forKey: .countryCode) as? String {
            filteredList = citiesList.filter({$0.country_code?.lowercased() != countryCode.lowercased()})
            collectionview.reloadData()
        }
    }
    
    func selectBtn(tag:Int){
        if tag == CitiesType.domestic.rawValue {
            internationalView.isHidden = true
            domesticView.isHidden = false
            filterDomesticCity()
        } else {
            internationalView.isHidden = false
            domesticView.isHidden = true
            filterInternationalCity()
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func getCitiesTapped(_ sender: UIButton) {
        selectBtn(tag:sender.tag)
    }
    
    @objc func selectCityTapped(_ target:UIButton){
        let city = filteredList[target.tag]
        let hotelTagVC : HotelTagViewController = UIStoryboard(storyboard: .hotel).initVC()
        hotelTagVC.city = city
        self.navigationController?.pushViewController(hotelTagVC, animated: true)
    }
}
extension CitiesViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return CitiesSection.caseCount
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind{
        case UICollectionElementKindSectionHeader:

            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "CityTitleHeaderView",for: indexPath) as? CityTitleHeaderView
                else {
                    fatalError("Invalid view type")
            }
            if let citySection = CitiesSection(rawValue: indexPath.section){
                switch citySection{
                case .currentCity:
                    headerView.titleHeaderLbl.text = languageModel?.lBL_CURRENT_CITY
                case .history:
                    headerView.titleHeaderLbl.text = languageModel?.lBL_HISTORY
                case .popularCities:
                    headerView.titleHeaderLbl.text = languageModel?.lBL_POPULAR_CITY
                }
            }
            
            return headerView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let citySection = CitiesSection(rawValue: section){
            switch citySection{
            case .currentCity:
                return selectedCIty != nil ? 1 : 0
            case .history:
                return selectedCityList.count
            case .popularCities:
                return filteredList.count
           
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CitiesCollectionViewCell", for: indexPath) as? CitiesCollectionViewCell else {
            return UICollectionViewCell()
        }
        if let citySection = CitiesSection(rawValue: indexPath.section){
            switch citySection{
            case .currentCity:
                cell.citiesBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1)
                cell.citiesBtn.setTitle(selectedCIty?.cityname ?? "", for: .normal)
            case .history:
                cell.citiesBtn.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.7450980392, blue: 0.7215686275, alpha: 1)
                cell.citiesBtn.setTitle(selectedCityList[indexPath.item].cityname ?? "", for: .normal)
            case .popularCities:
                cell.citiesBtn.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.7450980392, blue: 0.7215686275, alpha: 1)
                cell.citiesBtn.setTitle(filteredList[indexPath.item].cityname ?? "", for: .normal)
            }
        }
        cell.citiesBtn.tag = indexPath.item
//        cell.citiesBtn.addTarget(self, action: #selector(selectCityTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3-7, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let citySection = CitiesSection(rawValue: indexPath.section){
            switch citySection{
            case .currentCity:
                self.navigationController?.popViewController(animated: true)
            case .history:
                let city = selectedCityList[indexPath.row]
                self.delegate?.selectCity(city: city)
                self.navigationController?.popViewController(animated: true)
            case .popularCities:
                let city = filteredList[indexPath.row]
                if !selectedCityList.contains(where: { $0.citycode == city.citycode }) {
                    if var list = Defaults.shared.get(forKey: .selectedCities) as? [[String:Any]] {
                        list.append(city.dictionary ?? [String:Any]())
                        Defaults.shared.set(value: list, forKey: .selectedCities)
                    } else {
                        Defaults.shared.set(value: [city.dictionary], forKey: .selectedCities)
                    }
                }
                self.delegate?.selectCity(city: city)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


class CityTitleHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var titleHeaderLbl : UILabel!

}


extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
