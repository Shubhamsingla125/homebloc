//
//  RegistrationViewController.swift
//  BaseProject
//
//  Created by Navpreet Gogana on 16/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit
import Kingfisher


class RegistrationController: DIBaseController {
  
  @IBOutlet weak var imgUserProfile: UIImageView!
  @IBOutlet weak var textFieldFirstName: UITextField!
  @IBOutlet weak var textFieldLastName: UITextField!
  @IBOutlet weak var textFieldEmailAddress: UITextField!
  @IBOutlet weak var textFieldPassword: UITextField!
  @IBOutlet weak var textFieldDOB: CustomTextField!
  @IBOutlet weak var textFieldGender: UITextField!
  
  @IBOutlet weak var submitButton: UIButton!
  
  var user: User!
  var photoManager:PhotoManager!
  var isImageChanged = false
  //MARK:- ViewController Life Cycle
  override func viewDidLoad() {
    super.viewDidLoadWithKeyboardManager(viewController: self)
    textFieldDOB.delegate = self
    textFieldGender.delegate = self
    textFieldPassword.delegate = self
    setData()
    
    // Do any additional setup after loading the view.
  }
  
  override func viewDidLayoutSubviews() {
    
    super.viewDidLayoutSubviews()
    imgUserProfile.cornerRadius = imgUserProfile.bounds.width / 2
  }
  
  
  private func setData() {
    
    user = User.sharedInstance
    if user.id != nil {
      self.title = AppMessages.ScreenTitles.editProfile
      textFieldLastName.text = user.lastName
      textFieldFirstName.text = user.firstName
      textFieldEmailAddress.text = user.email
      textFieldDOB.text = user.dateOfBirth
      textFieldGender.text = user.gender
      let url = URL(string: user.profilePicUrl!)
      imgUserProfile.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "AddProfilePic"), options: nil, progressBlock: nil, completionHandler: nil)
      textFieldPassword.placeholder = "Change password"
      submitButton.setTitle(AppMessages.ButtonTitle.update, for: .normal)
      let logoutBarButton = UIBarButtonItem.init(title: AppMessages.ButtonTitle.logout, style: .plain, target: self, action: #selector(logoutButtonTapped))
      self.navigationItem.setLeftBarButton(logoutBarButton, animated: true);
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func updateData(){
    User.sharedInstance.firstName = textFieldFirstName.text
    User.sharedInstance.lastName = textFieldLastName.text
    User.sharedInstance.email = textFieldEmailAddress.text
    User.sharedInstance.dateOfBirth = textFieldDOB.text
    User.sharedInstance.gender = textFieldGender.text
    self.setData()
  }
  
  //MARK:- IBActions
  @IBAction func showPasswordButtonTapped(_ sender: UIButton) {
    self.hideShowPasswordButton(sender)
  }
  
  @IBAction func submitButtonTapped(_ sender: UIButton) {
    
    self.submitAction(sender)
  
  }
  
  func submitAction(_ sender: UIButton) {
    if isFormValid() {
      if (sender.titleLabel?.text?.contains(AppMessages.ButtonTitle.update))!{
        callUpdateAPI(completion: { (message, finish) in
          self.updateData()
          self.showAlert(message: message, okCall: {
            if !(self.submitButton.titleLabel!.text!.contains(AppMessages.ButtonTitle.update)) {
              _ = self.navigationController?.popViewController(animated: true)
            }
          })
        })
        return
      }
      self.view.endEditing(true)
      DILog.print(items: "Now you can proceed with login api")
      callRegsiterAPI(completion: { (message, finish) in      
        
      })
    }
  }
  
  @IBAction func addImageButtonTapped(_ sender: UIButton) {
   /* photoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, callback: { (pickedImage) in
      self.imgUserProfile.image = pickedImage
      self.isImageChanged = true
      
      /*
       self.uploadImageOnFirebase(completion: { (imageUrl) in
       
       if imageUrl != nil {
       User.sharedInstance.profilePicUrl = imageUrl!
       print(User.sharedInstance.profilePicUrl!)
       isImageChanged = true
       }
       }) */
    })*/
  }
  
  @IBAction func dobTextfieldTouchDown(_ sender: UITextField) {
    // self.addDatePicker(textfield: sender)
    
  }
  @IBAction func genderTextfieldTouchDown(_ sender: UITextField) {
    self.presentActionSheet(actionSheetTitle: AppMessages.AlertTitles.chooseGender, message: AppMessages.AlertTitles.genderMessage, actionsBtnTitles: ["Male", "Female", "Transgender"], cancelTitle: AppMessages.AlertTitles.Cancel)
  }
  
  
  func hideShowPasswordButton(_ sender: UIButton) {
    
    if textFieldPassword.isSecureTextEntry{
      sender.setTitle(Constant.ButtonName.hide, for: .normal)
    }
    else{
      sender.setTitle(Constant.ButtonName.show, for: .normal)
    }
    textFieldPassword.isSecureTextEntry = !textFieldPassword.isSecureTextEntry
  }
    
  //MARK:- Private Methods  
  fileprivate func isFormValid() -> Bool {
    print(submitButton.titleLabel?.text ?? "")

    var message : String?
    if (textFieldFirstName.text?.isBlank)! {
      message = AppMessages.UserName.emptyFirstname
    }
    else if !(textFieldFirstName.text?.isValidInRange(maxLength: 25))! {
      message = AppMessages.UserName.maxLengthFirstname
    }
    else if (textFieldLastName.text?.isBlank)! {
      message = AppMessages.UserName.emptyLastname
    }
    else if !(textFieldLastName.text?.isValidInRange(maxLength: 25))! {
      message = AppMessages.UserName.maxLengthLastname
    }
    else if (textFieldEmailAddress.text?.isBlank)! {
      message = AppMessages.Email.emptyEmail
    }
    else if !(textFieldEmailAddress.text?.isValidEmail)! {
      message = AppMessages.Email.invalidEmail
    }
    else if (textFieldPassword.text?.isBlank)! && !(submitButton.titleLabel!.text!.contains(AppMessages.ButtonTitle.update)) {
      message = AppMessages.Password.emptyPassword
    }
    else if !(textFieldPassword.text?.isValidPassword)! && !(textFieldPassword.text?.isBlank)! {
      message = AppMessages.Password.invalidPassword
    }
    if message != nil {
      self.showAlert( message: message)
      return false
    }
    return true
  }
  
  fileprivate func callUpdateAPI(completion:@escaping (_ message: String,_ finish: Bool) -> ()) {
    showLoader()
    if isImageChanged {
      self.uploadImageOnFirebase(completion: { (imageUrl) in
        if (imageUrl != nil) {
          User.sharedInstance.profilePicUrl = imageUrl
          self.proceedWithUpdate(completion: { (message, finish) in
            completion(message, true)
          })
        }
      })

//      DIFirebaseImageManager().removeFromServer(imageName: User.sharedInstance.firebaseProfileImageName!, atIndex: nil) { (finished) in
//        
//        self.uploadImageOnFirebase(completion: { (imageUrl) in
//          if (imageUrl != nil) {
//            User.sharedInstance.profilePicUrl = imageUrl
//            self.proceedWithUpdate(completion: { (message, finish) in
//              completion(message, true)
//            })
//          }
//        })
//      }
//      return
    }
    else{
    self.proceedWithUpdate(completion: { (message, finish) in
      completion(message, true)
    })
    }
  }
  
  fileprivate func getUpdatedUserInfo() -> [String: Any] {
    
    var updateProfile = UpdateProfile()
    updateProfile.firstName = textFieldFirstName.text ?? ""
    updateProfile.lastName = textFieldLastName.text ?? ""
    updateProfile.email = textFieldEmailAddress.text ?? ""
    updateProfile.password = textFieldPassword.text ?? ""
    updateProfile.dateOfBirth = textFieldDOB.text ?? ""
    updateProfile.gender = textFieldGender.text ?? ""
    if isImageChanged {
      updateProfile.profilePicUrl = User.sharedInstance.profilePicUrl!
      updateProfile.firebaseProfileImageName = User.sharedInstance.firebaseProfileImageName!
    }
    return updateProfile.getDictionary()!
  }
  
  
  fileprivate func proceedWithUpdate(completion:@escaping (_ message: String,_ finish: Bool) -> ()) {
    DIWebLayerUserAPI().updateUserProfile(parameters: self.getUpdatedUserInfo(), success: { (response) in
      self.hideLoader()
      print(response)
      completion(response, true)
      
    }) { (error) in
      self.hideLoader()
      completion(error.message!, false)
      self.showAlert(message: error.message!)
    }
  }
  
  
  fileprivate func callRegsiterAPI(completion:@escaping (_ message: String,_ finish: Bool) -> ()){
    showLoader()
    if imgUserProfile.image != #imageLiteral(resourceName: "AddProfilePic") {
      self.uploadImageOnFirebase(completion: { (imageUrl) in
        if imageUrl != nil{
          User.sharedInstance.profilePicUrl = imageUrl
          self.proceedWithRegistration(completion: { (message, finish) in
            completion(message, finish)
            
          })
        }
      })
    } else {
      self.proceedWithRegistration(completion: { (message, finish) in
          completion(message, finish)
      })
    }
  }
  
  func getRegistrationDict () -> [String: Any]? {
    var registrationDict = Registration()
    registrationDict.firstName = textFieldFirstName.text ?? ""
    registrationDict.lastName = textFieldLastName.text ?? ""
    registrationDict.email = textFieldEmailAddress.text ?? ""
    registrationDict.password = textFieldPassword.text ?? ""
    if User.sharedInstance.profilePicUrl != nil {
      registrationDict.profilePicUrl = User.sharedInstance.profilePicUrl!
      registrationDict.firebaseProfileImageName = User.sharedInstance.firebaseProfileImageName!
    }
    return registrationDict.getDictionary()
  }
  
  fileprivate func proceedWithRegistration(completion:@escaping (_ message: String,_ finish: Bool) -> ()) {
    DIWebLayerUserAPI().registation(parameters: self.getRegistrationDict(), success: { (response) in
      self.hideLoader()
      print(response)
      completion(response, true)
    })
    {
      self.hideLoader()
      self.showAlert(withError: $0)
   //   completion("", false)
    }
  }
  
  @objc private func logoutButtonTapped() {
    self.showAlert(message: AppMessages.AlertTitles.logoutMessage, okayTitle: AppMessages.AlertTitles.Yes, cancelTitle: AppMessages.AlertTitles.No, okCall: {
      UserManager.logout()
      _ = self.navigationController?.popViewController(animated: true)
    })
  }
  
  //MARK:- Firebase Image Upload Methods
  fileprivate func uploadImageOnFirebase(completion:@escaping (_ imageUrl: String?) ->()) {
    
    if imgUserProfile.image != #imageLiteral(resourceName: "AddProfilePic") && !(textFieldEmailAddress.text?.isBlank)! {
      
      LoginFirebaseUser.signIn(email: textFieldEmailAddress.text!, password: textFieldEmailAddress.text!, success: { (finished, error) in
        if (error.code == nil) {
          return
        }
        
        //Check will set a new profile image name on firebase, if uploading on first time, but in update time, it will use old imagename and reupload on firebase
        if User.sharedInstance.firebaseProfileImageName == nil {
        User.sharedInstance.firebaseProfileImageName = (self.textFieldEmailAddress.text?.replace("@", replacement: ""))! + String(Constant.Time().now())
        User.sharedInstance.firebaseProfileImageName = User.sharedInstance.firebaseProfileImageName?.replace(".", replacement: "")
        }
        let firImageName = User.sharedInstance.firebaseProfileImageName ?? ""
        // print(User.sharedInstance.firebaseProfileImageName!)
         print(firImageName)
        
        DIFirebaseImageManager().uploadImage(image: self.imgUserProfile.image!, withQuality:0.8 , withName: firImageName, completion: { (apiResponse, imageUrl, error) in
          
          switch apiResponse{
          case .success:
            completion(imageUrl)
            break
            
          case .failure :
            completion(nil)
            break
          }
        })
      })
    }
  }
  
  //MARK:TextField Delegate
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
    if textField == textFieldGender {
      return false
    }else if textField == textFieldDOB  {
      let datePickerView:UIDatePicker = UIDatePicker()
      //      let currentDate: NSDate = NSDate()
      let components: NSDateComponents = NSDateComponents()
      
      datePickerView.datePickerMode = UIDatePickerMode.date
      let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
      
      components.year = -27
      let minDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: NSDate() as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
      datePickerView.minimumDate = minDate as Date
      datePickerView.date = datePickerView.minimumDate!
      textField.inputView = datePickerView
      datePickerView.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
      return true
    }
    
    return true
  }
  //MARK:Custom Methods 
    @objc func datePickerValueChanged(sender:UIDatePicker) {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    //28 April - avoid minimum date for the birthday
    /*if sender.date > sender.minimumDate!{
      sender.date = sender.minimumDate!
      return
    }*/
    textFieldDOB.text = dateFormatter.string(from: sender.date)
  }
    
  override func actionSheetButtonTapped(index: Int, titles: [String]) {
    textFieldGender.text = titles[index]
  }
    
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    return true
  }
}

// MARK: - Textfield Delegation
extension RegistrationController: UITextFieldDelegate {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if textField == textFieldPassword {
      if string == " " {
        return false
      }
    }
    return true
  }
  
  
}




