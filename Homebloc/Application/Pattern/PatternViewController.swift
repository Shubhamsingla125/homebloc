//
//  PatternViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 03/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import CCGestureLock
import LocalAuthentication
class PatternViewController: DIBaseController {
    
    @IBOutlet weak var gestureLock : CCGestureLock!
    @IBOutlet weak var controlView : UIView!
    @IBOutlet weak var leftButton : UIButton!
    @IBOutlet weak var rightButton : UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var skipBtn: UIButton!
    var languageModel : LanguageLabel?
    
    private enum LockMode {
        case unlocked
        case locked
    }
    private var lockMode : LockMode {
        get {
            return Password.lockSequence == nil ? .unlocked : .locked
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLanguage()
        thumbnailLogin()
      
        // Do any additional setup after loading the view.
        skipBtn.isHidden = lockMode == .locked ? true : false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        setupGestureLock()
        setupControlPanel()
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            skipBtn.setTitle(" \(lblsValue.lBL_SKIP_PASSWORD ?? "") ", for: .normal)
            languageModel = lblsValue
            headerLbl.text = lblsValue.lBL_PLEASE_ENTER_PASSWORD
        }
    }
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func thumbnailLogin(){
        let myContext = LAContext()
        let myLocalizedReasonString = "\(languageModel?.lBL_CONFIRM_FINGERPRINT_TO_CONTINUE ?? "")\n\(languageModel?.lBL_TOUCH_PHONE_SENSOR ?? "")"
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    DispatchQueue.main.async {
                        if success {
                            // User authenticated successfully, take appropriate action
                            print("Awesome!!... User authenticated successfully")
                             Password.isUserLogin = true
                            self.navigateToTabBarVC()
                        } else {
                            // User did not authenticate successfully, look at error and take appropriate action
                            self.setupGestureLock()
                            self.setupControlPanel()
                             print("Sorry!!... User did not authenticate successfully")
                        }
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
                setupGestureLock()
                setupControlPanel()
                print("Sorry!!.. Could not evaluate policy.")
            }
        } else {
            // Fallback on earlier versions
            setupGestureLock()
            setupControlPanel()
            print("Ooops!!.. This feature is not supported.")
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    private func setupControlPanel() {
        controlView.isHidden = lockMode == .locked
        headerLbl.text = languageModel?.lBL_PLEASE_ENTER_PASSWORD
        
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        self.navigateToTabBarVC()
    }
    
    private func setupGestureLock() {
        
        // Set number of sensors
        gestureLock.lockSize = (3, 3)
        
        // Sensor grid customisations
        gestureLock.edgeInsets = UIEdgeInsetsMake(30, 30, 30, 30)
        
        // Sensor point customisation (normal)
        gestureLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 1,
            color: .red,
            forState: .normal
        )
        gestureLock.setSensorAppearance(
            type: .outer,
            color: .red,
            forState: .normal
        )
        
        // Sensor point customisation (selected)
        gestureLock.setSensorAppearance(
            type: .inner,
            radius: 3,
            width: 5,
            color: .red,
            forState: .selected
        )
        gestureLock.setSensorAppearance(
            type: .outer,
            radius: 30,
            width: 5,
            color: .red,
            forState: .selected
        )
        
        // Sensor point customisation (wrong password)
        gestureLock.setSensorAppearance(
            type: .inner,
            radius: 3,
            width: 5,
            color: .red,
            forState: .error
        )
        gestureLock.setSensorAppearance(
            type: .outer,
            radius: 30,
            width: 5,
            color: .red,
            forState: .error
        )
        
        // Line connecting sensor points (normal/selected)
        [CCGestureLock.GestureLockState.normal, CCGestureLock.GestureLockState.selected].forEach { (state) in
            gestureLock.setLineAppearance(
                width: 5.5,
                color: UIColor.red.withAlphaComponent(0.5),
                forState: state
            )
        }
        
        // Line connection sensor points (wrong password)
        gestureLock.setLineAppearance(
            width: 5.5,
            color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        
        gestureLock.addTarget(
            self,
            action: #selector(gestureComplete),
            for: .gestureComplete
        )
        
    }
    
    @objc func buttonTapped(button: UIButton) {
        if button == rightButton {
            let confirmPatternVc : ConfirmPatternViewController = UIStoryboard(storyboard: .main).initVC()
            confirmPatternVc.pattern = gestureLock.lockSequence
            self.navigationController?.pushViewController(confirmPatternVc, animated: true)
        } else {
            gestureLock.gestureLockState = .normal
        }
        
    }
    
    @objc func gestureComplete(gestureLock: CCGestureLock) {
        
        if lockMode == .locked {
            
            if Password.lockSequence! == gestureLock.lockSequence {                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                   self.navigateToTabBarVC()
                })
            } else {
                self.showAlert(message:languageModel?.lBL_PLEASE_ENTER_VALID_PASSWORD)
                gestureLock.gestureLockState = .error
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    gestureLock.gestureLockState = .normal
                })
            }
            
        } else {
            let confirmPatternVc : ConfirmPatternViewController = UIStoryboard(storyboard: .main).initVC()
            confirmPatternVc.pattern = gestureLock.lockSequence
            self.navigationController?.pushViewController(confirmPatternVc, animated: true)
//            enableButtons(true)
        }
    }
}

struct Password {
    
    static let passwordKey = "password"
    static let isLogin = "login"
    
    static var isUserLogin : Bool?{
        get {
            return UserDefaults.standard.value(forKey: isLogin) as? Bool
        }
        set (lockSequence) {
            UserDefaults.standard.set(lockSequence, forKey: isLogin)
        }
    }
    
    static var lockSequence : [NSNumber]? {
        get {
            return UserDefaults.standard.array(forKey: passwordKey) as? [NSNumber]
        }
        set (lockSequence) {
            UserDefaults.standard.set(lockSequence, forKey: passwordKey)
        }
    }
}
