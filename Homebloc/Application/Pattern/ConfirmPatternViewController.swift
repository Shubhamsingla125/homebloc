//
//  ConfirmPatternViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 04/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import CCGestureLock

class ConfirmPatternViewController: DIBaseController {
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var gestureLock : CCGestureLock!
  
    var pattern:[NSNumber]?
   var languageModel : LanguageLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpLanguage()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        setupGestureLock()
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            headerLbl.text = lblsValue.lBL_CONFIRM
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    private func setupGestureLock() {
        
        // Set number of sensors
        gestureLock.lockSize = (3, 3)
        
        // Sensor grid customisations
        gestureLock.edgeInsets = UIEdgeInsetsMake(30, 30, 30, 30)
        
        // Sensor point customisation (normal)
        gestureLock.setSensorAppearance(
            type: .inner,
            radius: 5,
            width: 1,
            color: .red,
            forState: .normal
        )
        gestureLock.setSensorAppearance(
            type: .outer,
            color: .red,
            forState: .normal
        )
        
        // Sensor point customisation (selected)
        gestureLock.setSensorAppearance(
            type: .inner,
            radius: 3,
            width: 5,
            color: .red,
            forState: .selected
        )
        gestureLock.setSensorAppearance(
            type: .outer,
            radius: 30,
            width: 5,
            color: .red,
            forState: .selected
        )
        
        // Sensor point customisation (wrong password)
        gestureLock.setSensorAppearance(
            type: .inner,
            radius: 3,
            width: 5,
            color: .red,
            forState: .error
        )
        gestureLock.setSensorAppearance(
            type: .outer,
            radius: 30,
            width: 5,
            color: .red,
            forState: .error
        )
        
        // Line connecting sensor points (normal/selected)
        [CCGestureLock.GestureLockState.normal, CCGestureLock.GestureLockState.selected].forEach { (state) in
            gestureLock.setLineAppearance(
                width: 5.5,
                color: UIColor.red.withAlphaComponent(0.5),
                forState: state
            )
        }
        
        // Line connection sensor points (wrong password)
        gestureLock.setLineAppearance(
            width: 5.5,
            color: UIColor.red.withAlphaComponent(0.5),
            forState: .error
        )
        
        gestureLock.addTarget(
            self,
            action: #selector(gestureComplete),
            for: .gestureComplete
        )
        
    }
    @objc func gestureComplete(gestureLock: CCGestureLock) {
            if pattern! == gestureLock.lockSequence {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    Password.lockSequence = gestureLock.lockSequence
                   self.navigateToTabBarVC()
                })
            } else {
                gestureLock.gestureLockState = .error
                self.showAlert(message:languageModel?.lBL_PLEASE_ENTER_VALID_PASSWORD
                )
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    gestureLock.gestureLockState = .normal
                })
            }
        
    }
}
