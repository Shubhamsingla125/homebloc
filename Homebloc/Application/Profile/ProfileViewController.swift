//
//  ProfileViewController.swift
//  BaseProject
//
//

import UIKit
enum AccountSettingSection:Int,CaseCountable {
    case dividend=0,address,invitation,event,socialEvent,feedback
    func getImageAndName()->(name:String,image:UIImage){
        guard let languageLbls = Utility.getLanguageLabelModel() else {
            return (name:"",image:UIImage())
        }
        switch self {
        case .dividend:
            return (name:languageLbls.lBL_DEVIDE_END ?? "",image:#imageLiteral(resourceName: "dividend"))
        case .address:
            return (name:languageLbls.lBL_ETH_ADDRESS ?? "",image:#imageLiteral(resourceName: "ethAddress"))
        case .invitation:
            return (name:languageLbls.lBL_INVITATION ?? "",image:#imageLiteral(resourceName: "profileinvitation"))
        case .event:
            return (name:languageLbls.lBL_EVENTS ?? "",image:#imageLiteral(resourceName: "event"))
        case .socialEvent:
            return (name:languageLbls.lBL_SOCAIL_EVENTS ?? "",image:#imageLiteral(resourceName: "socialEvent"))
        case .feedback:
            return (name:languageLbls.lBL_FEEDBACK_TO_APP ?? "",image:#imageLiteral(resourceName: "feedback"))
        }
    }
}

class ProfileViewController: DIBaseController {
    @IBOutlet weak var processingLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var pendingLbl: UILabel!
    @IBOutlet weak var allLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var accountSettingLbl: UILabel!
    @IBOutlet weak var reservationLbl: UILabel!
    @IBOutlet weak var profilePicImgVw: UIImageView!
    
    var languageModel:LanguageLabel?
    var userInfo:UserInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLangugae()
        getUserProfile()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = URL(string: userInfo?.userProfileLocation ?? ""){
            profilePicImgVw.kf.setImage(with: url)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpLangugae(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
            processingLbl.text = language.lBL_PROCESSING
            commentLbl.text = language.lBL_COMMENTS
            allLbl.text = language.lBL_ALL
            pendingLbl.text = language.lBL_PENDING
            accountSettingLbl.text = language.lBL_ACCOUNT_SETTING
            reservationLbl.text = language.lBL_MY_RESERVATION
        }
    }

    
    func getUserProfile(){
        self.showLoader()
        let params = [
            "token":User.sharedInstance.oauthToken ?? "",
            "lang":self.getLangugaeFromDefault()
        ]
        DIWebLayerUserAPI().getUserProfile(parameters: params, success: { (response) in
            self.hideLoader()
            self.userInfo = response
            if let url = URL(string: self.userInfo?.userProfileLocation ?? ""){
                self.profilePicImgVw.kf.setImage(with: url)
            }
             let user = User.sharedInstance
            user.profilePicUrl = self.userInfo?.userProfileLocation ?? ""
            UserManager.saveCurrentUser(user: user)
        }) { (error) in
            self.hideLoader()
            self.showAlert()
        }
    }
    
    @IBAction func profileTapped(_ sender: UIButton) {
        let personalInfoVC : PersonalInfoViewController = UIStoryboard(storyboard: .profile).initVC()
        personalInfoVC.userInfo = self.userInfo
        self.navigationController?.pushViewController(personalInfoVC, animated: true)
    }
    
    @IBAction func pendingTapped(_ sender: UIButton) {
        let allBookingVc: AllBookingViewController = UIStoryboard(storyboard: .profile).initVC()
        allBookingVc.bookingStatus = BookingStatus.pending
        self.navigationController?.pushViewController(allBookingVc, animated: true)
    }
    
    @IBAction func allTapped(_ sender: UIButton) {
        let allBookingVc: AllBookingViewController = UIStoryboard(storyboard: .profile).initVC()
        allBookingVc.bookingStatus = BookingStatus.all
        self.navigationController?.pushViewController(allBookingVc, animated: true)
    }
    
    @IBAction func processingTapped(_ sender: UIButton) {
        let allBookingVc: AllBookingViewController = UIStoryboard(storyboard: .profile).initVC()
        allBookingVc.bookingStatus = BookingStatus.processing
        self.navigationController?.pushViewController(allBookingVc, animated: true)
    }
    
    @IBAction func commentsTapped(_ sender: UIButton) {
        let allBookingVc: AllBookingViewController = UIStoryboard(storyboard: .profile).initVC()
        allBookingVc.bookingStatus = BookingStatus.comments
        self.navigationController?.pushViewController(allBookingVc, animated: true)
    }

}

extension ProfileViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return AccountSettingSection.caseCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingCell") as? AccountSettingCell else {
            return UITableViewCell()
        }
        if let section = AccountSettingSection(rawValue: indexPath.section){
          cell.settingTypeLbl.text = section.getImageAndName().name
            cell.settingTypeImgVw.image = section.getImageAndName().image
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = .clear
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let section = AccountSettingSection(rawValue: indexPath.section){
            if section == .socialEvent{
                let officalAccVC : OfficalAccountViewController = UIStoryboard(storyboard: .profile).initVC()
                self.navigationController?.pushViewController(officalAccVC, animated: true)
            } else if section == .feedback{
                let feedbackVC : FeedbackViewController = UIStoryboard(storyboard: .profile).initVC()
                self.navigationController?.pushViewController(feedbackVC, animated: true)
            }
        }
    }

    
}
