//
//  ThumbnailPhotoViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 08/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class ThumbnailPhotoViewController: DIBaseController {

    var languageModel:LanguageLabel?
    var imageUrl = ""
    var userinfo : UserInfo?
    @IBOutlet weak var photoImgVw: UIImageView!
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var camerabBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var thumbnaillbl: UILabel!
    
    var cameraPhotoManager:PhotoManager?
    var galleryPhotoManager:PhotoManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLangugae()
        // Do any additional setup after loading the view.
        if let url = URL(string: userinfo?.userProfileLocation ?? ""){
             photoImgVw.kf.setImage(with: url)
        }
    }
    

    func setUpLangugae(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
            thumbnaillbl.text = language.lBL_THUMB_PHOTO
            galleryBtn.setTitle(language.lBL_CHOOSE_FROM_GALLERY?.uppercased(), for: .normal)
            camerabBtn.setTitle(language.lBL_TAKE_IMAGE?.uppercased(), for: .normal)
            cancelBtn.setTitle(language.lBL_CANCEL?.uppercased(), for: .normal)
        }
    }

    @IBAction func galleryTapped(_ sender: UIButton) {
        galleryPhotoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, imagePickerType:ImagePickerOptions.Gallery, currentView: self.view, callback: { (image) in
            self.photoImgVw.image = image
            if let profilePic = image{
                self.uploadProfilePic(image:profilePic)
            }
        })
    }
    
    @IBAction func cameraTapped(_ sender: UIButton) {
        cameraPhotoManager = PhotoManager(navigationController: self.navigationController!, allowEditing: true, imagePickerType:ImagePickerOptions.Camera, currentView: self.view, callback: { (image) in
            self.photoImgVw.image = image
            if let profilePic = image{
                self.uploadProfilePic(image:profilePic)
            }
        })
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func uploadProfilePic(image:UIImage){
        let params = [
            "token":User.sharedInstance.oauthToken ?? "",
            "lang":self.getLangugaeFromDefault()
        ]
        self.showLoader()
        DIWebLayerUserAPI().uploadProfilePic(parameters:params, image: image, success: { (response) in
            if let path = response["path"] as? String{
                self.updateProfile(pathUrl: path)
                self.userinfo?.userProfileLocation = path
            } else{
                self.hideLoader()
            }
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    func updateProfile(pathUrl:String){
        let params = [
            "token":User.sharedInstance.oauthToken ?? "",
            "lang":self.getLangugaeFromDefault(),
            "mobile":userinfo?.mobile ?? "",
            "userProfileLocation":pathUrl,
            "userName":userinfo?.userName ?? ""
        ]
        DIWebLayerUserAPI().updateUserProfile(parameters: params, success: { (response) in
            self.hideLoader()
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    
}
