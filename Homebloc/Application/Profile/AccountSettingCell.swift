//
//  AccountSettingCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 07/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class AccountSettingCell: UITableViewCell {

    @IBOutlet weak var settingTypeLbl: UILabel!
    @IBOutlet weak var settingTypeImgVw: UIImageView!
    
    @IBOutlet weak var personalInfoHeadingLbl: UILabel!
    @IBOutlet weak var personalInfoDataLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initCell(userInfo:UserInfo,section:Int){
        if let infoSection = PersonInfoSection(rawValue: section){
            switch infoSection{
            case .nickname:
                personalInfoDataLbl.text = userInfo.userName
            case .contact:
                personalInfoDataLbl.text = userInfo.mobile
            case .myInvitation:
                personalInfoDataLbl.text = ""
            case .gender:
                personalInfoDataLbl.text = userInfo.sex
            case .socialNetworking:
                personalInfoDataLbl.text = ""
            case .authencation:
                personalInfoDataLbl.text = Utility.getLanguageLabelModel()?.lBL_COMPLETED ?? ""
            case .passcode:
                personalInfoDataLbl.text = ""
            }
        }
    }
}
