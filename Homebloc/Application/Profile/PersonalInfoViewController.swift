//
//  PersonalInfoViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 08/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
enum PersonInfoSection:Int,CaseCountable{
    case nickname,contact,myInvitation,gender,socialNetworking,authencation,passcode
    func getPersonalInfoHeader()->(String){
        guard let languageLbls = Utility.getLanguageLabelModel() else {
            return (header:"") as! (String)
        }
        switch self {
        case .nickname:
            return languageLbls.lBL_NIKNAME ?? ""
        case .contact:
            return languageLbls.lBL_CONTACT ?? ""
        case .myInvitation:
            return languageLbls.lBL_MY_INVITATION ?? ""
        case .gender:
            return languageLbls.lBL_GENDER ?? ""
        case .socialNetworking:
            return languageLbls.lBL_SOCIAL ?? ""
        case .authencation:
            return languageLbls.lBL_AUTHENTICATION ?? ""
        case .passcode:
            return languageLbls.lBL_PASSCODE ?? ""
        }
    }
}

class PersonalInfoViewController: DIBaseController {

    @IBOutlet weak var personalInfoLbl: UILabel!
     @IBOutlet weak var profilePicImgVw: UIImageView!
    @IBOutlet weak var thumbnailLbl: UILabel!
    
    var languageModel:LanguageLabel?
    var userInfo:UserInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLangugae()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let url = URL(string: userInfo?.userProfileLocation ?? ""){
            profilePicImgVw.kf.setImage(with: url)
        }
    }

    func setUpLangugae(){
        if let language = Utility.getLanguageLabelModel(){
            languageModel = language
           personalInfoLbl.text = language.lBL_PERSONAL_INFO
            thumbnailLbl.text = language.lBL_THUMB_PHOTO
        }
    }
    
    @IBAction func selectProfilePicTapped(_ sender: UIButton) {
        let thumbnailVC : ThumbnailPhotoViewController = UIStoryboard(storyboard: .profile).initVC()
        thumbnailVC.userinfo = userInfo
        self.navigationController?.pushViewController(thumbnailVC, animated: true)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PersonalInfoViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return PersonInfoSection.caseCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AccountSettingCell") as? AccountSettingCell else {
            return UITableViewCell()
        }
        if let section = PersonInfoSection(rawValue: indexPath.section),let info = self.userInfo{
            cell.personalInfoHeadingLbl.text = section.getPersonalInfoHeader()
            cell.initCell(userInfo: info,section:indexPath.section)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = .clear
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}
