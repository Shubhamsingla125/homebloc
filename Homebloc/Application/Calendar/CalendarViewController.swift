//
//  CalendarViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 29/03/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import FSCalendar
protocol CalendarDelegate {
    func getCalendarRange(range:[Date]?,selectedDates:Int)
}
class CalendarViewController: UIViewController {

    @IBOutlet weak var calendar: FSCalendar!
    
    private var firstDate: Date?
    private var lastDate: Date?
    
    var datesRange: [Date]?
    var delegate:CalendarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if datesRange != nil{
            firstDate = datesRange?.first
            self.lastDate = datesRange?.count ?? 0 > 1 ? datesRange?.last  : nil  
            setUpSelectedDates()
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func setUpSelectedDates(){
        var daysDiff: Int = 0
        if datesRange?.count == 1{
            calendar.select(firstDate)
        } else if datesRange?.count ?? 0 > 1 {
            if let fromDate = firstDate, let toDate = lastDate {
                daysDiff = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day ?? 0
                for value in 0...daysDiff {
                    let newDate = Calendar.current.date(byAdding: .day, value: value, to: fromDate )
                    calendar.select(newDate)
                }
            }
        }
    }
}
extension CalendarViewController : FSCalendarDelegate,FSCalendarDataSource{
    
    func datesRange(from: Date, to: Date) -> [Date] {
        // in case of the "from" date is more than "to" date,
        // it should returns an empty array:
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // nothing selected:
        if firstDate == nil {
            firstDate = date
            datesRange = [firstDate!]
            delegate?.getCalendarRange(range: datesRange,selectedDates: self.calendar.selectedDates.count)
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        // only first date is selected:
        if firstDate != nil && lastDate == nil {
            // handle the case of if the last date is less than the first date:
            if date <= firstDate! {
                calendar.deselect(firstDate!)
                firstDate = date
                datesRange = [firstDate!]
                delegate?.getCalendarRange(range: datesRange,selectedDates: self.calendar.selectedDates.count)
                self.dismiss(animated: true, completion: nil)
                return
            }
            
            let range = datesRange(from: firstDate!, to: date)
            lastDate = range.last
            for d in range {
                calendar.select(d)
            }
            datesRange = range
            delegate?.getCalendarRange(range: datesRange,selectedDates: self.calendar.selectedDates.count)
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        // both are selected:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            lastDate = nil
            firstDate = nil
            datesRange = []
            delegate?.getCalendarRange(range: datesRange,selectedDates: self.calendar.selectedDates.count)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // both are selected:
        
        // NOTE: the is a REDUANDENT CODE:
        if firstDate != nil && lastDate != nil {
            for d in calendar.selectedDates {
                calendar.deselect(d)
            }
            lastDate = nil
            firstDate = nil
            datesRange = []
            delegate?.getCalendarRange(range: datesRange,selectedDates: self.calendar.selectedDates.count)
            self.dismiss(animated: true, completion: nil)
        }
    }
}
