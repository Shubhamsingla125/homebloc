//
//  SignupViewController.swift
//  Homebloc
//
//  Created by iosdev on 14/01/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import CountryPicker

class SignupViewController: DIBaseController {
    @IBOutlet weak var searchHotelBtn: UIButton!
    @IBOutlet weak var hicomeOnInLbl: UILabel!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var textFieldPhNo: UITextField!
    @IBOutlet weak var textFieldCode: UITextField!
    @IBOutlet weak var textFieldInvitationCode: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldRenterPaswd: UITextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var havAcntLbl: UILabel!
    @IBOutlet weak var signUp: UIButton!
    var languageLbls : [String:Any]?
    var languageModel : LanguageLabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLanguage()
        createUserExperience()
        // Do any additional setup after loading the view.
    }

    func createUserExperience(){
//        Utility.addLeftViewInTxtFld(txtFld: textFieldCode, image: #imageLiteral(resourceName: "dots"))
//        Utility.addLeftViewInTxtFld(txtFld: textFieldInvitationCode, image: #imageLiteral(resourceName: "dots"))
//        Utility.addLeftViewInTxtFld(txtFld: textFieldPassword, image: #imageLiteral(resourceName: "key"))
//        Utility.addLeftViewInTxtFld(txtFld: textFieldRenterPaswd, image: #imageLiteral(resourceName: "key"))
        textFieldCode.rightViewMode = UITextFieldViewMode.always
        textFieldCode.rightView = getAcquireCodeBtn()
        setUpCountryPicker()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissCountryPicker(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            searchHotelBtn.setTitle(lblsValue.lBL_SEARCH_HOTEL, for: .normal)
            textFieldInvitationCode.placeholder = lblsValue.lBL_INVITATION_CODE ?? ""  + "(Optional)"
            textFieldRenterPaswd.placeholder = lblsValue.lBL_PASSWORD
            textFieldCode.placeholder = lblsValue.lBL_ENTER_CODE
            textFieldPhNo.placeholder = lblsValue.lBL_PHONE_NUMBER
            textFieldPassword.placeholder = lblsValue.lBL_PASSWORD
            signInBtn.setTitle(lblsValue.lBL_SIGN_IN, for: .normal)
            havAcntLbl.text = lblsValue.lBL_ALREADY_HAVE_ACCOUNT
            signUp.setTitle(lblsValue.lBL_SIGN_UP?.uppercased(), for: .normal)
        }
    }
    
    @objc func dismissCountryPicker(_ gesture:UITapGestureRecognizer){
        pickerView.isHidden = true
    }
    
    func setUpCountryPicker(){
        //get current country
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
        pickerView.isHidden = true
    }
    
    func getAcquireCodeBtn()->UIView?{
        let acquireBtnView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 40))
        let acquireBtn = UIButton(type: .system)
        acquireBtn.frame = CGRect(x: 5, y: 7, width: 80, height: 25)
        acquireBtn.setTitle(languageModel?.lBL_ACQUIRE_CODE ?? "", for: .normal)
        acquireBtn.titleLabel?.font = .systemFont(ofSize: 10)
        acquireBtn.setTitleColor(.white, for: .normal)
        acquireBtn.addTarget(self, action: #selector(acquireCodeTapped(_:)), for: .touchUpInside)
        acquireBtn.cornerRadius = 12
        acquireBtnView.addSubview(acquireBtn)
        acquireBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1)
        return acquireBtnView
    }
    
    
    @objc func acquireCodeTapped(_ target:UIButton){
        self.view.endEditing(true)
        if(textFieldPhNo.text?.isEmpty ?? false){
            self.showAlert(message:languageModel?.lBL_PLEASE_ENTER_PHONE ?? "")
        } else {
            self.showLoader()
            DIWebLayerUserAPI().getVerificationCode(parameters: nil, mobile:textFieldPhNo.text ?? "", countryCode:countryButton?.titleLabel?.text ?? "", success: { (response) in
                self.hideLoader()
                self.showAlert(message:response)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    @IBAction func selectCountryTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        pickerView.isHidden = false
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        pickerView.isHidden = true
    }
    @IBAction func clearPhoneNoTapped(_ sender: UIButton) {
        textFieldPhNo.text = ""
    }
    
    private func isSignUpFormValid()->Bool{
        var message : String?
        if(textFieldPhNo.text?.isEmpty ?? false){
             message = languageModel?.lBL_PLEASE_ENTER_PHONE ?? ""
        } else if (textFieldCode.text?.isEmpty ?? false) {
           message = languageModel?.lBL_PLEASE_ENTER_CODE ?? ""
        } else if (textFieldPassword.text?.isEmpty ?? false){
           message = languageModel?.lBL_PLEASE_ENTER_PASSWORD ?? ""
        } else if (textFieldPassword.text != textFieldRenterPaswd.text){
            message = languageModel?.lBL_PASSWORD_MISMATCH
        }
        if message != nil{
            self.showAlert(message:message)
            return false
        }
        return true
    }
    
    @IBAction func signUpTapped(_ sender : UIButton){
        self.view.endEditing(true)
        if isSignUpFormValid(){
            self.showLoader()
            DIWebLayerUserAPI().signUp(parameters: nil, mobile: textFieldPhNo.text ?? "", countryCode: countryButton?.titleLabel?.text ?? "", password: textFieldPassword.text ?? "", verifyCode: textFieldCode.text ?? "", success: { (response) in
                self.hideLoader()
                self.showAlert(message:response,okCall:{
                    self.navigationController?.popViewController(animated: true)
                })
            }, failure: { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            })
        }
    }
    
    @IBAction func signInTapped(_ sender : UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func googleSignInTapped(_ sender : UIButton){
        
    }
    
    @IBAction func fbSignInTapped(_ sender : UIButton){
        
    }
}
extension SignupViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView.isHidden = true
        return true
    }
}

extension SignupViewController : CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        countryButton.setTitle(phoneCode, for: .normal)
        
    }
}
