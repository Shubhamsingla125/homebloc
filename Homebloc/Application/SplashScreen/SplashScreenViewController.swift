//
//  SplashScreenViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 01/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import iOSDropDown
class SplashScreenViewController: DIBaseController {
    
    @IBOutlet weak var dropDown:DropDown!
    
    var languageList : [Language] = []
    var selectedLanguage : Language?
    override func viewDidLoad() {
        super.viewDidLoad() 
        if let languageLbls = Defaults.shared.get(forKey: .languageLabel) as? [String:Any]{
            let loginVC : LoginViewController = UIStoryboard(storyboard: .main).initVC()
            loginVC.languageLbls = languageLbls
            self.navigationController?.pushViewController(loginVC, animated: false)
        } else {
            getLanguages()
        }
        dropDown.selectedRowColor = .lightGray
        dropDown.didSelect{(selectedText , index ,id) in
            self.selectedLanguage = self.languageList[index]
        }
        // Do any additional setup after loading the view.
    }
    
    func getLanguages(){
        self.showLoader()
        DIWebLayerUserAPI().getLanguages(parameters: nil, success: { (response) in
            self.hideLoader()
            self.languageList = []
            if let list = response{
                 self.languageList = list
            }
            self.dropDown.optionArray = self.languageList.map({$0.language ?? ""})
            self.dropDown.optionIds = self.languageList.enumerated().map({$0.offset})
        }) { (error) in
            self.hideLoader()
            self.showAlert(withError:error)
        }
    }
    
    func getAllLanguageData(_ language:Language){
        self.showLoader()
        DIWebLayerUserAPI().getLanguageLabel(parameters: nil, language: language, success: { (response) in
            self.hideLoader()
            if let languageLbls = response {
                Defaults.shared.set(value: languageLbls, forKey: DefaultKey.languageLabel)
                let loginVC : LoginViewController = UIStoryboard(storyboard: .main).initVC()
                loginVC.languageLbls = languageLbls
                self.navigationController?.pushViewController(loginVC, animated: false)
            }
        }){ (error) in
            self.hideLoader()
            self.showAlert(withError:error)     
        }
    }
    
    @IBAction func chooseLangTapped(_ sender: UIButton) {
        if let language = self.selectedLanguage{
            Defaults.shared.set(value: language.code ?? "", forKey: .languageCode)
            getAllLanguageData(language)
        } else {
            self.showAlert(message:"Please select language.")
        }
    }
}

