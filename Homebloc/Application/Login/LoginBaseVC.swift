//
//  loginViewController.swift
//  BaseProject
//
//  Created by TpSingh on 08/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Google
import GoogleSignIn


class LoginBaseVC: DIBaseController,GIDSignInUIDelegate,GIDSignInDelegate  {
  
  //MARK:- IBOutlets
  @IBOutlet var textFieldEmail:UITextField!
  @IBOutlet var textFieldPassword:UITextField!
  
  //MARK:- Life Cycle
  override func viewDidLoad() {
    
    super.viewDidLoadWithKeyboardManager(viewController: self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
  }
  
  func hideShowPasswordButtonAction(sender:UIButton) {
    if textFieldPassword.isSecureTextEntry{
      sender.setTitle(Constant.ButtonName.hide, for: .normal)
    }
    else{
      sender.setTitle(Constant.ButtonName.show, for: .normal)
    }
    textFieldPassword.isSecureTextEntry = !textFieldPassword.isSecureTextEntry
  }
  
  
  func loginButtonAction(success:@escaping (_ finished: Bool) -> ()) {
    
    if isLoginFormValid() {
      self.view.endEditing(true)
      var loginDic = Login()
      loginDic.email = self.textFieldEmail.text ?? ""
      loginDic.password = self.textFieldPassword.text ?? ""
      DILog.print(items: "Now you can proceed with login api")
      
        callLoginAPI(loginDic: loginDic.getDictionary(),success: { (finish) in
        if finish {
          success(true)
          return
        }
        success(false)
      })
    }
    success(false)
  }
  
  //MARK:- .......... Private method
  /// This Method is used to call login api success: @escaping (_ user: User) -> (),
  private func callLoginAPI(loginDic : [String: Any],success: @escaping(_ finished: Bool)-> ()) {
    showLoader()
    let webLayerLogin = DIWebLayerUserAPI()
   /* webLayerLogin.login(parameters: loginDic, success: { (user) in
      UserManager.saveCurrentUser(user: user)
      DILog.print(items: "Logged in user: \(User.sharedInstance.firstName!) \(User.sharedInstance.lastName!)")
      self.hideLoader()
      success(true)
    }) {
      self.hideLoader()
      self.showAlert(withError: $0)
      success(false)
    }*/
  }
  
  private func isLoginFormValid() -> Bool {
    var message:String?
    if (textFieldEmail.text?.isBlank)! {
      message = AppMessages.Email.emptyEmail
    }else if !((textFieldEmail.text?.isValidEmail)!) {
      message = AppMessages.Email.invalidEmail
    }else if (textFieldPassword.text?.isEmpty)! {
      message = AppMessages.Password.emptyPassword
    }
    if message != nil {
      self.showAlert( message: message)
      return false
    }
    return true
  }
  
  //MARK:- Facebook Login Methods  
  func facebookLogin(success:@escaping (_ finished: Bool) -> ()) {
    let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
    fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
    
    fbLoginManager.logOut()
    
    fbLoginManager.logIn(withReadPermissions: [Constant.FacebookPermissions.publicProfile.rawValue,Constant.FacebookPermissions.email.rawValue,Constant.FacebookPermissions.birthday.rawValue], from: self) { (result, error) in
      
      if error != nil{
        print(error.debugDescription)
        // Calling back to previous class if error occured
       success(false)
        return
      }
      
      let FBLoginResult: FBSDKLoginManagerLoginResult = result!
      
      if FBLoginResult.isCancelled{
        
        print("User cancelled the login process")
        
      }else if FBLoginResult.grantedPermissions.contains(Constant.FacebookPermissions.email.rawValue){
        self.getFBUserData(success: { (finish) in
          if finish {
            success(true)
            return
          }
          success(false)
       })
    }
  }
  //success(true)
}
  
  func getFBUserData(success: @escaping(_ finished: Bool)-> ()){
    
    if (FBSDKAccessToken.current() != nil) {
      
      let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, first_name, last_name, email, birthday, gender"])
      let connection = FBSDKGraphRequestConnection()
      connection.add(graphRequest, completionHandler: { (connection, result, error) -> Void in
        let data = result as! [String : AnyObject]
        var login = Login()
        login.email = (data["email"] as? String)!
        login.snsType = 2
        login.snsId = (data["id"] as? String)!
        login.firstName = (data["first_name"] as? String)!
        login.lastName = (data["last_name"] as? String)!
        let url = "https://graph.facebook.com/\(login.snsId)/picture?type=large&return_ssl_resources=1"
        login.profilePicure = url
        login.gender = (data["gender"] as? String) ?? ""
        DILog.print(items: "\(login.getDictionaryForSocialType())")
        
        self.callLoginAPI(loginDic: login.getDictionaryForSocialType(), success: { (finish) in
          if finish {
            success(true)
            return
          }
          success(false)
        })
      })
      connection.start()
    }
  }
  
  
  
  //MARK:- Google SignIn Delegate
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
    
    if user != nil {
      var login = Login()
      login.email = user.profile.email
      login.snsId = user.userID
      login.snsType = 3
      login.firstName = user.profile.givenName
      login.lastName = user.profile.familyName
      login.profilePicure = user.profile.imageURL(withDimension: 200).absoluteString
            
        DILog.print(items: "\(login.getDictionaryForSocialType())")
      
      self.callLoginAPI(loginDic: login.getDictionaryForSocialType(), success: { (finish) in
        if finish {
          let registerObj: RegistrationController = UIStoryboard(storyboard: .main).initVC()
          self.title = ""
          self.navigationController?.pushViewController(registerObj, animated: true)
        }
      })
    }else{
      print("failure")
      return
    }
  }
  
  func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!){
    print("Disconnect")
  }
 
  
  func stringToDictionary(text:String) -> [String:Any]?{
    if let data = text.data(using: .utf8){
      do{
        return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
      }catch{
        print(error.localizedDescription)
      }
    }
    return nil
  }
  
  
}




