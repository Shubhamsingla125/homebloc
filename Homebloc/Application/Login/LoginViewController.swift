//
//  loginViewController.swift
//  BaseProject
//
//  Created by iosdev.
//  Copyright All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import FacebookCore
import FacebookLogin
import Google
import GoogleSignIn
import CountryPicker

class LoginViewController: DIBaseController,GIDSignInUIDelegate,GIDSignInDelegate  {
    
    var languageLbls : [String:Any]?
    var languageModel : LanguageLabel?
    var countryCode = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var searchHotelBtn: UIButton!
    @IBOutlet weak var justASecoundAndConfirmPhoneNumberLbl: UILabel!
    @IBOutlet weak var welcomeToHomeBlocLbl: UILabel!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var textFieldCode: UITextField!
    @IBOutlet var textFieldPhoneNo:UITextField!
    @IBOutlet var textFieldPassword:UITextField!
    @IBOutlet var loginBtn:UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var dontHaveAccntLbl: UILabel!

    @IBOutlet weak var forgotPwdBtn: UIButton!
    //MARK:- Life Cycle
    override func viewDidLoad() {
        createUserExperience()
        super.viewDidLoadWithKeyboardManager(viewController: self)
        //custom intialization for google
        var error: NSError?
        GGLContext.sharedInstance().configureWithError(&error)
        if error != nil {
            print("Error")
        }
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            loginBtn.setTitle(lblsValue.lBL_LOGIN?.uppercased(), for: .normal)
            signUpBtn.setTitle(lblsValue.lBL_SIGN_UP, for: .normal)
            dontHaveAccntLbl.text = lblsValue.lBL_DONT_HAVE_ACCOUNT
            languageModel = lblsValue
            textFieldCode.placeholder = lblsValue.lBL_ENTER_CODE
            textFieldPhoneNo.placeholder = lblsValue.lBL_PHONE_NUMBER
            textFieldPassword.placeholder = lblsValue.lBL_PASSWORD
            searchHotelBtn.setTitle(lblsValue.lBL_SEARCH_HOTEL, for: .normal)
            if let justScnd = lblsValue.lBL_A_SECOND?.dropLast() {
                justASecoundAndConfirmPhoneNumberLbl.text = "\(justScnd)"
            }
            welcomeToHomeBlocLbl.text = lblsValue.lBL_WELCOME_TO_APP
            forgotPwdBtn.setTitle(lblsValue.lBL_FORGOT_PASSWORD, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = AppMessages.ScreenTitles.login
        
    }
    
    func createUserExperience(){
        setUpLanguage()
//        Utility.addLeftViewInTxtFld(txtFld: textFieldCode, image: #imageLiteral(resourceName: "dots"))
//        Utility.addLeftViewInTxtFld(txtFld: textFieldPassword, image: #imageLiteral(resourceName: "key"))
        
        textFieldCode.rightViewMode = UITextFieldViewMode.always
        textFieldCode.rightView = getAcquireCodeBtn()
      
        setUpCountryPicker()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissCountryPicker(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissCountryPicker(_ gesture:UITapGestureRecognizer){
        pickerView.isHidden = true
    }
    
    func setUpCountryPicker(){
        //get current country
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
        pickerView.isHidden = true
    }
    
    func getAcquireCodeBtn()->UIView?{
        let acquireBtnView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 40))
        let acquireBtn = UIButton(type: .system)
        acquireBtn.frame = CGRect(x: 5, y: 7, width: 80, height: 25)
        acquireBtn.setTitle(languageModel?.lBL_ACQUIRE_CODE ?? "", for: .normal)
        acquireBtn.titleLabel?.font = .systemFont(ofSize: 10)
        acquireBtn.setTitleColor(.white, for: .normal)
        acquireBtn.addTarget(self, action: #selector(acquireCodeTapped(_:)), for: .touchUpInside)
        acquireBtn.cornerRadius = 12
        acquireBtnView.addSubview(acquireBtn)
        acquireBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1)
        return acquireBtnView
    }
    
    
    @objc func acquireCodeTapped(_ target:UIButton){
        if(textFieldPhoneNo.text?.isEmpty ?? false){
            self.showAlert(message:languageModel?.lBL_PLEASE_ENTER_PHONE ?? "")
        } else {
            self.showLoader()
            DIWebLayerUserAPI().getVerificationCode(parameters: nil, mobile:textFieldPhoneNo.text ?? "", countryCode:countryButton?.titleLabel?.text ?? "", success: { (response) in
                self.hideLoader()
                self.showAlert(message:response)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    
    func hideShowPasswordButtonAction(sender:UIButton) {
        if textFieldPassword.isSecureTextEntry{
            sender.setTitle(Constant.ButtonName.hide, for: .normal)
        }else{
            sender.setTitle(Constant.ButtonName.show, for: .normal)
        }
        textFieldPassword.isSecureTextEntry = !textFieldPassword.isSecureTextEntry
    }
    
    
    func loginButtonAction() {
        self.view.endEditing(true)
        if isLoginFormValid() {
            self.showLoader()
            DIWebLayerUserAPI().login(parameters: nil, mobile: textFieldPhoneNo.text ?? "", countryCode: countryButton?.titleLabel?.text ?? "", password: textFieldPassword.text ?? "", verifyCode: textFieldCode.text ?? "", success: { (response) in
                self.hideLoader()
                print(response)
                appDelegate.navigateToPattern()
                Defaults.shared.set(value: self.countryCode, forKey: .countryCode)
            }, failure: { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            })
        }
    }
    
    //MARK: Custom Methods
    func navigateNext() {
        let registerObj: RegistrationController = UIStoryboard(storyboard: .main).initVC()
        self.title = ""
        self.navigationController?.pushViewController(registerObj, animated: true)
    }
    
    //MARK:IBActions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        self.loginButtonAction()
    }
    
    @IBAction func selectCountryTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        pickerView.isHidden = false
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        pickerView.isHidden = true
    }
    
    @IBAction func forgotPwdTapped(_ sender: UIButton) {
        let forgotVc:ForgotPasswordViewController = UIStoryboard(storyboard: .main).initVC()
        self.navigationController?.pushViewController(forgotVc, animated:true)
    }
    
    @IBAction func facebookLogin(_ sender: UIButton) {
        self.facebookLogin()
        
    }
    
    @IBAction func linkedinLogin(_ sender: UIButton) {
        
        
    }
    
    @IBAction func googleLogin(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func showPasswordButtonTapped(_ sender: UIButton) {
        self.hideShowPasswordButtonAction(sender: sender)
    }
    
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        let signUpObj: SignupViewController = UIStoryboard(storyboard: .main).initVC()
        self.navigationController?.pushViewController(signUpObj, animated: true)
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let forgotObj: ForgotPasswordController = UIStoryboard(storyboard: .main).initVC()
        self.navigationController?.pushViewController(forgotObj, animated: true)
    }
    
    //MARK:- .......... Private method
    /// This Method is used to call login api success: @escaping (_ user: User) -> (),
    private func callLoginAPI(loginDic :Parameters,success: @escaping(_ finished: Bool)-> ()) {
        showLoader()
        let webLayerLogin = DIWebLayerUserAPI()
       /* webLayerLogin.login(parameters: loginDic, success: { (user) in
            UserManager.saveCurrentUser(user: user)
            print(User.sharedInstance.firstName!)
            DILog.print(items: "Logged in user: \(User.sharedInstance.firstName!) \(User.sharedInstance.lastName!)")
            self.hideLoader()
            success(true)
        }) {
            self.hideLoader()
            self.showAlert(withError: $0)
            success(false)
        }*/
    }
    
    private func isLoginFormValid()->Bool{
        var message : String?
        if(textFieldPhoneNo.text?.isEmpty ?? false){
            message = languageModel?.lBL_PLEASE_ENTER_PHONE ?? ""
        } else if (textFieldCode.text?.isEmpty ?? false) {
            message = languageModel?.lBL_PLEASE_ENTER_CODE ?? ""
        } else if (textFieldPassword.text?.isEmpty ?? false){
            message = languageModel?.lBL_PLEASE_ENTER_PASSWORD ?? ""
        }
        if message != nil{
            self.showAlert(message:message)
            return false
        }
        return true
    }
    
    
    //MARK:- Facebook Login Methods
    func facebookLogin() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
        
        fbLoginManager.logOut()
        
        fbLoginManager.logIn(withReadPermissions: [Constant.FacebookPermissions.publicProfile.rawValue,Constant.FacebookPermissions.email.rawValue,Constant.FacebookPermissions.birthday.rawValue], from: self) { (result, error) in
            
            if error != nil{
                print(error.debugDescription)
                
                return
            }
            
            let FBLoginResult: FBSDKLoginManagerLoginResult = result!
            
            if FBLoginResult.isCancelled{
                
                print("User cancelled the login process")
                
            }else if FBLoginResult.grantedPermissions.contains(Constant.FacebookPermissions.email.rawValue){
                self.getFBUserData(success: { (finish) in
                    if finish {
                        self.navigateNext()
                        return
                    }
                })
            }
        }
        //success(true)
    }
    
    func getFBUserData(success: @escaping(_ finished: Bool)-> ()){
        
        if (FBSDKAccessToken.current() != nil) {
            
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, first_name, last_name, email, birthday, gender"])
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) -> Void in
                let data = result as! [String : AnyObject]
                var login = Login()
                login.email = (data["email"] as? String)!
                login.snsType = 2
                login.snsId = (data["id"] as? String)!
                login.firstName = (data["first_name"] as? String)!
                login.lastName = (data["last_name"] as? String)!
                let url = "https://graph.facebook.com/\(login.snsId)/picture?type=large&return_ssl_resources=1"
                login.profilePicure = url
                login.gender = (data["gender"] as? String) ?? ""
                DILog.print(items: "\(login.getDictionaryForSocialType())")
                self.callLoginAPI(loginDic: login.getDictionaryForSocialType(), success: { (finish) in
                    if finish {
                        success(true)
                        return
                    }
                    success(false)
                })
            })
            connection.start()
        }
    }
    
    
    
    //MARK:- Google SignIn Delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
        
        if user != nil {
            var login = Login()
            login.email = user.profile.email
            login.snsId = user.userID
            login.snsType = 3
            login.firstName = user.profile.givenName
            login.lastName = user.profile.familyName
            login.profilePicure = user.profile.imageURL(withDimension: 200).absoluteString
            
            DILog.print(items: "\(login.getDictionaryForSocialType())")
            
            self.callLoginAPI(loginDic: login.getDictionaryForSocialType(), success: { (finish) in
                if finish {
                    let registerObj: RegistrationController = UIStoryboard(storyboard: .main).initVC()
                    self.title = ""
                    self.navigationController?.pushViewController(registerObj, animated: true)
                }
            })
        }else{
            print("failure")
            return
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!){
        print("Disconnect")
    }
    
    
    
    func stringToDictionary(text:String) -> Parameters?{
        if let data = text.data(using: .utf8){
            do{
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            }catch{
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
}
//MARK:LoginTextFieldExtension
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldPassword {
            self.loginButtonAction()
            
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView.isHidden = true
        return true
    }
}



extension LoginViewController : CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        self.countryCode = countryCode
        countryButton.setTitle(phoneCode, for: .normal)
        
    }
}



