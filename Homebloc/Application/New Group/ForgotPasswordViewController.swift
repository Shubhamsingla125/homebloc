//
//  ForgotPasswordViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 06/05/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import IQKeyboardManagerSwift
import CountryPicker
import UIKit
class ForgotPasswordViewController: DIBaseController {
    
    var languageLbls : [String:Any]?
    var languageModel : LanguageLabel?
    var countryCode = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var justASecoundAndConfirmPhoneNumberLbl: UILabel!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var textFieldCode: UITextField!
    @IBOutlet var textFieldPhoneNo:UITextField!
    @IBOutlet var textFieldPassword:UITextField!
    @IBOutlet var textFieldNewPassword:UITextField!
    
    @IBOutlet var loginBtn:UIButton!

    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        createUserExperience()
        super.viewDidLoadWithKeyboardManager(viewController: self)
        //custom intialization for google
        var error: NSError?
        if error != nil {
            print("Error")
        }
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            loginBtn.setTitle(lblsValue.lBL_RECOVER_PASSWORD?.uppercased(), for: .normal)
            languageModel = lblsValue
            textFieldCode.placeholder = lblsValue.lBL_ENTER_CODE
            textFieldPhoneNo.placeholder = lblsValue.lBL_PHONE_NUMBER
            textFieldPassword.placeholder = lblsValue.lBL_ENTER_NEW_PASSWORD
            justASecoundAndConfirmPhoneNumberLbl.text = lblsValue.lBL_ENTER_MOBILE_AND_VERIFY
            textFieldNewPassword.placeholder = lblsValue.lBL_VERIFY_NEW_PASSWORD
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = AppMessages.ScreenTitles.login
        
    }
    
    func createUserExperience(){
        setUpLanguage()
        //        Utility.addLeftViewInTxtFld(txtFld: textFieldCode, image: #imageLiteral(resourceName: "dots"))
        //        Utility.addLeftViewInTxtFld(txtFld: textFieldPassword, image: #imageLiteral(resourceName: "key"))
        
        textFieldCode.rightViewMode = UITextFieldViewMode.always
        textFieldCode.rightView = getAcquireCodeBtn()
        
        setUpCountryPicker()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissCountryPicker(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissCountryPicker(_ gesture:UITapGestureRecognizer){
        pickerView.isHidden = true
    }
    
    func setUpCountryPicker(){
        //get current country
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .black, rowBackgroundColor: .white, showFlagsBorder: false)        //optional for UIPickerView theme changes
        picker.theme = theme //optional for UIPickerView theme changes
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
        pickerView.isHidden = true
    }
    
    func getAcquireCodeBtn()->UIView?{
        let acquireBtnView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 40))
        let acquireBtn = UIButton(type: .system)
        acquireBtn.frame = CGRect(x: 5, y: 7, width: 80, height: 25)
        acquireBtn.setTitle(languageModel?.lBL_ACQUIRE_CODE ?? "", for: .normal)
        acquireBtn.titleLabel?.font = .systemFont(ofSize: 10)
        acquireBtn.setTitleColor(.white, for: .normal)
        acquireBtn.addTarget(self, action: #selector(acquireCodeTapped(_:)), for: .touchUpInside)
        acquireBtn.cornerRadius = 12
        acquireBtnView.addSubview(acquireBtn)
        acquireBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.3411764706, blue: 0.3803921569, alpha: 1)
        return acquireBtnView
    }
    
    
    @objc func acquireCodeTapped(_ target:UIButton){
        if(textFieldPhoneNo.text?.isEmpty ?? false){
            self.showAlert(message:languageModel?.lBL_PLEASE_ENTER_PHONE ?? "")
        } else {
            self.showLoader()
            DIWebLayerUserAPI().getVerificationCode(parameters: nil, mobile:textFieldPhoneNo.text ?? "", countryCode:countryButton?.titleLabel?.text ?? "", success: { (response) in
                self.hideLoader()
                self.showAlert(message:response)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    
    func hideShowPasswordButtonAction(sender:UIButton) {
        if textFieldPassword.isSecureTextEntry{
            sender.setTitle(Constant.ButtonName.hide, for: .normal)
        }else{
            sender.setTitle(Constant.ButtonName.show, for: .normal)
        }
        textFieldPassword.isSecureTextEntry = !textFieldPassword.isSecureTextEntry
    }
    
    
    func forgotButtonAction() {
        self.view.endEditing(true)
        if isLoginFormValid() {
            self.showLoader()
            DIWebLayerUserAPI().forgotPassword(lang:getLangugaeFromDefault(),parameters: nil, mobile: textFieldPhoneNo.text ?? "", countryCode: countryButton?.titleLabel?.text ?? "", password: textFieldPassword.text ?? "", verifyCode: textFieldCode.text ?? "", success: { (response) in
                self.hideLoader()
                self.showAlert(message: response,okCall: {
                    self.navigationController?.popViewController(animated: true)
                })
            }, failure: { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            })
        }
    }
    
    //MARK: Custom Methods
    func navigateNext() {
        let registerObj: RegistrationController = UIStoryboard(storyboard: .main).initVC()
        self.title = ""
        self.navigationController?.pushViewController(registerObj, animated: true)
    }
    
    //MARK:IBActions
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        self.forgotButtonAction()
    }
    
    @IBAction func selectCountryTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        pickerView.isHidden = false
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        pickerView.isHidden = true
    }
    
    @IBAction func showPasswordButtonTapped(_ sender: UIButton) {
        self.hideShowPasswordButtonAction(sender: sender)
    }
    
    @IBAction func backtapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        let signUpObj: SignupViewController = UIStoryboard(storyboard: .main).initVC()
        self.navigationController?.pushViewController(signUpObj, animated: true)
    }
    

    
    //MARK:- .......... Private method
    /// This Method is used to call login api success: @escaping (_ user: User) -> (),
    private func callLoginAPI(loginDic :Parameters,success: @escaping(_ finished: Bool)-> ()) {
        showLoader()
        let webLayerLogin = DIWebLayerUserAPI()
        /* webLayerLogin.login(parameters: loginDic, success: { (user) in
         UserManager.saveCurrentUser(user: user)
         print(User.sharedInstance.firstName!)
         DILog.print(items: "Logged in user: \(User.sharedInstance.firstName!) \(User.sharedInstance.lastName!)")
         self.hideLoader()
         success(true)
         }) {
         self.hideLoader()
         self.showAlert(withError: $0)
         success(false)
         }*/
    }
    
    private func isLoginFormValid()->Bool{
        var message : String?
        if(textFieldPhoneNo.text?.isEmpty ?? false){
            message = languageModel?.lBL_PLEASE_ENTER_PHONE ?? ""
        } else if (textFieldCode.text?.isEmpty ?? false) {
            message = languageModel?.lBL_PLEASE_ENTER_CODE ?? ""
        } else if (textFieldPassword.text?.isEmpty ?? false){
            message = languageModel?.lBL_PLEASE_ENTER_PASSWORD ?? ""
        }else if (textFieldNewPassword.text?.isEmpty ?? false){
            message = languageModel?.lBL_PLEASE_RE_ENTER_PASSWORD ?? ""
        }
        if message != nil{
            self.showAlert(message:message)
            return false
        }
        return true
    }
   
}
//MARK:LoginTextFieldExtension
extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldNewPassword {
            self.forgotButtonAction()
            
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        pickerView.isHidden = true
        return true
    }
}



extension ForgotPasswordViewController : CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        self.countryCode = countryCode
        countryButton.setTitle(phoneCode, for: .normal)
        
    }
}
