//
//  LuckyPoolPaymentViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 18/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//


import UIKit
import Stripe
class LuckyPoolPaymentViewController: DIBaseController {
    
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var contactNoLbl: UILabel!
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var checkNightsLbl: UILabel!
    @IBOutlet weak var noticeLbl: UILabel!
    @IBOutlet weak var checkInjBeforeLbl: UILabel!
    @IBOutlet weak var reservationLbl: UILabel!
    @IBOutlet weak var reservationPendingLbl: UILabel!
    @IBOutlet weak var titleHeaderLbl: UILabel!
    @IBOutlet weak var confirmbtn: UIButton!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var amountlbl: UILabel!
    @IBOutlet weak var contexttxtFld: UITextField!
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var nightsLbl: UILabel!
    @IBOutlet weak var datesLbl: UILabel!
    @IBOutlet weak var hotelnameLbl: UILabel!

    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBOutlet weak var visaBtn: UIButton!
    @IBOutlet weak var debitCreditCardLbl: UILabel!
    @IBOutlet weak var cardNumberTxtFld: UITextField!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var monthTxtFld: UITextField!
    @IBOutlet weak var yearTxtFld: UITextField!
    @IBOutlet weak var cvvLbl: UILabel!
    @IBOutlet weak var cvvTxtFld: UITextField!
    @IBOutlet weak var paymentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var payWithHocLbl: UILabel!
    @IBOutlet weak var hocSwitch: UISwitch!
    @IBOutlet weak var noticetxtLbl: UILabel!
    
    @IBOutlet weak var locationAreaLbl: UILabel!
    @IBOutlet weak var hotelPhoneNolbl: UILabel!
    //Confirmation Pop up
 
    
    var startDate = Date()
    var endDate = Date()
    var nights = ""
    var languageModel : LanguageLabel?
    var hotel :Hotel?
    var hotelRoomList: HotelRoomList?
    var hotelDetail : HotelDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLanguage()
        initalization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            titleHeaderLbl.text = lblsValue.lBL_BUY_FROM_LUCKY_POOL ?? "Buy from Lucky Poll"
            hotelnameLbl.text = hotel?.hotelName ?? ""
            reservationLbl.text = (lblsValue.lBL_RESERVATION ?? "") + ": " + (self.hotel?.order ?? "")
            priceLbl.text = (lblsValue.lBL_PRICE ?? "") + ": " + (self.hotel?.currency_text ?? "")
            priceLbl.text = (priceLbl.text ?? "")  + (self.hotel?.price ?? "")
            
            customerNameLbl.text = (lblsValue.lBL_CUSTOMER_NAME ?? "Customer Name") + ": " + (self.hotel?.reseller_name ?? "")
            contactNoLbl.text = (lblsValue.lBL_CONTACT ?? "") + ": " + (self.hotel?.reseller_phone ?? "")
            
            reservationPendingLbl.text = (lblsValue.lBL_RESERVATION ?? "") + " " + (lblsValue.lBL_PENDING ?? "")
            checkNightsLbl.text = "1 \(lblsValue.lBL_NIGHT ?? "")"
            
            
            contactLbl.text = lblsValue.lBL_CONTACT
            nameLbl.text = lblsValue.lBL_NAME
            nameTxtFld.placeholder = languageModel?.lBL_NAME
            emailTxtFld.placeholder = languageModel?.lBL_EMAIL
            emailLbl.text = languageModel?.lBL_EMAIL
            
            
            locationAreaLbl.text = self.hotel?.locationArea ?? ""
             hotelPhoneNolbl.text = (languageModel?.lBL_CONTACT ?? "") + (self.hotel?.hotelPhone ?? "")
            
            paymentMethodLbl.text = lblsValue.lBL_PAYMENT_DETAIL
            debitCreditCardLbl.text = lblsValue.lBL_DEBIT_CARD_NO
            cardNumberTxtFld.placeholder = lblsValue.lBL_ENTER_CARD_NO_XXX
            expiryDateLbl.text = lblsValue.lBL_EXPIRY_DATE
            monthTxtFld.placeholder = lblsValue.lBL_MONTH
            yearTxtFld.placeholder = lblsValue.lBL_YEAR
            cvvLbl.text = lblsValue.lBL_CVV_INFO
            cvvTxtFld.placeholder = lblsValue.lBL_CVV
            payWithHocLbl.text = lblsValue.lBL_PAY_WITH_HOC
        }
    }
    
    
    func initalization(){
        let dateformatter = getDateFormatter(format:"MMM,dd yyyy",timeZone:NSTimeZone.local as NSTimeZone)
        if let hotel = self.hotel{
            hotelnameLbl.text = hotel.hotelName
            nightsLbl.text = nights
            let startDateString = dateformatter.string(from: startDate)
            let endDateString = dateformatter.string(from: endDate)
            datesLbl.text = "\(languageModel?.lBL_DATES ?? "") \(startDateString) \(endDateString)"
            confirmbtn.setTitle(languageModel?.lBL_PURCHASE ?? "", for: .normal)
            detailLbl.text = languageModel?.lBL_DETAIL
            amountlbl.text = "\(languageModel?.lBL_AMOUNT ?? "")\n \(self.hotel?.currency_text ?? "") \(self.hotel?.price ?? "")"
            
            checkInjBeforeLbl.text = (languageModel?.lBL_CHECK_IN ?? "") + " " + (languageModel?.lBL_BEFORE ?? "")
            
            checkInjBeforeLbl.text = (checkInjBeforeLbl.text ?? "") + " " + startDateString
            noticeLbl.text = languageModel?.lBL_NOTICE ?? ""
            noticetxtLbl.text = languageModel?.lBL_NOTICE_TEXT ?? ""
        }
        paymentViewHeightConstraint.constant = 0.0
    }
    
    func validateAmount()->Bool{
        var message : String?
        if nameTxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_ERROR_USER_NAME
        } else if contexttxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_ERROR_PHONE_NO
        } else if emailTxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_ERROR_EMAIL
        }else if !(emailTxtFld.text?.isValidEmail ?? true){
            message = languageModel?.lBL_ERROR_EMAIL
        }else if !(cardNumberTxtFld.text?.isValidInRange(minLength: 11, maxLength: 17) ?? true)  {
            message = languageModel?.lBL_INVALID_CARD_INFO
        } else if monthTxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_INVALID_CARD_INFO
        } else if yearTxtFld.text?.length != 4 {
            message = languageModel?.lBL_INVALID_CARD_INFO
        } else if cvvTxtFld.text?.length != 3  {
            message = languageModel?.lBL_INVALID_CARD_INFO
        }
        
        if message != nil {
            self.showAlert(message:message)
            return false
        }
        return true
    }
    
    func createToken(orderid:String){
        self.showLoader()
        let cardParams = STPCardParams()
        cardParams.number = cardNumberTxtFld.text ?? "" //"4242424242424242"
        cardParams.expMonth = UInt(Int(monthTxtFld.text ?? "") ?? 0) //10
        cardParams.expYear = UInt(Int(yearTxtFld.text ?? "") ?? 0) //2021
        cardParams.cvc = cvvTxtFld.text ?? "" //"123"
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                self.hideLoader()
                self.showAlert(message:error?.localizedDescription)
                return
            }
            self.makePayment(orderid:orderid,token:token)
        }
    }
    
    
    func addOrder(){
        let requestDateFormatter = getDateFormatter(format:"yyyy-MM-dd",timeZone:NSTimeZone.local as NSTimeZone)
        let startDateString = requestDateFormatter.string(from: startDate)
        let endDateString = requestDateFormatter.string(from: endDate)
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader()
            var params = [
                "hotelId":hotel?.hotelId ?? "",
                "houseId":hotel?.room_id ?? "",
                "checkInDate":startDateString,
                "stayedDate":endDateString,
                "houseNumber":hotel?.roomsBooked ?? "",
                "checkInName":nameTxtFld.text ?? "",
                "checkInPhone":contexttxtFld.text ?? "",
                "discountScore":"0",
                "discountMoney":"0",
                "payType":"1",
            ]
            params["token"] = User.sharedInstance.oauthToken ?? ""
            params["lang"] = languageCode
            params["currencytext"] = self.hotel?.currency_text ?? ""
            params["currency"] = self.hotel?.currency ?? ""
            DIWebLayerUserAPI().addOrder(parameters: params, success: { (response) in
                self.hideLoader()
                self.createToken(orderid:response)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    func makePayment(orderid:String,token:STPToken?){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            let params =
                [
                    "lang":languageCode,
                    "token":User.sharedInstance.oauthToken ?? "",
                    "stripeEmail":"nitish%40@gmail.com",
                    "stripeToken":token?.tokenId ?? "",
                    "amount":"\(self.hotel?.price ?? "")",
                    "currency":self.hotel?.currency ?? ""
            ]
            DIWebLayerUserAPI().stripeCharge(parameters: params, success: { (response) in
                self.hideLoader()
                let bookingVc : AllBookingViewController = UIStoryboard(storyboard: .profile).initVC()
                self.navigationController?.pushViewController(bookingVc, animated: true)
                
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    @IBAction func confirmtapped(_ sender: UIButton) {
        if validateAmount() {
            let paymentVc : PaymentVerificationViewController = UIStoryboard(storyboard: .hotel).initVC()
            paymentVc.modalPresentationStyle = .overCurrentContext
            paymentVc.delegate = self
            self.present(paymentVc, animated: true, completion: nil)
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func visaTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            // 255
            paymentViewHeightConstraint.constant = 255.0
        } else {
            //0
            paymentViewHeightConstraint.constant = 0.0
        }
    }
    
}

extension LuckyPoolPaymentViewController:PaymentVerificationDelegate{
    func  passcodeVerified(){
        self.addOrder()
    }
}

