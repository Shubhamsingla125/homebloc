//
//  PaymentVerificationViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 07/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
protocol PaymentVerificationDelegate {
    func passcodeVerified()
}

class PaymentVerificationViewController: DIBaseController {

    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    @IBOutlet weak var fifthTextField: UITextField!
    @IBOutlet weak var sixthTextField: UITextField!
    
    @IBOutlet weak var passcodeLbl: UILabel!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var languageModel : LanguageLabel?
    var otp : String = ""
    var delegate : PaymentVerificationDelegate?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        setUpLanguage()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            confirmBtn.setTitle(lblsValue.lBL_CONFIRM, for: .normal)
            cancelBtn.setTitle(lblsValue.lBL_CANCEL, for: .normal)
            passcodeLbl.text = lblsValue.lBL_SIX_DIGIT_PASSCODE
        }
    }
    
    @IBAction func numberTapped(_ sender: UIButton) {
        setCurrentDigit(value:"\(sender.tag)")
    }
    
    @IBAction func confirmTapped(_ sender: UIButton) {
        if otp.count == 6 {
           otpVerifyApi(otp)
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Current digit
    func setCurrentDigit(value:String) {
        if (firstTextField.text == "") {
            firstTextField.text = value
        } else if (secondTextField.text == "") {
            secondTextField.text = value
        } else if (thirdTextField.text == "") {
            thirdTextField.text = value
        } else if (fourthTextField.text == "") {
            fourthTextField.text = value
        } else if (fifthTextField.text == "") {
            fifthTextField.text = value
        } else if (sixthTextField.text == "") {
            sixthTextField.text = value
            self.otp =
                (firstTextField?.text)!
                + (secondTextField?.text)!
                + (thirdTextField?.text)!
                + (fourthTextField?.text)!
                + (fifthTextField?.text)!
                + (sixthTextField?.text)!
        }
    }

    
    
    private func otpVerifyApi(_ otp:String){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            let params = [
                "token":User.sharedInstance.oauthToken ?? "",
                "password":otp,
                "lang":languageCode
            ]
            self.showLoader()
            DIWebLayerUserAPI().verifyPasscode(parameters: params, success: { (response) in
                self.hideLoader()
                self.delegate?.passcodeVerified()
                self.dismiss(animated: true, completion: nil)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
  
}
