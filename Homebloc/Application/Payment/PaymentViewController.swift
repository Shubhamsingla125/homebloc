//
//  PaymentViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 02/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Stripe
class PaymentViewController: DIBaseController {

    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    
    @IBOutlet weak var confirmbtn: UIButton!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var amountlbl: UILabel!
    @IBOutlet weak var contexttxtFld: UITextField!
    @IBOutlet weak var contactLbl: UILabel!
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var noOfRoomsTxtFld: UITextField!
    @IBOutlet weak var hashOfRooms: UILabel!
    @IBOutlet weak var hotelServicesLbl: UILabel!
    @IBOutlet weak var nightsLbl: UILabel!
    @IBOutlet weak var datesLbl: UILabel!
    @IBOutlet weak var hotelnameLbl: UILabel!
    @IBOutlet weak var discountReservationLbl: UILabel!
    @IBOutlet weak var requestFrontDesLbl: UILabel!
    @IBOutlet weak var paymentMethodLbl: UILabel!
    @IBOutlet weak var peakLbl: UILabel!
    @IBOutlet weak var visaBtn: UIButton!
    @IBOutlet weak var debitCreditCardLbl: UILabel!
    @IBOutlet weak var cardNumberTxtFld: UITextField!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var monthTxtFld: UITextField!
    @IBOutlet weak var yearTxtFld: UITextField!
    @IBOutlet weak var cvvLbl: UILabel!
    @IBOutlet weak var cvvTxtFld: UITextField!
    @IBOutlet weak var paymentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var payWithHocLbl: UILabel!
    @IBOutlet weak var hocSwitch: UISwitch!
    
    //Confirmation Pop up
    @IBOutlet weak var confirmDatelbl: UILabel!
    @IBOutlet weak var bookingOverNightLbl: UILabel!
    @IBOutlet weak var confirmNameLbl: UILabel!
    @IBOutlet weak var confirmPhoneNoLbl: UILabel!
    @IBOutlet weak var correctInformationLbl: UILabel!
    @IBOutlet weak var paymentConfirmbtn: UIButton!
    @IBOutlet weak var paymentCancelBtn: UIButton!
    @IBOutlet weak var confirmPaymentBgView: UIView!
    @IBOutlet weak var confirmPaymentView: UIView!
    
    var startDate = Date()
    var endDate = Date()
    var nights = ""
    var languageModel : LanguageLabel?
    var hotel :Hotel?
    var hotelRoomList: HotelRoomList?
    var hotelDetail : HotelDetail?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLanguage()
        initalization()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setUpLanguage(){
        if let lblsValue = Utility.getLanguageLabelModel(){
            languageModel = lblsValue
            contactLbl.text = lblsValue.lBL_CONTACT
            nameLbl.text = lblsValue.lBL_NAME
            hashOfRooms.text = lblsValue.lBL_ROOMS
            discountReservationLbl.text = lblsValue.lBL_DISCOUNT_STRING
            requestFrontDesLbl.text = lblsValue.lBL_REQUEST_SLIP
            peakLbl.text = lblsValue.lBL_PEAK_BOOKING_TEXT
            paymentMethodLbl.text = lblsValue.lBL_PAYMENT_DETAIL
            debitCreditCardLbl.text = lblsValue.lBL_DEBIT_CARD_NO
            cardNumberTxtFld.placeholder = lblsValue.lBL_ENTER_CARD_NO_XXX
            expiryDateLbl.text = lblsValue.lBL_EXPIRY_DATE
            monthTxtFld.placeholder = lblsValue.lBL_MONTH
            yearTxtFld.placeholder = lblsValue.lBL_YEAR
            cvvLbl.text = lblsValue.lBL_CVV_INFO
            cvvTxtFld.placeholder = lblsValue.lBL_CVV
            payWithHocLbl.text = lblsValue.lBL_PAY_WITH_HOC
            bookingOverNightLbl.text = lblsValue.lBL_OVER_NIGHT
            correctInformationLbl.text = lblsValue.lBL_CONFIRM_BOOKING_CHECK_INFO
            paymentConfirmbtn.setTitle(lblsValue.lBL_CONFIRM, for: .normal)
           paymentCancelBtn.setTitle(lblsValue.lBL_CANCEL, for: .normal)
            emailTxtFld.placeholder = languageModel?.lBL_EMAIL
            emailLbl.text = languageModel?.lBL_EMAIL
        }
    }
    

    func initalization(){
         let dateformatter = getDateFormatter(format:"MMM,dd yyyy",timeZone:NSTimeZone.local as NSTimeZone)
        if let hotel = self.hotel{
            hotelnameLbl.text = hotel.hotelName
            nightsLbl.text = nights
            let startDateString = dateformatter.string(from: startDate)
            let endDateString = dateformatter.string(from: endDate)
            datesLbl.text = "\(languageModel?.lBL_DATES ?? "") \(startDateString) \(endDateString)"
            nameTxtFld.placeholder = languageModel?.lBL_NAME
            for value in hotel.services_list{
                let startValue = hotelServicesLbl.text != "" ? hotelServicesLbl.text ?? "" +  " | " : ""
                hotelServicesLbl.text = startValue + value
            }
            
            confirmbtn.setTitle(languageModel?.lBL_CONFIRM?.uppercased() ?? "", for: .normal)
            detailLbl.text = languageModel?.lBL_DETAIL
            amountlbl.text = "\(languageModel?.lBL_AMOUNT ?? "")\n \(hotelRoomList?.predeterminedPrice ?? 0) \(hotelRoomList?.currency ?? "")"
            confirmDatelbl.text = "\(startDateString) - \(endDateString),\(nights)"
        }
        confirmPaymentBgView.isHidden = true
        confirmPaymentView.isHidden = true
        paymentViewHeightConstraint.constant = 0.0
    }
    
    func validateAmount()->Bool{
        var message : String?
        if noOfRoomsTxtFld.text?.isEmpty ?? true && noOfRoomsTxtFld.text?.length == 0{
            message = languageModel?.lBL_ERROR_ROOM_COUNT
        } else if nameTxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_ERROR_USER_NAME
        } else if contexttxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_ERROR_PHONE_NO
        }else if emailTxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_ERROR_EMAIL
        }else if !(emailTxtFld.text?.isValidEmail ?? true){
            message = languageModel?.lBL_ERROR_EMAIL
        }else if !(cardNumberTxtFld.text?.isValidInRange(minLength: 11, maxLength: 17) ?? true)  {
            message = languageModel?.lBL_INVALID_CARD_INFO
        } else if monthTxtFld.text?.isEmpty ?? true{
            message = languageModel?.lBL_INVALID_CARD_INFO
        } else if yearTxtFld.text?.length != 4 {
            message = languageModel?.lBL_INVALID_CARD_INFO
        } else if cvvTxtFld.text?.length != 3  {
            message = languageModel?.lBL_INVALID_CARD_INFO
        }

        if message != nil {
            self.showAlert(message:message)
            return false
        }
        return true
    }
    
    func createToken(orderid:String){
        self.showLoader()
        let cardParams = STPCardParams()
        cardParams.number = cardNumberTxtFld.text ?? "" //"4242424242424242"
        cardParams.expMonth = UInt(Int(monthTxtFld.text ?? "") ?? 0) //10
        cardParams.expYear = UInt(Int(yearTxtFld.text ?? "") ?? 0) //2021
        cardParams.cvc = cvvTxtFld.text ?? "" //"123"
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                self.hideLoader()
                self.showAlert(message:error?.localizedDescription)
                return
            }
            self.makePayment(orderid:orderid,token:token)
        }
    }
    
    
    func addOrder(){
        let requestDateFormatter = getDateFormatter(format:"yyyy-MM-dd",timeZone:NSTimeZone.local as NSTimeZone)
        let startDateString = requestDateFormatter.string(from: startDate)
        let endDateString = requestDateFormatter.string(from: endDate)
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            self.showLoader()
            var params = [
                "hotelId":hotel?.hotelId ?? "",
                "houseId":hotelRoomList?.houseId ?? "",
                "checkInDate":startDateString,
                "stayedDate":endDateString,
                "houseNumber":noOfRoomsTxtFld.text ?? "",
                "checkInName":nameTxtFld.text ?? "",
                "checkInPhone":contexttxtFld.text ?? "",
                "discountScore":"0",
                "discountMoney":"0",
                "payType":"1",
                "token":User.sharedInstance.oauthToken ?? "",
                "lang":languageCode
            ]
            params["totalOrder"] = "\((hotelRoomList?.predeterminedPrice ?? 0))"
            DIWebLayerUserAPI().addOrder(parameters: params, success: { (response) in
                self.hideLoader()
                self.createToken(orderid:response)
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    func makePayment(orderid:String,token:STPToken?){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String {
            let params =
                [
                    "lang":languageCode,
                    "token":User.sharedInstance.oauthToken ?? "",
                    "stripeEmail":"nitish%40@gmail.com",
                    "stripeToken":token?.tokenId ?? "",
                    "amount":"\(hotelRoomList?.predeterminedPrice ?? 0)",
                    "currency":hotelDetail?.currency ?? ""
            ]
            DIWebLayerUserAPI().stripeCharge(parameters: params, success: { (response) in
                self.hideLoader()
                print(response)
                let ratingVc : RatingViewController = UIStoryboard(storyboard: .hotel).initVC()
                ratingVc.modalPresentationStyle = .overCurrentContext
                ratingVc.delegate = self
                ratingVc.orderId = orderid
                self.present(ratingVc, animated: true, completion: nil)
                
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    @IBAction func confirmtapped(_ sender: UIButton) {
        if validateAmount() {
            confirmNameLbl.text = nameTxtFld.text ?? ""
            confirmPhoneNoLbl.text = contexttxtFld.text ?? ""
            confirmPaymentBgView.isHidden = false
            confirmPaymentView.isHidden = false
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirmPaymentTapped(_ sender: UIButton) {
        confirmPaymentBgView.isHidden = true
        confirmPaymentView.isHidden = true
        let paymentVc : PaymentVerificationViewController = UIStoryboard(storyboard: .hotel).initVC()
        paymentVc.modalPresentationStyle = .overCurrentContext
        paymentVc.delegate = self
        self.present(paymentVc, animated: true, completion: nil)
    }
    
    @IBAction func cancelPaymentTapped(_ sender: UIButton) {
        confirmPaymentBgView.isHidden = true
        confirmPaymentView.isHidden = true
    }
    
    @IBAction func visaTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
           // 255
            paymentViewHeightConstraint.constant = 255.0
        } else {
            //0
            paymentViewHeightConstraint.constant = 0.0
        }
    }
    
}
extension PaymentViewController: RatingDelegate{
    func ratingSucess() {
        let bookingVc : AllBookingViewController = UIStoryboard(storyboard: .profile).initVC()
        self.navigationController?.pushViewController(bookingVc, animated: true)
    }
}
extension PaymentViewController:PaymentVerificationDelegate{
    func  passcodeVerified(){
        self.addOrder()
    }
}
