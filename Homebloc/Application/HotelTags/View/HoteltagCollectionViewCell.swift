//
//  HoteltagCollectionViewCell.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 09/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit
import Kingfisher
class HoteltagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var tagImgVw: UIImageView!
    @IBOutlet weak var tagNameLbl: UILabel!
    
    
    func initCell(_ hoteltag:HotelTag){
        if let url = URL(string: hoteltag.img ?? ""){
            tagImgVw.kf.setImage(with: url)
        }
        tagNameLbl.text = hoteltag.tag_name ?? ""
        countLbl.text = hoteltag.uniacid ?? ""
    }
}
