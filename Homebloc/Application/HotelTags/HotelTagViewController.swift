//
//  HotelTagViewController.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 09/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import UIKit

class HotelTagViewController: DIBaseController {
    var city : City?
    @IBOutlet weak var headerTitle: UILabel!
    var tagList:[HotelTag] = []
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        headerTitle.text = city?.cityname ?? ""
         getHotelTag()
    }
    
    func getHotelTag(){
        if let languageCode = Defaults.shared.get(forKey: .languageCode) as? String, let selectedCity = city {
            self.showLoader()
            DIWebLayerUserAPI().getHotelTags(parameters: nil, language: languageCode, city: selectedCity, success: { (response) in
                self.hideLoader()
                if let hoteltagList = response {
                    self.tagList = hoteltagList
                }
                self.collectionView.reloadData()
            }) { (error) in
                self.hideLoader()
                self.showAlert(withError:error)
            }
        }
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
extension HotelTagViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tagList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HoteltagCollectionViewCell", for: indexPath) as? HoteltagCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.initCell(self.tagList[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2-5, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let hotelTag = self.tagList[indexPath.item]
        let hotelVC : HotelViewController = UIStoryboard(storyboard: .hotel).initVC()
        hotelVC.hoteltag = hotelTag
        hotelVC.city = self.city
        self.navigationController?.pushViewController(hotelVC, animated: true)
    }
}
