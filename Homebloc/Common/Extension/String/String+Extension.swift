//
//  String+Extension.swift
//  BaseProject
//
//  Created by Aj Mehra on 09/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import Foundation

extension String {
    /// This variable will holds the length of the string.
    var length: Int {
        return self.characters.count
    }

    /// This will check whether a string is empty or not by remove all spaces
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    /// This will check wheher a string is a valid email or not.
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)

    }
  
  var isValidPassword: Bool {
    
    if self.characters.count > 5 && self.characters.count < 15{
      return true
    }
    return false
  }
  
  func isValidInRange(minLength min: Int = 0, maxLength max: Int)-> Bool {
    if self.count > min && self.characters.count < max {
      return true
    }
    return false
  }

    func replace(_ string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
  
    var trim: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    func condensed() -> String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }

    var trimmed: String {
        let components = self.components(separatedBy: NSCharacterSet.whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }

    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")

    }
}

protocol OptionalString { }
extension String: OptionalString { }

extension Optional where Wrapped: OptionalString {
    var isBlankOption: Bool {
        guard var value = self as? String else { return true }
        value = value.trimmed
        return value.isEmpty
    }

//    var isvalidEmail: Bool {
//        guard let email = self as?String else { return false }
//        return email.isValidEmail
//    }


}
