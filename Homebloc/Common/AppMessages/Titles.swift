//
//  Titles.swift
//  BaseProject
//
//  Created by TpSingh on 04/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation

struct Titles {
  struct Button {
    static let facebookLogin: String = "facebookLogin".localized
    static let googleLogin: String = "googleLogin".localized
    static let linkedInLogin: String = "linkedInLogin".localized
  }
}
