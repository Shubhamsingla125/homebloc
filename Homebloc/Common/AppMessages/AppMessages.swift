//
//  AppMessages.swift
//
//
//  Created by Aj Mehra on 14/10/16.
//  Copyright © 2016 Debut InfoTech. All rights reserved.
//

import Foundation
struct AppMessages {
  
  struct Unkown {
    static let title:String = "".localized
    static let message:String = "Sorry, something went wrong. We're working on getting this fixed as soon as we can.".localized
    static let camera:String = "Homebloc app is not authorized to use camera.".localized
    static let gallery : String = "Homebloc app is not authorized to use gallery images.".localized
  }

    
    struct AlertTitles {
        static let Ok:String = "OK".localized
        static let Cancel:String = "CANCEL".localized
        static let Yes:String = "Yes".localized
        static let No:String = "No".localized
        static let Alert:String = "Alert".localized
        static let logoutMessage:String = "logoutMessage".localized
        static let pleaseWait:String = "pleaseWait".localized
        static let chooseGender:String = "chooseGender".localized
        static let genderMessage:String = "genderMessage".localized
        static let actionSheet:String = "actionSheet".localized
        static let error:String = "Error".localized
        static let setting:String = "Setting".localized
        static let unSubscribePlan = "unSubscribePlan".localized
        static let planSubscribe = "planSubscribe".localized
        static let deleteMessage = "deleteMessage".localized
        static let deleteSampleRqst = "deleteSampleRqst".localized
        static let delete = "Delete".localized
        static let emailUpdate = "Email Updated successfully".localized
        static let deleteRepMem = "deleteRepMem".localized
        static let chooseFrom = "Choose from".localized
        static let options = "Please select an option".localized
        static let locationAccess = "locationAccess".localized
        static let locationSettingMessage = "locationSettingMessage".localized
        static let DELETE = "DELETE".localized
    }
  
  struct UserName {
    static let empty:String = "emptyUsername".localized
    static let invalid:String = "validUsernameRange".localized
    static let emptyFirstname:String = "emptyFirstname".localized
    static let emptyLastname:String = "emptyLastname".localized
    static let maxLengthFirstname:String = "maxLengthFirstname".localized
    static let maxLengthLastname:String = "maxLengthLastname".localized
    
    
    
  }
  struct PhoneNumber {
    static let empty:String = "emptyPhoneNumber".localized
    static let invalid:String = "invalidPhoneNumber".localized
    static let newmpty:String = "emptyNewPhoneNumber".localized
    static let newinvalid:String = "invalidNewPhoneNumber".localized
    static let emptyMerchantMobileNo = "emptyMerchantPhoneNumber".localized
    static let invalidMerchantMobileNo = "invalidMerchantPhoneNumber".localized
  }
  struct Password {
    static let emptyPassword:String = "emptyPassword".localized
    static let invalidPassword:String = "invalidPassword".localized
    static let newEmpty:String = "newEmptyPassword".localized
    static let currentEmpty:String = "emptyCurrentPassword".localized
    static let emptyConfirmPassword = "emptyConfirmPassword".localized
    static let invalidCurrentPassword:String = "validCurrentPasswordRange".localized
    static let invalidNewPassword:String = "validNewPasswordRange".localized
    static let confirmNew:String = "confirmNewPassword".localized
    static let newConfirmMismatch = "newConfirmMismatch".localized
  }
  struct Email {
    static let emptyEmail:String = "emptyEmail".localized
    static let invalidEmail:String = "invalidEmail".localized
    static let emptyNewEmail:String = "emptyNewEmail".localized
  }
  struct Image {
    static let invalid = "invalidImage".localized
  }
  struct Voucher {
    static let empty = "emptyVoucherCode".localized
    static let invalid = "invalidVoucherCode".localized
  }
  
  struct Pin {
    static let empty = "emptyPin".localized
    static let invalid = "invalidPin".localized
  }
  
  struct ContactUS {
    struct Title {
      static let empty = "emptyContactUSTitle".localized
    }
    struct Query {
      static let empty = "emptyContactUSQuery".localized
    }
  }
  struct QRCode {
    static let noSupport = "QRNotSupportted".localized
  }
  struct Item {
    static let descMissing = "emptyItemDesc".localized
  }
  struct Purchase {
    static let emptyTotalAmount = "emptyTotalPayableAmount".localized
    static let invalidVoucherAmount = "validTotalPayableAmountRange".localized
    static let emptyRefNo = "emptyTellerReferenceNumber".localized
  }
  struct VerficationPin {
    static let empty = "emptyOTP".localized
    static let invalid = "invalidOTP".localized
    static let expire = "OTPExpired".localized
  }
  struct ScreenTitles {
    static let login = "login".localized
    static let forgotPassword = "forgotPassword".localized
    static let userProfile = "userProfile".localized
    static let editProfile = "editProfile".localized
  }
  
  struct ButtonTitle {
    static let update = "UPDATE".localized
    static let logout = "LOGOUT".localized
  }
  
  struct ErrorAlerts {
    static let unknownError = "unknownError".localized
    static let invalidUrl = "invalidUrl".localized
    static let dataNotFound = "dataNotFound".localized
    static let invalidJson = "invalidJson".localized
    static let missingKeys = "missingKeys".localized
    static let invalidData = "invalidData".localized
    static let responseError = "responseError".localized
    static let dictNotFound = "dictNotFound".localized
  }
}
