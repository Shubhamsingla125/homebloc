//
//  PhotoManager.swift
//  ImageComponentDemo
//
//  Created by Narinder on 30/12/16.
//  Copyright © 2016 Narinder. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import PhotosUI

typealias PhotoTakingHelperCallback = ((UIImage?) -> Void)
typealias CallbackwithURL = ((UIImage?) -> Void)

enum ImagePickerOptions: Int {
    case Camera
    case Gallery
}

private struct constant {
    static let actionTitle = "Choose From"
    static let actionMessage = "Please select an option to choose image"
    static let cameraBtnTitle = "Take Picture"
    static let galeryBtnTitle = "Select From Gallery"
    static let cancelBtnTitle = "Cancel"
}

@objc class PhotoManager:NSObject {
    
    let imagePicker = UIImagePickerController()
    internal var navController: UINavigationController!
    internal var callback: PhotoTakingHelperCallback!
    var allowEditing: Bool
    var currentView : UIView
    
    /*
     Intialize the navController from give reference of navigationcontroller while creating Photomanager class object.
     Callback: Callback will be call after the picking image.
     */
    init(navigationController:UINavigationController, allowEditing:Bool,imagePickerType:ImagePickerOptions,currentView: UIView , callback:@escaping PhotoTakingHelperCallback) {
        
        self.navController = navigationController
        self.callback = callback
        self.allowEditing = allowEditing
        self.currentView = currentView
        super.init()
        if imagePickerType == .Camera {
            if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
                    self.openCameraButton()
            }
        } else {
            self.openPhotoLibraryButton()
        }
//        presentActionSheet()
        
        //        if checkPhotoLibraryPermission() == true {
        //            presentActionSheet()
        //        }else{
        //            //  CommonFunctions.sharedInstance.showAlert(titleStr: "Alert!", messageStr: "Camera permission is required, Please allow camera in settings.", actiontitleStr: "OK", withAction: false, viewController: navigationController.topViewController!)
        //        }
    }
    
    
    //MARK: ImagePicker Custom Functions
    /// Presenting sheet with option to select image source
    private func presentActionSheet() {
        
        let alertController = UIAlertController(title: constant.actionTitle, message: constant.actionMessage, preferredStyle: .actionSheet)
        
        if(UIImagePickerController.isSourceTypeAvailable(.camera))
        {
            let cameraButton = UIAlertAction(title: constant.cameraBtnTitle, style: .default, handler: { (action) -> Void in
                self.openCameraButton()
            })
            alertController.addAction(cameraButton)
        }
        
        let  galleryButton = UIAlertAction(title: constant.galeryBtnTitle, style: .default, handler: { (action) -> Void in
            self.openPhotoLibraryButton()
        })
        alertController.addAction(galleryButton)
        
        
        let cancelButton = UIAlertAction(title: constant.cancelBtnTitle, style: .cancel, handler: { (action) -> Void in
            
        })
        alertController.addAction(cancelButton)
        alertController.popoverPresentationController?.sourceView = currentView
        alertController.popoverPresentationController?.sourceRect = currentView.bounds
        navController.present(alertController, animated: true, completion: nil)
    }
    
    private func openPhotoLibraryButton() {
        PHPhotoLibrary.requestAuthorization({(status:PHAuthorizationStatus)in
            switch status{
            case .denied:
                self.libraryDeniedAlert()
            case .authorized:
                self.presentUIimagePicker(type: .photoLibrary)
            default:
                self.libraryDeniedAlert()
            }
        })
    }
    
    private func libraryDeniedAlert(){
        let alert = UIAlertController(title:AppMessages.AlertTitles.error, message:AppMessages.Unkown.gallery, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: AppMessages.AlertTitles.setting, style: .default, handler: { (_) in
            DispatchQueue.main.async {
                if let settingsURL = URL(string: "\(UIApplicationOpenSettingsURLString)") {
                    UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                }
            }
        }))
        alert.addAction(UIAlertAction(title:AppMessages.AlertTitles.Cancel, style: .cancel, handler: nil))
        self.navController.present(alert, animated: true, completion: nil)
        return
    }
    
    
    private func openCameraButton() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
            if granted == true {
                self.presentUIimagePicker(type: .camera)
            } else {
                let alert = UIAlertController(title:AppMessages.AlertTitles.error, message:AppMessages.Unkown.camera, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: AppMessages.AlertTitles.setting, style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: "\(UIApplicationOpenSettingsURLString)") {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                alert.addAction(UIAlertAction(title:AppMessages.AlertTitles.Cancel, style: .cancel, handler: nil))
                self.navController.present(alert, animated: true, completion: nil)
                return
            }
        });
    }
    
    
    
    /*
     presentUIimagePicker will present the UIImagePicker with give type
     type: Camera or Gallery
     controller: UINavigationcontroller, navigationcontroller on with uiimagepicker will present.
     */
    private func presentUIimagePicker(type: UIImagePickerControllerSourceType){
        imagePicker.allowsEditing = self.allowEditing
        imagePicker.sourceType = type
        imagePicker.delegate = self
        navController.present(imagePicker, animated: true, completion: nil)
    }
    
}
//MARK: ------------------------Class End------------------------------------



//MARK: ------------------------Class Extension------------------------------------

/*Extension for UIImagePickerControllerDelegate & UINavigationControllerDelegate
 
 
 */
extension PhotoManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if allowEditing {
            if #available(iOS 9.1, *) {
                if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                    callback(pickedImage)
                }
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                callback(pickedImage)
            }
            
        }
        navController.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        navController.dismiss(animated: true, completion: nil)
    }
}





