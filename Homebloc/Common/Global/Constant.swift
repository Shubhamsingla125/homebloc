//
//  Constant.swift
//  BaseProject
//
//  Created by TpSingh on 22/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
let linkedInUrl = NSString(string:"https://api.linkedin.com/v1/people/~:(id,industry,firstName,gender,,lastName,email,headline,summary,publicProfileUrl,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),pictureUrls::(original),location:(name))?format=json")
let MOST_POPULAR_MOVIES = "https://api.trakt.tv/movies/popular?extended=full,images"
let CLIENT_ID = "ad005b8c117cdeee58a1bdb7089ea31386cd489b21e14b19818c91511f12a086"
let SEARCH = "https://api.trakt.tv/search/movie?type=movie&query="
// Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String

class Constant {
  struct Time {
    let now = { round(NSDate().timeIntervalSince1970)} // seconds
  }


struct AppColor {  
  static let navigationColor = UIColor.init(red: 107/255, green: 72/255, blue:157/255, alpha: 1.0)
  static let navigationBarTintColor = UIColor.white
  static let navigationColorTextColor = UIColor.white

}
  
  struct ButtonName {
    static let hide:String = "HIDE".localized
    static let show:String = "SHOW".localized
  }
  
  enum FacebookPermissions:String {
    case email = "email"
    case publicProfile = "public_profile"
    case birthday = "user_birthday"
  }

}
