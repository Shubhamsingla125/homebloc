//
//  Utility.swift
//  BaseProject
//
//  Created by MAC on 02/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

class Utility: NSObject {
    class func enableNetworkLog(value:Bool = true){
        Defaults.shared.set(value: value, forKey: .enableLog)
    }
  
    class func addLeftViewInTxtFld(txtFld:UITextField,image:UIImage){
        txtFld.leftViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 20, y: 0, width: 30, height: 30))
        imageView.image = image
        txtFld.leftView = imageView
    }
    
    class func getLanguageLabelModel()->LanguageLabel?{
        if let languageLbls = Defaults.shared.get(forKey: .languageLabel) as? [String:Any]{
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: languageLbls, options: .prettyPrinted)
                let languageList = try JSONDecoder().decode(LanguageLabel.self, from: jsonData)
                return languageList
            } catch(let error){
                print("parsing error----->\(error)")
                return nil
            }
        }
        return nil
    }
    
    class func getSelectedCities()->[City]?{
        if let cityList = Defaults.shared.get(forKey: .selectedCities) as? [[String:Any]]{
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: cityList, options: .prettyPrinted)
                let languageList = try JSONDecoder().decode([City].self, from: jsonData)
                return languageList
            } catch(let error){
                print("parsing error----->\(error)")
                return nil
            }
        }
        return nil
    }
}

