//
//  UserManager.swift
//  BaseProject
//
//  Created by Aj Mehra on 08/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation
import UIKit

class UserManager: NSObject {
    
    class func saveCurrentUser(user:User) {
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: user)
        Defaults.shared.set(value: encodedData, forKey: .user)
        User.sharedInstance = user
    }
    
    class func isUserLoggedIn() -> Bool {
        if Defaults.shared.get(forKey: .user) != nil {
            return true
        }else {
            return false
        }
    }
    /*
    class func saveToken(token:String) {
        Defaults.shared.set(value: token, forKey: .token)
    }
    
    class func getToken() -> String  {
        return Defaults.shared.get(forKey: .token) as? String ?? ""
    }*/
    
    class func getCurrentUser() -> User? {
      
        guard let userData = Defaults.shared.get(forKey: .user) as? Data, let user =
            NSKeyedUnarchiver.unarchiveObject(with: userData) as? User  else {
            return nil
        }
      //User.sharedInstance = user
        return user
    }
    
    class func logout(){
      Defaults.shared.removeAll()
      UserManager.resetNavigation()
    }
  
  class func resetNavigation() {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    appDelegate.navigateToLogin()
    
  }
}
