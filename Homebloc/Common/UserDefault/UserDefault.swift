//
//  UserDefault.swift
//  BaseProject
//
//  Created by Aj Mehra on 08/03/17.
//  Copyright © 2017 Debut Infotech. All rights reserved.
//

import Foundation

enum DefaultKey {
  case user
  //case token
  case enableLog
	case firstLaunch
    case languageCode
    case languageLabel
    case countryCode
    case selectedCities
}

class Defaults {
  //MARK:- SingleTon
  static let shared = Defaults()
  
  //MARK:- Variables
  let userDefault = UserDefaults.standard
  
  //MARK:- Setter
  func set(value:Any,  forKey key:DefaultKey) {
    userDefault.set(value, forKey: String(describing: key))
    userDefault.synchronize()
  }
  
  //MARK:- Getter
  func get(forKey key:DefaultKey) -> Any? {
    return userDefault.object(forKey: String(describing: key))
  }
  
  //MARK:- Methods
  func removeAll() {
    User.sharedInstance.resetUserInstance()
    let appDomain = Bundle.main.bundleIdentifier
    userDefault.removePersistentDomain(forName: appDomain!)
    userDefault.synchronize()
  }
  
  func remove(_ key:DefaultKey) {
    userDefault.removeObject(forKey: String(describing: key))
    userDefault.synchronize()
  }
}
