//
//  UserInfo.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 08/04/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
class UserInfo : Codable {
    var userProfileLocation : String = ""
    let userName : String?
    let mobile : String?
    let sex : String?
    let realnameState : String?
    
    enum CodingKeys: String, CodingKey {
        
        case userProfileLocation = "userProfileLocation"
        case userName = "userName"
        case mobile = "mobile"
        case sex = "sex"
        case realnameState = "realnameState"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userProfileLocation = try values.decodeIfPresent(String.self, forKey: .userProfileLocation) ?? ""
        userName = try values.decodeIfPresent(String.self, forKey: .userName)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        sex = try values.decodeIfPresent(String.self, forKey: .sex)
        realnameState = try values.decodeIfPresent(String.self, forKey: .realnameState)
    }
    
}
