//
//  Hotel.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 09/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
class Hotel : Codable {
    let hotelId : String?
    let hotelName : String?
    let hotelLevel : String?
    let hotelLogo : String?
    let score : String?
    let commentCount : String?
    var services_list : [String] = []
    let locationArea : String?
    let order : String?
    let label : String?
    let price : String?
    let rootType : String?
    let roomsBooked : String?
    let services : [Services] = []
    let reseller_name : String?
    let reseller_phone : String?
    let room_id : String?
    let currency_text : String?
    let currency : String?
    
    let hotelPhone : String?
    enum CodingKeys: String, CodingKey {
        
        case hotelId = "hotelId"
        case hotelName = "hotelName"
        case hotelLevel = "hotelLevel"
        case hotelLogo = "hotelLogo"
        case score = "score"
        case order = "order"
        case commentCount = "commentCount"
        case services_list = "services_list"
        case services = "services"
        case locationArea = "locationArea"
        case label = "label"
        case price = "price"
        case rootType = "rootType"
        case roomsBooked = "roomsbooked"
        case reseller_name = "reseller_name"
        case reseller_phone = "reseller_phone"
        case room_id = "room_id"
        case currency_text = "currency_text"
        case currency = "currency"
        case hotelPhone = "hotelPhone"
    }
 
    
}
