/*
 Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
class HotelDesc : Codable {
    let open_time : String?
    let pl_count : String?
    let shop_area : String?
    let tel : String?
    let introductio : String?
    let img : String?
    let coordinates : String?
    let deposit : String?
    let ticket : String?
    let wake : String?
    let posts : String?
    let foods : String?
    let baby : String?
    let breakfast : String?
    let name : String?
    let star : String?
    let country : String?
    let province : String?
    let city : String?
    let district : String?
    let address : String?
    let stayDate : String?
    let foreignGuests : String?
    let collect : Int?
    let bringPets : String?
    let score : String?
    let services : [Services]?
    let likes : Int?
    let dislikes : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case open_time = "open_time"
        case pl_count = "pl_count"
        case shop_area = "shop_area"
        case tel = "tel"
        case introductio = "introductio"
        case img = "img"
        case coordinates = "coordinates"
        case deposit = "deposit"
        case ticket = "ticket"
        case wake = "wake"
        case posts = "posts"
        case foods = "foods"
        case baby = "baby"
        case breakfast = "breakfast"
        case name = "name"
        case star = "star"
        case country = "country"
        case province = "province"
        case city = "city"
        case district = "district"
        case address = "address"
        case stayDate = "stayDate"
        case foreignGuests = "foreignGuests"
        case collect = "collect"
        case bringPets = "bringPets"
        case score = "score"
        case services = "services"
        case likes = "likes"
        case dislikes = "dislikes"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        open_time = try values.decodeIfPresent(String.self, forKey: .open_time)
        pl_count = try values.decodeIfPresent(String.self, forKey: .pl_count)
        shop_area = try values.decodeIfPresent(String.self, forKey: .shop_area)
        tel = try values.decodeIfPresent(String.self, forKey: .tel)
        introductio = try values.decodeIfPresent(String.self, forKey: .introductio)
        img = try values.decodeIfPresent(String.self, forKey: .img)
        coordinates = try values.decodeIfPresent(String.self, forKey: .coordinates)
        deposit = try values.decodeIfPresent(String.self, forKey: .deposit)
        ticket = try values.decodeIfPresent(String.self, forKey: .ticket)
        wake = try values.decodeIfPresent(String.self, forKey: .wake)
        posts = try values.decodeIfPresent(String.self, forKey: .posts)
        foods = try values.decodeIfPresent(String.self, forKey: .foods)
        baby = try values.decodeIfPresent(String.self, forKey: .baby)
        breakfast = try values.decodeIfPresent(String.self, forKey: .breakfast)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        star = try values.decodeIfPresent(String.self, forKey: .star)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        province = try values.decodeIfPresent(String.self, forKey: .province)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        district = try values.decodeIfPresent(String.self, forKey: .district)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        stayDate = try values.decodeIfPresent(String.self, forKey: .stayDate)
        foreignGuests = try values.decodeIfPresent(String.self, forKey: .foreignGuests)
        collect = try values.decodeIfPresent(Int.self, forKey: .collect)
        bringPets = try values.decodeIfPresent(String.self, forKey: .bringPets)
        score = try values.decodeIfPresent(String.self, forKey: .score)
        services = try values.decodeIfPresent([Services].self, forKey: .services)
        likes = try values.decodeIfPresent(Int.self, forKey: .likes)
        dislikes = try values.decodeIfPresent(Int.self, forKey: .dislikes)
    }
    
}
