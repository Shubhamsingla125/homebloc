//
//  HotelDetail.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 06/03/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
struct HotelDetail : Codable {
    let hotelName : String?
    let hotelLevel : String?
    let decorationYears : String?
    let score : String?
    let commentCount : String?
    let locationArea : String?
    let stayDate : String?
    let hotelRoomList : [HotelRoomList]?
    let likes : Int?
    let dislikes : Int?
    let currency_text : String?
    let currency : String?
    
    enum CodingKeys: String, CodingKey {
        
        case hotelName = "hotelName"
        case hotelLevel = "hotelLevel"
        case decorationYears = "decorationYears"
        case score = "score"
        case commentCount = "commentCount"
        case locationArea = "locationArea"
        case stayDate = "stayDate"
        case hotelRoomList = "hotelRoomList"
        case likes = "likes"
        case dislikes = "dislikes"
        case currency_text = "currency_text"
        case currency = "currency"
    }
    
}
