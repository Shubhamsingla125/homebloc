//
//  HotComment.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 12/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
class HotComment : Codable{
    let score : String?
    let hotcommentUserName : String?
    let hotcommentContent : String?
    let hotcommentSupport : String?
    let hotcommentObject : String?
    let commentList : [CommentList]?
    
    enum CodingKeys: String, CodingKey {
        
        case score = "score"
        case hotcommentUserName = "hotcommentUserName"
        case hotcommentContent = "hotcommentContent"
        case hotcommentSupport = "hotcommentSupport"
        case hotcommentObject = "hotcommentObject"
        case commentList = "commentList"
    }
}
