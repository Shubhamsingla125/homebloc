//
//  City.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 04/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
class City : Codable {
    let citycode : String?
    let cityname : String?
    let country_code : String?
    
    
    enum CodingKeys: String, CodingKey {
        case citycode
        case cityname
        case country_code
    }
}

