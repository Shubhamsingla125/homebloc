/*
 Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
class Order : Codable {
    let id : String?
    let seller_id : String?
    let room_id : String?
    let user_id : String?
    let coupons_id : String?
    let order_no : String?
    let seller_name : String?
    let seller_address : String?
    let coordinates : String?
    let arrival_time : String?
    let departure_time : String?
    let dd_time : String?
    let price : String?
    let num : String?
    let days : String?
    let room_type : String?
    let room_logo : String?
    let bed_type : String?
    let name : String?
    let tel : String?
    let status : String?
    let out_trade_no : String?
    let dis_cost : String?
    let yj_cost : String?
    let yhq_cost : String?
    let yyzk_cost : String?
    let total_cost : String?
    let is_delete : String?
    let time : String?
    let uniacid : String?
    let ytyj_cost : String?
    let hb_cost : String?
    let hb_id : String?
    let from_id : String?
    let classify : String?
    let type : String?
    let code : String?
    let jj_time : String?
    let is_pj : String?
    let zruser_id : String?
    let zrname : String?
    let zrtel : String?
    let newtotal_co : String?
    let order_id : String?
    let is_zhuan : String?
    let score : String?
    let score_cost : String?
    let desc : String?
    let zr_time : String?
    let is_sms : String?
    let payment_token : String?
    let payment_date : String?
    let adults : String?
    let children : String?
    let currencytext : String?
    let currency : String?
    let resell_price : String?
    let resell_date : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case seller_id = "seller_id"
        case room_id = "room_id"
        case user_id = "user_id"
        case coupons_id = "coupons_id"
        case order_no = "order_no"
        case seller_name = "seller_name"
        case seller_address = "seller_address"
        case coordinates = "coordinates"
        case arrival_time = "arrival_time"
        case departure_time = "departure_time"
        case dd_time = "dd_time"
        case price = "price"
        case num = "num"
        case days = "days"
        case room_type = "room_type"
        case room_logo = "room_logo"
        case bed_type = "bed_type"
        case name = "name"
        case tel = "tel"
        case status = "status"
        case out_trade_no = "out_trade_no"
        case dis_cost = "dis_cost"
        case yj_cost = "yj_cost"
        case yhq_cost = "yhq_cost"
        case yyzk_cost = "yyzk_cost"
        case total_cost = "total_cost"
        case is_delete = "is_delete"
        case time = "time"
        case uniacid = "uniacid"
        case ytyj_cost = "ytyj_cost"
        case hb_cost = "hb_cost"
        case hb_id = "hb_id"
        case from_id = "from_id"
        case classify = "classify"
        case type = "type"
        case code = "code"
        case jj_time = "jj_time"
        case is_pj = "is_pj"
        case zruser_id = "zruser_id"
        case zrname = "zrname"
        case zrtel = "zrtel"
        case newtotal_co = "newtotal_co"
        case order_id = "order_id"
        case is_zhuan = "is_zhuan"
        case score = "score"
        case score_cost = "score_cost"
        case desc = "desc"
        case zr_time = "zr_time"
        case is_sms = "is_sms"
        case payment_token = "payment_token"
        case payment_date = "payment_date"
        case adults = "adults"
        case children = "children"
        case currencytext = "currencytext"
        case currency = "currency"
        case resell_price = "resell_price"
        case resell_date = "resell_date"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        seller_id = try values.decodeIfPresent(String.self, forKey: .seller_id)
        room_id = try values.decodeIfPresent(String.self, forKey: .room_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        coupons_id = try values.decodeIfPresent(String.self, forKey: .coupons_id)
        order_no = try values.decodeIfPresent(String.self, forKey: .order_no)
        seller_name = try values.decodeIfPresent(String.self, forKey: .seller_name)
        seller_address = try values.decodeIfPresent(String.self, forKey: .seller_address)
        coordinates = try values.decodeIfPresent(String.self, forKey: .coordinates)
        arrival_time = try values.decodeIfPresent(String.self, forKey: .arrival_time)
        departure_time = try values.decodeIfPresent(String.self, forKey: .departure_time)
        dd_time = try values.decodeIfPresent(String.self, forKey: .dd_time)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        num = try values.decodeIfPresent(String.self, forKey: .num)
        days = try values.decodeIfPresent(String.self, forKey: .days)
        room_type = try values.decodeIfPresent(String.self, forKey: .room_type)
        room_logo = try values.decodeIfPresent(String.self, forKey: .room_logo)
        bed_type = try values.decodeIfPresent(String.self, forKey: .bed_type)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        tel = try values.decodeIfPresent(String.self, forKey: .tel)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        out_trade_no = try values.decodeIfPresent(String.self, forKey: .out_trade_no)
        dis_cost = try values.decodeIfPresent(String.self, forKey: .dis_cost)
        yj_cost = try values.decodeIfPresent(String.self, forKey: .yj_cost)
        yhq_cost = try values.decodeIfPresent(String.self, forKey: .yhq_cost)
        yyzk_cost = try values.decodeIfPresent(String.self, forKey: .yyzk_cost)
        total_cost = try values.decodeIfPresent(String.self, forKey: .total_cost)
        is_delete = try values.decodeIfPresent(String.self, forKey: .is_delete)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        uniacid = try values.decodeIfPresent(String.self, forKey: .uniacid)
        ytyj_cost = try values.decodeIfPresent(String.self, forKey: .ytyj_cost)
        hb_cost = try values.decodeIfPresent(String.self, forKey: .hb_cost)
        hb_id = try values.decodeIfPresent(String.self, forKey: .hb_id)
        from_id = try values.decodeIfPresent(String.self, forKey: .from_id)
        classify = try values.decodeIfPresent(String.self, forKey: .classify)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        code = try values.decodeIfPresent(String.self, forKey: .code)
        jj_time = try values.decodeIfPresent(String.self, forKey: .jj_time)
        is_pj = try values.decodeIfPresent(String.self, forKey: .is_pj)
        zruser_id = try values.decodeIfPresent(String.self, forKey: .zruser_id)
        zrname = try values.decodeIfPresent(String.self, forKey: .zrname)
        zrtel = try values.decodeIfPresent(String.self, forKey: .zrtel)
        newtotal_co = try values.decodeIfPresent(String.self, forKey: .newtotal_co)
        order_id = try values.decodeIfPresent(String.self, forKey: .order_id)
        is_zhuan = try values.decodeIfPresent(String.self, forKey: .is_zhuan)
        score = try values.decodeIfPresent(String.self, forKey: .score)
        score_cost = try values.decodeIfPresent(String.self, forKey: .score_cost)
        desc = try values.decodeIfPresent(String.self, forKey: .desc)
        zr_time = try values.decodeIfPresent(String.self, forKey: .zr_time)
        is_sms = try values.decodeIfPresent(String.self, forKey: .is_sms)
        payment_token = try values.decodeIfPresent(String.self, forKey: .payment_token)
        payment_date = try values.decodeIfPresent(String.self, forKey: .payment_date)
        adults = try values.decodeIfPresent(String.self, forKey: .adults)
        children = try values.decodeIfPresent(String.self, forKey: .children)
        currencytext = try values.decodeIfPresent(String.self, forKey: .currencytext)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        resell_price = try values.decodeIfPresent(String.self, forKey: .resell_price)
        resell_date = try values.decodeIfPresent(String.self, forKey: .resell_date)
    }
    
}
