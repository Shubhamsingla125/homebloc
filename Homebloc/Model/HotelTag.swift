//
//  HotelTag.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 09/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
class HotelTag : Codable {
    let tag_id : String?
    let uniacid : String?
    let tag_name : String?
     let tag_ename : String?
    let sort : String?
    let img : String?
    let min : Int?
    let max : Int?
    
    enum CodingKeys: String, CodingKey {
        case tag_id = "tag_id"
        case uniacid = "uniacid"
        case tag_name = "tag_name"
        case tag_ename = "tag_ename"
        case sort = "sort"
        case img = "img"
        case min = "min"
        case max = "max"
    }
}
