//
//  Language.swift
//  Homebloc
//
//  Created by Mukesh Khulve on 01/02/19.
//  Copyright © 2019 ios dev. All rights reserved.
//

import Foundation
class Language : Codable {
    let code : String?
    let language : String?
    
    enum CodingKeys: String, CodingKey {
        case code
        case language
    }
    
}


