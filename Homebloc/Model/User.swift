//
//	User.swift
//

import Foundation

class User: NSObject, NSCoding {
  
  static var sharedInstance = User()
  
  //MARK:- Properties
  
  var id: String?
  var aboutMe: String?
  var deviceToken: String?
  var oauthToken: String?
  var email: String?
  var firstName: String?
  var lastName: String?
  var profilePicUrl: String?
  var dateOfBirth: String?
  var gender: String?
  var firebaseProfileImageName: String?
  
  
  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let email = "email"
    static let id = "id"
    static let lastName = "lastName"
    static let oauthToken = "oauthToken"
    static let firstName = "firstName"
    static let aboutMe = "aboutMe"
    static let deviceToken = "deviceToken"
    static let profilePicUrl = "profilePic_url"
    static let firebaseProfileImageName = "firebaseProfileImageName"
    
  }
  
  func resetUserInstance() {
    self.email = nil
    self.id = nil
    self.lastName = nil
    self.firstName = nil
    self.deviceToken = nil
    self.aboutMe = nil
    self.oauthToken = nil
    self.profilePicUrl = nil
    self.firebaseProfileImageName = nil
  }
  
  
  //MARK:- Default Initializer
  override init () {
    // uncomment this line if your class has been inherited from any other class
    //super.init()
  }
  
  /**
   * Instantiate the instance using the passed dictionary values to set the properties values
   */
  
  convenience init(_ dictionary: Parameters) {
    self.init()
    id = dictionary["_id"] as? String ?? ""
    aboutMe = dictionary["aboutMe"] as? String ?? ""
    deviceToken = dictionary["deviceToken"] as? String ?? ""
    email = dictionary["email"] as? String ?? ""
    firstName = dictionary["first_name"] as? String ?? ""
    lastName = dictionary["last_name"] as? String ?? ""
    profilePicUrl = dictionary["profile_pic"] as? String ?? ""
    firebaseProfileImageName = dictionary["firebaseProfileImageName"] as? String ?? ""
  }
  
  
  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
    self.aboutMe = aDecoder.decodeObject(forKey: SerializationKeys.aboutMe) as? String
    self.oauthToken = aDecoder.decodeObject(forKey: SerializationKeys.oauthToken) as? String
    self.profilePicUrl = aDecoder.decodeObject(forKey: SerializationKeys.profilePicUrl) as? String
    self.firebaseProfileImageName = aDecoder.decodeObject(forKey: SerializationKeys.firebaseProfileImageName) as? String
  
  }
  
  public func encode(with aCoder: NSCoder) {
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
    aCoder.encode(oauthToken, forKey: SerializationKeys.oauthToken)
    aCoder.encode(aboutMe, forKey: SerializationKeys.aboutMe)
    aCoder.encode(profilePicUrl, forKey: SerializationKeys.profilePicUrl)
    aCoder.encode(firebaseProfileImageName, forKey: SerializationKeys.firebaseProfileImageName)
  }
  
  
}





