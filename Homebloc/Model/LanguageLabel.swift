/*
 Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
class LanguageLabel : Codable {
    let lBL_FIRST_NAME : String?
    let lBL_LAST_NAME : String?
    let lBL_PHONE_NUMBER : String?
    let lBL_PASSWORD : String?
    let lBL_LOGIN : String?
    let lBL_SIGN_UP : String?
    let lBL_SIGN_IN : String?
    let lBL_OR_CONNECT_WITH : String?
    let lBL_DONT_HAVE_ACCOUNT : String?
    let lBL_HOMEBLOC : String?
    let lBL_ALREADY_HAVE_ACCOUNT : String?
    let lBL_DATA_CONNECTION_AVAILABLE : String?
    let lBL_TURN_ON_DATA_COLLECTION_TRY_LATER : String?
    let lBL_ENTER_VALID_MOBILE_NUMBER : String?
    let lBL_SELECT_COUNTRY_CODE : String?
    let lBL_SEND_OTP : String?
    let lBL_PLEASE_ENTER_VALID_OTP : String?
    let lBL_PLEASE_ENTER_VALID_PASSWORD : String?
    let lBL_SUBMIT : String?
    let lBL_ENTER_CODE : String?
    let lBL_ACQUIRE_CODE : String?
    let lBL_INVITATION_CODE : String?
    let lBL_PLEASE_ENTER_USERNAME : String?
    let lBL_PLEASE_ENTER_CODE : String?
    let lBL_PLEASE_ENTER_PASSWORD : String?
    let lBL_PLEASE_ENTER_PHONE : String?
    let lBL_PLEASE_RE_ENTER_PASSWORD : String?
    let lBL_PASSWORD_MISMATCH : String?
    let lBL_PLEASE_WAIT : String?
    let lBL_SKIP_PASSWORD : String?
    let lBL_CONFIRM_FINGERPRINT_TO_CONTINUE : String?
    let lBL_TOUCH_PHONE_SENSOR : String?
    let lBL_TEST : String?
    let lBL_SERVICE_DEPOSIT : String?
    let lBL_SERVICE_TICKET : String?
    let lBL_SERVICE_WAKE : String?
    let lBL_SERVICE_POSTS : String?
    let lBL_SERVICE_FOODS : String?
    let lBL_SERVICE_BREAKFAST : String?
    let lBL_GOOD : String?
    let lBL_YES : String?
    let lBL_NO : String?
    let lBL_CANCEL : String?
    let lBL_DESC : String?
    let lBL_OK : String?
    let lBL_CITY : String?
    let lBL_AMEN : String?
    let lBL_ADDITIONAL_INFO : String?
    let lBL_WRITE_COMMENT : String?
    let lBL_POST_REVIEW : String?
    let lBL_REVIEW : String?
    let lBL_SELECT_PASSWORD : String?
    let lBL_PROCEED : String?
    let lBL_END_DATE : String?
    let lBL_START_DATE : String?
    let lBL_INTERNATIONAL : String?
    let lBL_CONFIRM : String?
    let lBL_DETAIL : String?
    let lBL_CONTACT : String?
    let lBL_NAME : String?
    let lBL_ROOMS : String?
    let lBL_NO_DATA_AVAILABLE : String?
    let lBL_DOMESTIC : String?
    let lBL_SELECT_DATE : String?
    let lBL_SELECT_DATE_ERROR : String?
    let lBL_VERY_GOOD : String?
    let lBL_AVERAGE : String?
    let lBL_USER_COMMENTS : String?
    let lBL_RESERVATION : String?
    let lBL_COMMENTS : String?
    let lBL_CHECK_IN_OUT : String?
    let lBL_LANGUAGE_SERVICE : String?
    let lBL_PET_ALLWANCE : String?
    let lBL_SALE : String?
    let lBL_RESERVE : String?
    let lBL_RETURN : String?
    let lBL_DATES : String?
    let lBL_NIGHT : String?
    let lBL_AMOUNT : String?
    let lBL_DISCOUNT_STRING : String?
    let lBL_PEAK_BOOKING_TEXT : String?
    let lBL_REQUEST_SLIP : String?
    let lBL_WRITE_REVIEW : String?
    let lBL_SEARCH : String?
    let lBL_PROFIT_GENERATING : String?
    let lBL_LUCKY_POOL : String?
    let lBL_SELECT_CITY : String?
    let lBL_SELECT_HINT_TEXT : String?
    let lBL_CURRENT_CITY : String?
    let lBL_HISTORY : String?
    let lBL_POPULAR_CITY : String?
    let lBL_WELCOME_TO_APP : String?
    let lBL_A_SECOND : String?
    let lBL_SEARCH_HOTEL : String?
    let lBL_HOTEL : String?
    let lBL_BIG_BED : String?
    let lBL_DOUBLE_BED : String?
    let lBL_SINGLE_BED : String?
    let lBL_MULTIPLE_BED : String?
    let lBL_HOTEL_FEATURED : String?
    let lBL_SEARCH_RESULT : String?
    let lBL_RECOMENDED : String?
    let lBL_PRICE_LOW_TO_HIGH : String?
    let lBL_PRICE_HIGH_TO_LOW : String?
    let lBL_PRICE_HIGHLY_RATED : String?
    let lBL_PRICE_NEARBY : String?
    let lBL_SUGGESTED : String?
    let lBL_AREA : String?
    let lBL_PRICE : String?
    let lBL_ALL : String?
    let lBL_PAYMENT_DETAIL : String?
    let lBL_PAY_WITH_HOC : String?
    let lBL_DEBIT_CARD_NO : String?
    let lBL_ENTER_CARD_NO_XXX : String?
    let lBL_EXPIRY_DATE : String?
    let lBL_MONTH : String?
    let lBL_YEAR : String?
    let lBL_CVV_INFO : String?
    let lBL_CVV : String?
    let lBL_ERROR_ROOM_COUNT : String?
    let lBL_ERROR_USER_NAME : String?
    let lBL_ERROR_PHONE_NO : String?
    let lBL_INVALID_CARD_INFO : String?
    let lBL_BOOKING : String?
    let lBL_CONFIRM_BOOKING_CHECK_INFO : String?
    let lBL_OVER_NIGHT : String?
    let lBL_SIX_DIGIT_PASSCODE : String?
    let lBL_PENDING : String?
    let lBL_PROCESSING : String?
    let lBL_ACCOUNT_SETTING : String?
    let lBL_MY_RESERVATION : String?
    let lBL_DAYS : String?
    let lBL_CHECK_IN : String?
    let lBL_CHECK_OUT : String?
    let lBL_CONFIRMED_READY : String?
    let lBL_DURATION : String?
    let lBL_MALE : String?
    let lBL_FEMALE : String?
    let lBL_PERSONAL_INFO : String?
    let lBL_THUMB_PHOTO : String?
    let lBL_NIKNAME : String?
    let lBL_MY_INVITATION : String?
    let lBL_GENDER : String?
    let lBL_SOCIAL : String?
    let lBL_AUTHENTICATION : String?
    let lBL_PASSCODE : String?
    let lBL_COMPLETED : String?
    let lBL_DEVIDE_END : String?
    let lBL_COMMING_SOON : String?
    let lBL_ETH_ADDRESS : String?
    let lBL_OFFICIAL_EVENTS : String?
    let lBL_INVITATION : String?
    let lBL_EVENTS : String?
    let lBL_FEEDBACK_TO_APP : String?
    let lBL_TAKE_IMAGE : String?
    let lBL_CHOOSE_FROM_GALLERY : String?
    let lBL_SOCAIL_EVENTS : String?
    let lBL_OFFICIAL_ACCOUNT : String?
    let lBL_OFFICIAL_ACCOUNT_INFO : String?
    let lBL_SCAN_QR_TEXT : String?
    let lBL_OFFICIAL_WE_CHAT_GROUP : String?
    let lBL_FEEDBACK : String?
    let lBL_COMMENT_FOR_REWARDS : String?
    let lBL_SUBMITT : String?
    let lBL_ABOUT_HOMEBLOC : String?
    let lBL_CREDIT : String?
    let lBL_CREDIT_INFO : String?
    let lBL_VALUE : String?
    let lBL_VALUE_INFO : String?
    let lBL_DEVIDE_END_INFO : String?
    let lBL_CUSTOMER_SERVICE : String?
    let lBL_BEFORE : String?
    let lBL_CUSTOMER_NAME : String?
    let lBL_SOURCE : String?
    let lBL_NOTICE : String?
    let lBL_NOTICE_TEXT : String?
    let lBL_COUNTER : String?
    let lBL_BUY_FROM_LUCKY_POOL : String?
    let lBL_PURCHASE : String?
    let lBL_EMAIL : String?
    let lBL_ERROR_EMAIL : String?
    let lBL_FORGOT_PASSWORD : String?
    let lBL_ENTER_MOBILE_AND_VERIFY : String?
    let lBL_ENTER_NEW_PASSWORD : String?
    let lBL_VERIFY_NEW_PASSWORD : String?
    let lBL_RECOVER_PASSWORD : String?
    let lBL_PLEASE_ENTER_NEW_PASSWORD : String?
    
    enum CodingKeys: String, CodingKey {
        
        case lBL_FIRST_NAME = "LBL_FIRST_NAME"
        case lBL_LAST_NAME = "LBL_LAST_NAME"
        case lBL_PHONE_NUMBER = "LBL_PHONE_NUMBER"
        case lBL_PASSWORD = "LBL_PASSWORD"
        case lBL_LOGIN = "LBL_LOGIN"
        case lBL_SIGN_UP = "LBL_SIGN_UP"
        case lBL_SIGN_IN = "LBL_SIGN_IN"
        case lBL_OR_CONNECT_WITH = "LBL_OR_CONNECT_WITH"
        case lBL_DONT_HAVE_ACCOUNT = "LBL_DONT_HAVE_ACCOUNT"
        case lBL_HOMEBLOC = "LBL_HOMEBLOC"
        case lBL_ALREADY_HAVE_ACCOUNT = "LBL_ALREADY_HAVE_ACCOUNT"
        case lBL_DATA_CONNECTION_AVAILABLE = "LBL_DATA_CONNECTION_AVAILABLE"
        case lBL_TURN_ON_DATA_COLLECTION_TRY_LATER = "LBL_TURN_ON_DATA_COLLECTION_TRY_LATER"
        case lBL_ENTER_VALID_MOBILE_NUMBER = "LBL_ENTER_VALID_MOBILE_NUMBER"
        case lBL_SELECT_COUNTRY_CODE = "LBL_SELECT_COUNTRY_CODE"
        case lBL_SEND_OTP = "LBL_SEND_OTP"
        case lBL_PLEASE_ENTER_VALID_OTP = "LBL_PLEASE_ENTER_VALID_OTP"
        case lBL_PLEASE_ENTER_VALID_PASSWORD = "LBL_PLEASE_ENTER_VALID_PASSWORD"
        case lBL_SUBMIT = "LBL_SUBMIT"
        case lBL_ENTER_CODE = "LBL_ENTER_CODE"
        case lBL_ACQUIRE_CODE = "LBL_ACQUIRE_CODE"
        case lBL_INVITATION_CODE = "LBL_INVITATION_CODE"
        case lBL_PLEASE_ENTER_USERNAME = "LBL_PLEASE_ENTER_USERNAME"
        case lBL_PLEASE_ENTER_CODE = "LBL_PLEASE_ENTER_CODE"
        case lBL_PLEASE_ENTER_PASSWORD = "LBL_PLEASE_ENTER_PASSWORD"
        case lBL_PLEASE_ENTER_PHONE = "LBL_PLEASE_ENTER_PHONE"
        case lBL_PLEASE_RE_ENTER_PASSWORD = "LBL_PLEASE_RE_ENTER_PASSWORD"
        case lBL_PASSWORD_MISMATCH = "LBL_PASSWORD_MISMATCH"
        case lBL_PLEASE_WAIT = "LBL_PLEASE_WAIT"
        case lBL_SKIP_PASSWORD = "LBL_SKIP_PASSWORD"
        case lBL_CONFIRM_FINGERPRINT_TO_CONTINUE = "LBL_CONFIRM_FINGERPRINT_TO_CONTINUE"
        case lBL_TOUCH_PHONE_SENSOR = "LBL_TOUCH_PHONE_SENSOR"
        case lBL_TEST = "LBL_TEST"
        case lBL_SERVICE_DEPOSIT = "LBL_SERVICE_DEPOSIT"
        case lBL_SERVICE_TICKET = "LBL_SERVICE_TICKET"
        case lBL_SERVICE_WAKE = "LBL_SERVICE_WAKE"
        case lBL_SERVICE_POSTS = "LBL_SERVICE_POSTS"
        case lBL_SERVICE_FOODS = "LBL_SERVICE_FOODS"
        case lBL_SERVICE_BREAKFAST = "LBL_SERVICE_BREAKFAST"
        case lBL_GOOD = "LBL_GOOD"
        case lBL_YES = "LBL_YES"
        case lBL_NO = "LBL_NO"
        case lBL_CANCEL = "LBL_CANCEL"
        case lBL_DESC = "LBL_DESC"
        case lBL_OK = "LBL_OK"
        case lBL_CITY = "LBL_CITY"
        case lBL_AMEN = "LBL_AMEN"
        case lBL_ADDITIONAL_INFO = "LBL_ADDITIONAL_INFO"
        case lBL_WRITE_COMMENT = "LBL_WRITE_COMMENT"
        case lBL_POST_REVIEW = "LBL_POST_REVIEW"
        case lBL_REVIEW = "LBL_REVIEW"
        case lBL_SELECT_PASSWORD = "LBL_SELECT_PASSWORD"
        case lBL_PROCEED = "LBL_PROCEED"
        case lBL_END_DATE = "LBL_END_DATE"
        case lBL_START_DATE = "LBL_START_DATE"
        case lBL_INTERNATIONAL = "LBL_INTERNATIONAL"
        case lBL_CONFIRM = "LBL_CONFIRM"
        case lBL_DETAIL = "LBL_DETAIL"
        case lBL_CONTACT = "LBL_CONTACT"
        case lBL_NAME = "LBL_NAME"
        case lBL_ROOMS = "LBL_ROOMS"
        case lBL_NO_DATA_AVAILABLE = "LBL_NO_DATA_AVAILABLE"
        case lBL_DOMESTIC = "LBL_DOMESTIC"
        case lBL_SELECT_DATE = "LBL_SELECT_DATE"
        case lBL_SELECT_DATE_ERROR = "LBL_SELECT_DATE_ERROR"
        case lBL_VERY_GOOD = "LBL_VERY_GOOD"
        case lBL_AVERAGE = "LBL_AVERAGE"
        case lBL_USER_COMMENTS = "LBL_USER_COMMENTS"
        case lBL_RESERVATION = "LBL_RESERVATION"
        case lBL_COMMENTS = "LBL_COMMENTS"
        case lBL_CHECK_IN_OUT = "LBL_CHECK_IN_OUT"
        case lBL_LANGUAGE_SERVICE = "LBL_LANGUAGE_SERVICE"
        case lBL_PET_ALLWANCE = "LBL_PET_ALLWANCE"
        case lBL_SALE = "LBL_SALE"
        case lBL_RESERVE = "LBL_RESERVE"
        case lBL_RETURN = "LBL_RETURN"
        case lBL_DATES = "LBL_DATES"
        case lBL_NIGHT = "LBL_NIGHT"
        case lBL_AMOUNT = "LBL_AMOUNT"
        case lBL_DISCOUNT_STRING = "LBL_DISCOUNT_STRING"
        case lBL_PEAK_BOOKING_TEXT = "LBL_PEAK_BOOKING_TEXT"
        case lBL_REQUEST_SLIP = "LBL_REQUEST_SLIP"
        case lBL_WRITE_REVIEW = "LBL_WRITE_REVIEW"
        case lBL_SEARCH = "LBL_SEARCH"
        case lBL_PROFIT_GENERATING = "LBL_PROFIT_GENERATING"
        case lBL_LUCKY_POOL = "LBL_LUCKY_POOL"
        case lBL_SELECT_CITY = "LBL_SELECT_CITY"
        case lBL_SELECT_HINT_TEXT = "LBL_SELECT_HINT_TEXT"
        case lBL_CURRENT_CITY = "LBL_CURRENT_CITY"
        case lBL_HISTORY = "LBL_HISTORY"
        case lBL_POPULAR_CITY = "LBL_POPULAR_CITY"
        case lBL_WELCOME_TO_APP = "LBL_WELCOME_TO_APP"
        case lBL_A_SECOND = "LBL_A_SECOND"
        case lBL_SEARCH_HOTEL = "LBL_SEARCH_HOTEL"
        case lBL_HOTEL = "LBL_HOTEL"
        case lBL_BIG_BED = "LBL_BIG_BED"
        case lBL_DOUBLE_BED = "LBL_DOUBLE_BED"
        case lBL_SINGLE_BED = "LBL_SINGLE_BED"
        case lBL_MULTIPLE_BED = "LBL_MULTIPLE_BED"
        case lBL_HOTEL_FEATURED = "LBL_HOTEL_FEATURED"
        case lBL_SEARCH_RESULT = "LBL_SEARCH_RESULT"
        case lBL_RECOMENDED = "LBL_RECOMENDED"
        case lBL_PRICE_LOW_TO_HIGH = "LBL_PRICE_LOW_TO_HIGH"
        case lBL_PRICE_HIGH_TO_LOW = "LBL_PRICE_HIGH_TO_LOW"
        case lBL_PRICE_HIGHLY_RATED = "LBL_PRICE_HIGHLY_RATED"
        case lBL_PRICE_NEARBY = "LBL_PRICE_NEARBY"
        case lBL_SUGGESTED = "LBL_SUGGESTED"
        case lBL_AREA = "LBL_AREA"
        case lBL_PRICE = "LBL_PRICE"
        case lBL_ALL = "LBL_ALL"
        case lBL_PAYMENT_DETAIL = "LBL_PAYMENT_DETAIL"
        case lBL_PAY_WITH_HOC = "LBL_PAY_WITH_HOC"
        case lBL_DEBIT_CARD_NO = "LBL_DEBIT_CARD_NO"
        case lBL_ENTER_CARD_NO_XXX = "LBL_ENTER_CARD_NO_XXX"
        case lBL_EXPIRY_DATE = "LBL_EXPIRY_DATE"
        case lBL_MONTH = "LBL_MONTH"
        case lBL_YEAR = "LBL_YEAR"
        case lBL_CVV_INFO = "LBL_CVV_INFO"
        case lBL_CVV = "LBL_CVV"
        case lBL_ERROR_ROOM_COUNT = "LBL_ERROR_ROOM_COUNT"
        case lBL_ERROR_USER_NAME = "LBL_ERROR_USER_NAME"
        case lBL_ERROR_PHONE_NO = "LBL_ERROR_PHONE_NO"
        case lBL_INVALID_CARD_INFO = "LBL_INVALID_CARD_INFO"
        case lBL_BOOKING = "LBL_BOOKING"
        case lBL_CONFIRM_BOOKING_CHECK_INFO = "LBL_CONFIRM_BOOKING_CHECK_INFO"
        case lBL_OVER_NIGHT = "LBL_OVER_NIGHT"
        case lBL_SIX_DIGIT_PASSCODE = "LBL_SIX_DIGIT_PASSCODE"
        case lBL_PENDING = "LBL_PENDING"
        case lBL_PROCESSING = "LBL_PROCESSING"
        case lBL_ACCOUNT_SETTING = "LBL_ACCOUNT_SETTING"
        case lBL_MY_RESERVATION = "LBL_MY_RESERVATION"
        case lBL_DAYS = "LBL_DAYS"
        case lBL_CHECK_IN = "LBL_CHECK_IN"
        case lBL_CHECK_OUT = "LBL_CHECK_OUT"
        case lBL_CONFIRMED_READY = "LBL_CONFIRMED_READY"
        case lBL_DURATION = "LBL_DURATION"
        case lBL_MALE = "LBL_MALE"
        case lBL_FEMALE = "LBL_FEMALE"
        case lBL_PERSONAL_INFO = "LBL_PERSONAL_INFO"
        case lBL_THUMB_PHOTO = "LBL_THUMB_PHOTO"
        case lBL_NIKNAME = "LBL_NIKNAME"
        case lBL_MY_INVITATION = "LBL_MY_INVITATION"
        case lBL_GENDER = "LBL_GENDER"
        case lBL_SOCIAL = "LBL_SOCIAL"
        case lBL_AUTHENTICATION = "LBL_AUTHENTICATION"
        case lBL_PASSCODE = "LBL_PASSCODE"
        case lBL_COMPLETED = "LBL_COMPLETED"
        case lBL_DEVIDE_END = "LBL_DEVIDE_END"
        case lBL_COMMING_SOON = "LBL_COMMING_SOON"
        case lBL_ETH_ADDRESS = "LBL_ETH_ADDRESS"
        case lBL_OFFICIAL_EVENTS = "LBL_OFFICIAL_EVENTS"
        case lBL_INVITATION = "LBL_INVITATION"
        case lBL_EVENTS = "LBL_EVENTS"
        case lBL_FEEDBACK_TO_APP = "LBL_FEEDBACK_TO_APP"
        case lBL_TAKE_IMAGE = "LBL_TAKE_IMAGE"
        case lBL_CHOOSE_FROM_GALLERY = "LBL_CHOOSE_FROM_GALLERY"
        case lBL_SOCAIL_EVENTS = "LBL_SOCAIL_EVENTS"
        case lBL_OFFICIAL_ACCOUNT = "LBL_OFFICIAL_ACCOUNT"
        case lBL_OFFICIAL_ACCOUNT_INFO = "LBL_OFFICIAL_ACCOUNT_INFO"
        case lBL_SCAN_QR_TEXT = "LBL_SCAN_QR_TEXT"
        case lBL_OFFICIAL_WE_CHAT_GROUP = "LBL_OFFICIAL_WE_CHAT_GROUP"
        case lBL_FEEDBACK = "LBL_FEEDBACK"
        case lBL_COMMENT_FOR_REWARDS = "LBL_COMMENT_FOR_REWARDS"
        case lBL_SUBMITT = "LBL_SUBMITT"
        case lBL_ABOUT_HOMEBLOC = "LBL_ABOUT_HOMEBLOC"
        case lBL_CREDIT = "LBL_CREDIT"
        case lBL_CREDIT_INFO = "LBL_CREDIT_INFO"
        case lBL_VALUE = "LBL_VALUE"
        case lBL_VALUE_INFO = "LBL_VALUE_INFO"
        case lBL_DEVIDE_END_INFO = "LBL_DEVIDE_END_INFO"
        case lBL_CUSTOMER_SERVICE = "LBL_CUSTOMER_SERVICE"
        case lBL_BEFORE = "LBL_BEFORE"
        case lBL_CUSTOMER_NAME = "LBL_CUSTOMER_NAME"
        case lBL_SOURCE = "LBL_SOURCE"
        case lBL_NOTICE = "LBL_NOTICE"
        case lBL_NOTICE_TEXT = "LBL_NOTICE_TEXT"
        case lBL_COUNTER = "LBL_COUNTER"
        case lBL_BUY_FROM_LUCKY_POOL = "LBL_BUY_FROM_LUCKY_POOL"
        case lBL_PURCHASE = "LBL_PURCHASE"
        case lBL_EMAIL = "LBL_EMAIL"
        case lBL_ERROR_EMAIL = "LBL_ERROR_EMAIL"
        case lBL_FORGOT_PASSWORD = "LBL_FORGOT_PASSWORD"
        case lBL_ENTER_MOBILE_AND_VERIFY = "LBL_ENTER_MOBILE_AND_VERIFY"
        case lBL_ENTER_NEW_PASSWORD = "LBL_ENTER_NEW_PASSWORD"
        case lBL_VERIFY_NEW_PASSWORD = "LBL_VERIFY_NEW_PASSWORD"
        case lBL_RECOVER_PASSWORD = "LBL_RECOVER_PASSWORD"
        case lBL_PLEASE_ENTER_NEW_PASSWORD = "LBL_PLEASE_ENTER_NEW_PASSWORD"
    }
    
}
/*{
 "responseData": 1,
 "languages": {
 "LBL_FIRST_NAME": "FIRST NAME",
 "LBL_LAST_NAME": "LAST NAME",
 "LBL_PHONE_NUMBER": "Phone number",
 "LBL_PASSWORD": "Password",
 "LBL_LOGIN": "Login",
 "LBL_SIGN_UP": "Sign Up",
 "LBL_SIGN_IN": "Sign In",
 "LBL_OR_CONNECT_WITH": "OR CONNECT WITH",
 "LBL_DONT_HAVE_ACCOUNT": "Don't have an account?",
 "LBL_HOMEBLOC": "HOMEBLOC",
 "LBL_ALREADY_HAVE_ACCOUNT": "Already have an account?",
 "LBL_DATA_CONNECTION_AVAILABLE": "Data Connection Unavailable",
 "LBL_TURN_ON_DATA_COLLECTION_TRY_LATER": "Turn on Data connection and try later",
 "LBL_ENTER_VALID_MOBILE_NUMBER": "Enter valid mobile number",
 "LBL_SELECT_COUNTRY_CODE": "Please select your country code",
 "LBL_SEND_OTP": "Send Otp",
 "LBL_PLEASE_ENTER_VALID_OTP": "Please enter valid OTP",
 "LBL_PLEASE_ENTER_VALID_PASSWORD": "Please enter a valid password",
 "LBL_SUBMIT": "Submit",
 "LBL_ENTER_CODE": "Enter code",
 "LBL_ACQUIRE_CODE": "Acquire Code",
 "LBL_INVITATION_CODE": "Invitation Code",
 "LBL_PLEASE_ENTER_USERNAME": "Please enter username",
 "LBL_PLEASE_ENTER_CODE": "Please enter code",
 "LBL_PLEASE_ENTER_PASSWORD": "Please enter password",
 "LBL_PLEASE_ENTER_PHONE": "Please enter phone number",
 "LBL_PLEASE_RE_ENTER_PASSWORD": "Please re-enter password",
 "LBL_PASSWORD_MISMATCH": "Password not matched",
 "LBL_PLEASE_WAIT": "Please wait",
 "LBL_SKIP_PASSWORD": "Skip password",
 "LBL_CONFIRM_FINGERPRINT_TO_CONTINUE": "Confirm fingerprint to continue",
 "LBL_TOUCH_PHONE_SENSOR": "Touch the phone sensor",
 "LBL_TEST": "aa",
 "LBL_SERVICE_DEPOSIT": "Store Baggage 24 Hours",
 "LBL_SERVICE_TICKET": "Ticket Service",
 "LBL_SERVICE_WAKE": "Morning Call",
 "LBL_SERVICE_POSTS": "Postal Services",
 "LBL_SERVICE_FOODS": "Food Service Delivery",
 "LBL_SERVICE_BREAKFAST": "Breakfast",
 "LBL_GOOD": "Good",
 "LBL_YES": "YES",
 "LBL_NO": "No",
 "LBL_CANCEL": "Cancel",
 "LBL_DESC": "Description",
 "LBL_OK": "OK",
 "LBL_CITY": "City",
 "LBL_AMEN": "Amenities",
 "LBL_ADDITIONAL_INFO": "Additional information",
 "LBL_WRITE_COMMENT": "Write your comments",
 "LBL_POST_REVIEW": "Post Review",
 "LBL_REVIEW": "Please Review the hotel",
 "LBL_SELECT_PASSWORD": "Please select password",
 "LBL_PROCEED": "Proceed",
 "LBL_END_DATE": "End Date",
 "LBL_START_DATE": "Start Date",
 "LBL_INTERNATIONAL": "International",
 "LBL_CONFIRM": "Confirm",
 "LBL_DETAIL": "Detail",
 "LBL_CONTACT": "Contact",
 "LBL_NAME": "Name",
 "LBL_ROOMS": "# of Rooms",
 "LBL_NO_DATA_AVAILABLE": "There is no data available",
 "LBL_DOMESTIC": "Domestic",
 "LBL_SELECT_DATE": "Please select dates",
 "LBL_SELECT_DATE_ERROR": "You can not select the previous date.",
 "LBL_VERY_GOOD": "Very Good",
 "LBL_AVERAGE": "Average",
 "LBL_USER_COMMENTS": "User comments",
 "LBL_RESERVATION": "Reservation",
 "LBL_COMMENTS": "Comments",
 "LBL_CHECK_IN_OUT": "Check in-out",
 "LBL_LANGUAGE_SERVICE": "Language Service",
 "LBL_PET_ALLWANCE": "Pet allowance",
 "LBL_SALE": "Sale",
 "LBL_RESERVE": "Reserve",
 "LBL_RETURN": "Return",
 "LBL_DATES": "Dates",
 "LBL_NIGHT": "Night",
 "LBL_AMOUNT": "Amount",
 "LBL_DISCOUNT_STRING": "Discount reservation are not able to be cancelled or changed ",
 "LBL_PEAK_BOOKING_TEXT": "We’re currently experiencing peak booking volume.\r\nInstant payment is required to secure your\r\nreservation. Check-in will be starting at 10pm.\r\nWe will inquire your reservation and personal\r\ninformation are secured.",
 "LBL_REQUEST_SLIP": "Request a receipy from the front des",
 "LBL_WRITE_REVIEW": "Please write some reviews",
 "LBL_SEARCH": "Search",
 "LBL_PROFIT_GENERATING": "Profit Generating...",
 "LBL_LUCKY_POOL": "Lucky Pool",
 "LBL_SELECT_CITY": "Select City",
 "LBL_SELECT_HINT_TEXT": "Keywords/Hotel/Address",
 "LBL_CURRENT_CITY": "Current City",
 "LBL_HISTORY": "History",
 "LBL_POPULAR_CITY": "Popular City",
 "LBL_WELCOME_TO_APP": "Welcome to Homebloc~",
 "LBL_A_SECOND": "Just a second......\r\nPlease confirm your phone number\r\n",
 "LBL_SEARCH_HOTEL": "Search Hotel",
 "LBL_HOTEL": "Hotel",
 "LBL_BIG_BED": "Big Bed",
 "LBL_DOUBLE_BED": "Double Bed",
 "LBL_SINGLE_BED": "Single Bed",
 "LBL_MULTIPLE_BED": "Multiple Beds",
 "LBL_HOTEL_FEATURED": "Featured List",
 "LBL_SEARCH_RESULT": "Search Result",
 "LBL_RECOMENDED": "Recomended",
 "LBL_PRICE_LOW_TO_HIGH": "Price(Low to high)",
 "LBL_PRICE_HIGH_TO_LOW": "Price(High to Low)",
 "LBL_PRICE_HIGHLY_RATED": "Highly Rated",
 "LBL_PRICE_NEARBY": "All Nearby",
 "LBL_SUGGESTED": "Suggested",
 "LBL_AREA": "Area",
 "LBL_PRICE": "Price",
 "LBL_ALL": "All",
 "LBL_PAYMENT_DETAIL": "Payment Method",
 "LBL_PAY_WITH_HOC": "Pay with HOC",
 "LBL_DEBIT_CARD_NO": "Debit / Credit Card Number",
 "LBL_ENTER_CARD_NO_XXX": "Input card number",
 "LBL_EXPIRY_DATE": "Expiry Date",
 "LBL_MONTH": "MM",
 "LBL_YEAR": "YY",
 "LBL_CVV_INFO": "CVV (last 3 digits on the back of the card, next to the signature line)",
 "LBL_CVV": "Input CVV",
 "LBL_ERROR_ROOM_COUNT": "Please enter number of rooms you want to book",
 "LBL_ERROR_USER_NAME": "Please enter your name before booking",
 "LBL_ERROR_PHONE_NO": "Please enter valid mobile number to book hotel",
 "LBL_INVALID_CARD_INFO": "Please insert valid card information",
 "LBL_BOOKING": "Booking",
 "LBL_CONFIRM_BOOKING_CHECK_INFO": "Please check if the information above is correct",
 "LBL_OVER_NIGHT": "Booking Overnight",
 "LBL_SIX_DIGIT_PASSCODE": "6 Digit PassCode",
 "LBL_PENDING": "Pending",
 "LBL_PROCESSING": "Processing",
 "LBL_ACCOUNT_SETTING": "Account Settings",
 "LBL_MY_RESERVATION": "My Reservation",
 "LBL_DAYS": "Days",
 "LBL_CHECK_IN": "Check-In",
 "LBL_CHECK_OUT": "Check-Out",
 "LBL_CONFIRMED_READY": "Confirmed, Ready to check in",
 "LBL_DURATION": "Duration",
 "LBL_MALE": "Male",
 "LBL_FEMALE": "Female",
 "LBL_PERSONAL_INFO": "Personal Info",
 "LBL_THUMB_PHOTO": "Thumbnail Photo",
 "LBL_NIKNAME": "NickName",
 "LBL_MY_INVITATION": "My Invitation",
 "LBL_GENDER": "Gender",
 "LBL_SOCIAL": "Social Networking",
 "LBL_AUTHENTICATION": "Authentication",
 "LBL_PASSCODE": "Passcode",
 "LBL_COMPLETED": "Completed",
 "LBL_DEVIDE_END": "Dividend",
 "LBL_COMMING_SOON": "(Coming Soon)",
 "LBL_ETH_ADDRESS": "Eth Address",
 "LBL_OFFICIAL_EVENTS": "Official Events",
 "LBL_INVITATION": "LBL_INVITATION",
 "LBL_EVENTS": "Events",
 "LBL_FEEDBACK_TO_APP": "Feedback to HOMEBLOC",
 "LBL_TAKE_IMAGE": "Take Image",
 "LBL_CHOOSE_FROM_GALLERY": "Choose Image from Gallery",
 "LBL_SOCAIL_EVENTS": "Social Events",
 "LBL_OFFICIAL_ACCOUNT": "Official Account",
 "LBL_OFFICIAL_ACCOUNT_INFO": "Official Account enjoy our HOM-candies giveway and other fun activities",
 "LBL_SCAN_QR_TEXT": "Scan the QR code and get in touch with our 24 hours customer service",
 "LBL_OFFICIAL_WE_CHAT_GROUP": "Offical We Chat Group\r\n",
 "LBL_FEEDBACK": "Feedback",
 "LBL_COMMENT_FOR_REWARDS": "Comments for rewards",
 "LBL_SUBMITT": "Submit",
 "LBL_ABOUT_HOMEBLOC": "About Homeboc",
 "LBL_CREDIT": "Credit",
 "LBL_CREDIT_INFO": "All Hotels’ Credentials and customers’ Identi-\r\nties are  Verified. With Simplified  booking.\r\nProcess and frequently updated  photos.\r\nMaking sure customers get what they see.",
 "LBL_VALUE": "Value",
 "LBL_VALUE_INFO": "By acquiring him credit, users are granted\r\nWith a so called  user-investor status.",
 "LBL_DEVIDE_END_INFO": "Hom rewarding goes  to both hotels and \r\nusers Upon completing  service .Value of hom\r\ngrowsas the amount  of customers and \r\ncooperational Partner(hotels) grows.",
 "LBL_CUSTOMER_SERVICE": "Customer Service：400 8097 2238",
 "LBL_BEFORE": "Before",
 "LBL_CUSTOMER_NAME": "Customer Name",
 "LBL_SOURCE": "Source",
 "LBL_NOTICE": "Notice",
 "LBL_NOTICE_TEXT": "IN CASE YOU PICKED AN INVALID RESERVATION, A\r\nFULL REFUND ALONG WITH A FULL COMPENSATION\r\nWILL BE GRANTED 5 DAYS AFTER COMPLETION.",
 "LBL_COUNTER": "Counter",
 "LBL_BUY_FROM_LUCKY_POOL": "Buy from Lucky Poll",
 "LBL_PURCHASE": "Purchase",
 "LBL_EMAIL": "Email",
 "LBL_ERROR_EMAIL": "Please Enter your email"
 }
 }
*/
