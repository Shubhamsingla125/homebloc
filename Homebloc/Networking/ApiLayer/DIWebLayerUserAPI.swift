//
//  DIWebLayerUserAPI.swift
//  BaseProject
//
//  Created by TpSingh on 05/04/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import UIKit

class DIWebLayerUserAPI: DIWebLayer {
    
    
    
    func registation (parameters: Parameters?, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "signup", success: { dataResponse in
            if let message = dataResponse["message"] as? String {
                
                success(message)
                
                return
            }
            failure(DIError.unKnowError())
            
        }) {
            failure($0) }
    }
    
    
    func getVerificationCode (parameters: Parameters?,mobile:String,countryCode:String, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let params = [
            "mobile" : mobile,
            "countryCode": countryCode
        ]
        print(params)
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Sendsms&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if let message = dataResponse["responseMessage"] as? String{
                success(message)
                return
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    
    func signUp (parameters: Parameters?,mobile:String,countryCode:String,password:String,verifyCode:String, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let params = [
            "mobile":mobile,
            "password":password,
            "countryCode":countryCode,
            "verifyCode":verifyCode
        ]
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Register&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if let message = dataResponse["responseMessage"] as? String{
                success(message)
                return
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func login (parameters: Parameters?,mobile:String,countryCode:String,password:String,verifyCode:String, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let params = [
            "staffAccount":mobile,
            "password":password,
            "countryCode":countryCode,
            "verifyCode":verifyCode
        ]
        
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Login&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if let token = dataResponse["token"] as? String{
                let user = User()
                user.oauthToken = token
                UserManager.saveCurrentUser(user: user)
            }
            if let message = dataResponse["responseMessage"] as? String{
                success(message)
                return
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func forgotPassword (lang:String,parameters: Parameters?,mobile:String,countryCode:String,password:String,verifyCode:String, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
       
        let params = [
        "staffAccount":mobile,
        "md5passWord":password,
        "countryCode":countryCode,
        "verifyCode":verifyCode,
        "lang":lang
        ]
        
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=ResetPassword&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if let message = dataResponse["responseMessage"] as? String{
                success(message)
                return
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func getCities (parameters: Parameters?,language:String, success: @escaping (_ cityList: [City]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=GetCity&m=zh_jdgjb&lang=\(language)"
        self.webService(httpMethod:.get,  parameters: parameters, functionName: url, success: { dataResponse in
            if let response = dataResponse["response"] as? NSArray{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let cityList = try JSONDecoder().decode([City].self, from: jsonData)
                    success(cityList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func getLanguages (parameters: Parameters?, success: @escaping (_ languageList: [Language]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Languages&m=zh_jdgjb"
        self.webService(httpMethod:.get,  parameters: parameters, functionName: url, success: { dataResponse in
            if let languages = dataResponse["languages"] as? NSArray {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: languages, options: .prettyPrinted)
                    let locationList = try JSONDecoder().decode([Language].self, from: jsonData)
                    success(locationList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func getLanguageLabel (parameters: Parameters?,language:Language, success: @escaping (_ languageLabels: [String:Any]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Languagelabels&m=zh_jdgjb"
        
        let params = [
            "lang":language.code ?? ""
        ]
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if let languages = dataResponse["languages"] as? [String:Any]{
                success(languages)
                return
            } else {
                success(nil)
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func resellHotel(parameters: Parameters?, success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=ResellHotel&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
           success(dataResponse)
        }) {
            failure($0) }
    }
    
    
    func getHotelTags (parameters: Parameters?,language:String,city:City, success: @escaping (_ hotelTag: [HotelTag]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=ChoicenessJd&m=zh_jdgjb&city=\(city.citycode ?? "")"
        let params = ["lang":language]
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            print(dataResponse)
            if let response = dataResponse["response"] as? NSArray{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let hotelTagList = try JSONDecoder().decode([HotelTag].self, from: jsonData)
                    success(hotelTagList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func submitFeedback(parameters: Parameters?,language:String,feedback:String, success: @escaping (_ response:Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Feedback&m=zh_jdgjb"
        let params = [
            "lang":language,
            "token":User.sharedInstance.oauthToken ?? "",
            "searchDate":feedback
            ]
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            print(dataResponse)
            success(dataResponse)
            return
        }) {
            failure($0) }
    }
    
    func getLuckyPoolHotelList(parameters: Parameters?,language:String,searchKey:String,city:City?,startDate:String,endDate:String, success: @escaping (_ hotels: [Hotel]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        let url = "http://18.191.61.5/app/index.php?&i=2&c=entry&a=bzapp&do=LuckyPool&m=zh_jdgjb&searchCity=\(city?.citycode ?? "")&searchKey=\(searchKey)&startTime=\(startDate)&endTime=\(endDate)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let params = [
            "lang":language,
            ]
        self.webService(httpMethod:.post,  parameters: params, functionName: url ?? "", success: { dataResponse in
            print(dataResponse)
            if let response = dataResponse["response"] as? NSArray{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let hotelList = try JSONDecoder().decode([Hotel].self, from: jsonData)
                    success(hotelList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    func getHotelList(parameters: Parameters?,language:String,searchKey:String,city:City?,startDate:String,endDate:String,orderBy:HotelSuggestion?,star:Int=0,minPrice:Int=0,maxPrice:Int=1000, success: @escaping (_ hotels: [Hotel]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        let orderValue = orderBy != nil ? "\(orderBy?.rawValue ?? 0 + 1)" : ""
        let url = "http://18.191.61.5/app/index.php?&i=2&c=entry&a=bzapp&do=JdList&m=zh_jdgjb&lang=\(language)&searchCity=\(city?.citycode ?? "")&searchKey=\(searchKey)&startTime=\(startDate)&endTime=\(endDate)&orderBy=\(orderValue)&lat=&lng=&star=\(star)&min_price=\(minPrice)&max_price=\(maxPrice)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let params = [
            "lang":language
        ]
        
        self.webService(httpMethod:.post,  parameters: params, functionName: url ?? "", success: { dataResponse in
            print(dataResponse)
            if let response = dataResponse["response"] as? NSArray{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let hotelList = try JSONDecoder().decode([Hotel].self, from: jsonData)
                    success(hotelList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    
    func getHotelListByTag(parameters: Parameters?,tagId:String,language:String,searchKey:String,city:City?,startDate:String,endDate:String,orderBy:HotelSuggestion?,star:Int=0,minPrice:Int=0,maxPrice:Int=1000, success: @escaping (_ hotels: [Hotel]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        //        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=ChoicenessJdList&m=zh_jdgjb"
        
        let url = "http://18.191.61.5/app/index.php?&i=2&c=entry&a=bzapp&do=ChoicenessJdList&m=zh_jdgjb&city=\(city?.citycode ?? "")&tag_id=\(tagId)"
        
        let params = ["lang":language]
        
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if var response = dataResponse["response"] as? NSArray{
                var array : [Parameters] = []
                for value in response{
                    if var dict = value as? Parameters{
                        dict["services_list"] = []
                           array.append(dict)
                    }
                }
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: array, options: .prettyPrinted)
                    let hotelList = try JSONDecoder().decode([Hotel].self, from: jsonData)
                    success(hotelList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    func removeNullFromDict (dict : [String:Any]) -> [String:Any]{
        var dic = dict;
        for (key, value) in dict {
            let val : NSObject = value as! NSObject;
            if(val.isEqual(NSNull())) {
                dic[key] = ""
            } else {
                dic[key] = value
            }
        }
        return dic;
    }
    
    func getCommentList(parameters: Parameters?,language:String,city:City?,hotel:Hotel, success: @escaping (_ commentList: [CommentList]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let params = [
            "lang":language,
            ]
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=GetAssess&m=zh_jdgjb&hotelId=\(hotel.hotelId ?? "")"
        self.webService(httpMethod:.post,  parameters: params, functionName: url, success: { dataResponse in
            if let commentList = dataResponse["commentList"]as? NSArray{
                var commentNewArray : [[String:Any]] = [[String:Any]]()
                print(commentList)
                for value in commentList{
                    if let dict = value as? [String:Any] {
                        let newdict = self.removeNullFromDict(dict: dict);
                       commentNewArray.append(newdict)
                    }
                }
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: commentNewArray, options: .prettyPrinted)
                    let commentList = try JSONDecoder().decode([CommentList].self, from: jsonData)
                    success(commentList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
            success(nil)
            return
        }) {
            failure($0) }
    }
    
    
    func verifyPasscode(parameters: Parameters?, success: @escaping (_ response: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=VerifyPassword&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            success("")
            return
            failure(DIError.unKnowError())
            return
            
        }) {
            failure($0) }
    }
    
    func addOrder(parameters: Parameters?, success: @escaping (_ response: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=AddOrder&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            if let orderId = dataResponse["order_id"] as? String {
                
                success(orderId)
                return
            }
            failure(DIError.unKnowError())
                return
           
        }) {
            failure($0) }
    }
    
    func stripeCharge(parameters: Parameters?, success: @escaping (_ HotComment: HotComment?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=stripecharge&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                let hotCommentList = try JSONDecoder().decode(HotComment.self, from: jsonData)
                success(hotCommentList)
                return
            } catch(let error){
                print("parsing error----->\(error)")
                success(nil)
                return
            }
        }) {
            failure($0) }
    }
    
    func getHotelDisc(parameters: Parameters?,hotelId:String,success: @escaping (_ hotelDesc: HotelDesc?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=GetHotelDesc&m=zh_jdgjb&hotelId=\(hotelId)"
        
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                let hotCommentList = try JSONDecoder().decode(HotelDesc.self, from: jsonData)
                success(hotCommentList)
                return
            } catch(let error){
                print("parsing error----->\(error)")
                success(nil)
                return
            }
        }) {
            failure($0) }
    }
    
    func getHotelReservationDetail(parameters: Parameters?,language:String,hotel:Hotel,city:City?,startDate:String,endDate:String, success: @escaping (_ hotalDetailt: HotelDetail?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=JdDetails&m=zh_jdgjb&&lang=\(language)&hotelId=\(hotel.hotelId ?? "")&searchDate%5BstartTime%5D=\(startDate)&searchDate%5BendTime%5D=\(endDate)"
        self.webService(httpMethod:.get,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                let hotelDetail = try JSONDecoder().decode(HotelDetail.self, from: jsonData)
                success(hotelDetail)
                return
            } catch(let error){
                print("parsing error----->\(error)")
                success(nil)
                return
            }
        }) {
            failure($0) }
    }
    
    func markFvtHotel(parameters: Parameters?, success: @escaping (_ HotComment: HotComment?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Collection&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
           success(nil)
            return
        }) {
            failure($0) }
    }
    
    func rateHotel(parameters: Parameters?, success: @escaping (_ HotComment: HotComment?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=SaveAssess&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                let hotCommentList = try JSONDecoder().decode(HotComment.self, from: jsonData)
                success(hotCommentList)
                return
            } catch(let error){
                print("parsing error----->\(error)")
                success(nil)
                return
            }
        }) {
            failure($0) }
    }
    
    func rateComment(parameters: Parameters?, success: @escaping (_ HotComment: HotComment?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=DianAssess&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            success(nil)
            return
        }) {
            failure($0) }
    }
    
    func getOrderList(parameters: Parameters?,page:Int, success: @escaping (_ orderList: [Order]?) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=MyOrder&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            if let response = dataResponse["response"] as? NSArray{
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let orderList = try JSONDecoder().decode([Order].self, from: jsonData)
                    success(orderList)
                    return
                } catch(let error){
                    print("parsing error----->\(error)")
                    success(nil)
                    return
                }
            }
        }) {
            failure($0)
        }
    }
    
    func getUserProfile (parameters: Parameters?, success: @escaping (_ userInfo: UserInfo?) -> (), failure: @escaping (_ error: DIError) -> ()) {
         let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=GetUser&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dataResponse, options: .prettyPrinted)
                let userInfo = try JSONDecoder().decode(UserInfo.self, from: jsonData)
                success(userInfo)
                return
            } catch(let error){
                print("parsing error----->\(error)")
                success(nil)
                return
            }
            
        }) {
            failure($0) }
    }
    
    func uploadProfilePic (parameters: Parameters?,image:UIImage, success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=Upload&m=zh_jdgjb"
        self.mutipart(parameters: parameters, image: image, key: "upfile", fileName: "\(User.sharedInstance.oauthToken ?? "").jpg", functionName: url, success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    func updateUserProfile (parameters: Parameters?, success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        let url = "http://18.191.61.5/app/index.php?i=2&c=entry&a=bzapp&do=EditUser&m=zh_jdgjb"
        self.webService(httpMethod:.post,  parameters: parameters, functionName: url, success: { dataResponse in
            print(dataResponse)
            if let message = dataResponse["responseMessage"] as? String {
                success(message)
            }
            failure(DIError.unKnowError())
        }) {
            failure($0) }
    }
    
    
    
    func forgotPassword(parameters: Parameters?,success: @escaping (_ message: String) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        self.webService(parameters: parameters, functionName: "forgot_password/otp_code", success: { response in
            print(response)
            
            if let message = response["message"] as? String {
                success(message)
            }
            failure(DIError.unKnowError())
            
            
        }) {
            failure($0) }
        
    }
    
    
   /* func uploadProfilePic(image: UIImage, parameters: Parameters?, success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) -> Void {
        
        self.mutipart(parameters: parameters, image: image, key: "image", fileName: "image.jpeg", functionName: "profile/updateProfileImage", success: { (response) in
            print(response)
            success(response)
        }) {
            failure($0)
        }
    }*/
    
    
    
}
