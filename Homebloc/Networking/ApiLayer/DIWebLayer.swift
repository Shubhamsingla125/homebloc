//
//  DIWebLayer.swift
//  BaseProject
//
//  Created by Aj Mehra on 08/03/17.
//  Copyright © 2017 Debut infotech. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class DIWebLayer {
    
    let webManager = APIHandler()
    
    func webService (httpMethod: Alamofire.HTTPMethod = .post,  parameters: Parameters?, functionName: String, success: @escaping (_ responseValue: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        
        webManager.post(apiMethod: httpMethod, parameters: parameters, functionName: functionName, success: { (data) in
            self.handleResponseData(data: data, success: {
                success($0)
            }, failure: {
                failure($0)
            })
        }) {
            failure($0)
        }
    }
    
    func mutipart(parameters: Parameters?, image: UIImage, key: String, fileName: String, functionName: String, success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        webManager.multipart(parameters: parameters, image: image, key: key, fileName: fileName, functionName: functionName, success: { (data) in
            self.handleResponseData(data: data, success: {
                success($0)
            }, failure: {
                failure($0)
            })
        }) {
            failure($0)
        }
    }
    
    func handleResponseData(data: Data?, success: @escaping (_ response: Response) -> (), failure: @escaping (_ error: DIError) -> ()) {
        
        guard let responseData = data else {
            failure(DIError.invalidData())
            return
        }
        do {
            let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments)
            if let responseArray = json as? NSArray  {
                var dict = [String:Any]()
                dict["response"] = responseArray
                success(dict)
                return
            }
            guard let responseDict = json as? [String: AnyObject] else {
                failure(DIError.invalidData())
                return
            }
            if (responseDict["services"] as? NSArray) != nil{
                success(responseDict)
                return
            }
            if (responseDict["commentList"] as? NSArray) != nil{
                success(responseDict)
                return
            }
            if (responseDict["hotelRoomList"] as? NSArray) != nil{
                success(responseDict)
                return
            }
            if (responseDict["mobile"] as? String) != nil{
                success(responseDict)
                return
            }
            guard let status = responseDict["responseData"] as? Int else {
                failure(DIError.missingKey())
                return
            }
            
            if status == 1 || status == 3 || status == 2{
                success(responseDict)
                /*if let dataArray = responseDict["data"] as? NSArray, dataArray.count > 0 {
                 if let reponseDict = dataArray.firstObject as? [String: AnyObject] {
                 success(reponseDict)
                 if let token = responseDict["token"] as? String {
                 UserManager.saveToken(token: token)
                 }
                 } else {
                 print("Response invalid format.")
                 
                 }
                 } else {
                 print("Data key not found.")
                 failure(DIError.missingKey())
                 }*/
            } else {
                print("Server Failure.") //Server Failure will handle here
                if let message = responseDict["responseMessage"] as? String{
                    failure(DIError.serverResponse(message: message))
                    return
                }
                failure(DIError.missingKey())
            }
        } catch {
            failure(DIError.invalidJSON())
            return
        }
    }
}
