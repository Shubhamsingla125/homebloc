////
////  DiSessionManger.swift
////  BaseProject
////
////  Created by Aj Mehra on 08/03/17.
////  Copyright © 2017 Debut Infotech. All rights reserved.
////
//
//import UIKit
//import Foundation
//import Alamofire
//
////enum HTTPMethods: String {
////    case post = "POST"
////    case get = "GET"
////    case patch = "PATCH"
////    case put = "PUT"
////}
//
//enum HttpContentType: String {
//    case urlecncoded = "application/x-www-form-urlencoded"
//    case json = "application/json"
//    case multipart = "multipart/form-data; boundary="
//}
//
//typealias Parameters = [String: Any]
//typealias Response = [String: Any]
//
///// This Enum is used to assign the base Url for different stagging of the application like whether application is under development or QA or production
//// dev :- Development Environment
//// qa :- For QA
//// live :- Live URL for App Store
//// beta :- Client Testing
//enum BaseUrl: String {
//    case dev
//    case qa
//    case beta
//    case live
//
//    var version: String {
//        switch self {
//        case .dev: return ""
//        case .qa: return ""
//        case .beta: return ""
//        case .live: return ""
//        }
//    }
//    var url: String {
//        switch self {
//        case .dev: return "http://js-server.debutinfotech.com:3048"
//        case .qa: return "http://112.196.27.243:3006"
//        case .beta: return "http://112.196.27.243:3006"
//        case .live: return "http://188.166.243.24:3027"
//        }
//    }
//}
//
//enum APICallBacks {
//    case success, failure
//}
//
//struct DIError {
//    var title: String?
//    var message: String?
//    var code: DIErrorCode?
//    var icon: UIImage?
//
//    static func unKnowError() -> DIError {
//        return DIError(title: "", message: "Unable to find error.", code: .unknown, icon: nil)
//    }
//    static func invalidUrl () -> DIError {
//        return DIError(title: "Invalid url", message: "Url is invalid.", code: .invalidUrl, icon: nil)
//    }
//    static func nilData () -> DIError {
//        return DIError(title: "", message: "Data not found.", code: .nilData, icon: nil)
//    }
//    static func invalidJSON () -> DIError {
//        return DIError(title: "", message: "Invalid JSON response.", code: .invalidJSON, icon: nil)
//    }
//    static func missingKey () -> DIError {
//        return DIError(title: "", message: "Missing key for dictionary or array.", code: .missingKey, icon: nil)
//    }
//    static func invalidData () -> DIError {
//        return DIError(title: "", message: "Invalid data.", code: .invalidData, icon: nil)
//    }
//    static func serverResponse (message: String) -> DIError {
//        return DIError(title: "", message: message, code: .invalidData, icon: nil)
//    }
//    static func serverResponseError (error: Error) -> DIError {
//        return DIError(title: "Response Error", message: error.localizedDescription, code: .invalidData, icon: nil)
//    }
//    static func invalidAppInfoDictionary () -> DIError {
//        return DIError(title: "No Info Dictionary", message: "Unable to find info dictionary for application.", code: .invalidAppInfoDict, icon: nil)
//    }
//}
//
//enum DIErrorCode {
//    case unknown, invalidUrl, nilData, invalidJSON, missingKey, invalidData, invalidAppInfoDict
//}
//
//class DISessionManager {
//
//    //Envirnoment Change
//    var baseURL: BaseUrl {
//        return .dev
//    }
//
//
//    func post( apiMethod: Alamofire.HTTPMethod = .post, parameters: Parameters?, functionName: String, success: @escaping (_ responseValue: Data) -> (), failure: @escaping (_ error: DIError) -> ()) {
//
//        var header = [String: String]()
//
//        let path = absolutePath(forApi: functionName)
//
//        if UserManager.isUserLoggedIn() {
//            let token = User.sharedInstance.oauthToken
//            header["token"] = token
//        }
//
//        Alamofire.request(path, method: apiMethod, parameters: parameters, encoding: JSONEncoding.default, headers: header)
//            .responseJSON { result in
//
//                if let errorResponse = result.error {
//                    print(errorResponse.localizedDescription)
//                    failure(self.parseError(error: errorResponse))
//                } else if let httpResponse = result.response {
//                    if httpResponse.statusCode == 200 {
//                        if let data = result.data {
//                            success(data)
//                            return
//                        }
//                    }
//                }
//                failure(DIError.nilData())
//        }
//    }
//
//
//    func multipart(parameters: Parameters?, image: UIImage, key: String, fileName: String, functionName: String, success: @escaping (_ responseData: Data?) -> (), failure: @escaping (_ error: DIError) -> ()) {
//
//        let parameters = [
//            "file_name": "swift_file.jpeg"
//        ]
//
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            multipartFormData.append(UIImageJPEGRepresentation(image, 1)!, withName: "photo_path", fileName: fileName, mimeType: "image/jpeg")
//            for (key, value) in parameters {
//                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//            }
//        }, to:"http://sample.com/upload_img.php")
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    //Print progress
//                })
//
//                upload.responseJSON { response in
//                    //print response.result
//                }
//
//            case .failure(_): break
//                //print encodingError.description
//            }
//        }
//    }
//
//
//    /// Create boundary string for multipart/form-data request
//    ///
//    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
//
//    func generateBoundaryString() -> String {
//        return "Boundary-\(NSUUID().uuidString)"
//    }
//
//    /// This method will convert the parameters into request data
//    ///
//    /// - Parameter parameters: list of parameter required by Api
//    /// - Returns: json data
//    func createRequestData(parameters: Parameters?) -> Data? {
//        if parameters != nil {
//            do {
//                let jsonData = try JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
//                return jsonData
//            } catch {
//                return nil
//            }
//
//        }
//
//        return nil
//    }
//
//    /// This method will create the complete path for the api request with the help of base url, version and domain Name
//    ///
//    /// - Parameter apiName: Name of api domin like login
//    /// - Returns: complete Url String
//    func absolutePath(forApi apiName: String) -> String {
//        var path = baseURL.url
//        let version = baseURL.version
//        if !path.hasSuffix("/") {
//            path += "/"
//        }
//        if version.isEmpty {
//            return path + apiName
//        }
//        if !version.hasSuffix("/") {
//            path += version + "/"
//        } else {
//            path += version
//        }
//        return path + apiName
//    }
//
//    /// This method will parse the error catch through the Api session manager and return an object of DIError with details about the error for debugging purpose
//    ///
//    /// - Parameter error: error catch by session manager
//    /// - Returns: Convert Object to display error to user
//    func parseError(error: Error) -> DIError {
//        return DIError(title: "", message: "Something Went Wrong.", code: .invalidUrl, icon: nil)
//    }
//
//
//}
//
//extension Data {
//
//    /// Append string to NSMutableData
//    ///
//    /// Rather than littering my code with calls to `dataUsingEncoding` to convert strings to NSData, and then add that data to the NSMutableData, this wraps it in a nice convenient little extension to NSMutableData. This converts using UTF-8.
//    ///
//    /// - parameter string:       The string to be added to the `NSMutableData`.
//
//    mutating func append(_ string: String) {
//        if let data = string.data(using: .utf8) {
//            append(data)
//        }
//    }
//}

