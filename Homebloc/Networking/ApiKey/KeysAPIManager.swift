//
//  APIManager.swift
//  BaseProject
//
//  Created by narinder on 01/03/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation

struct Login {
    var facebookId: String = ""
    var snsType : Int = 0
    var snsId: String = ""
    var email: String = ""
    var password: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var dateOfBirth:String = ""
    var profilePicure:String = ""
    var gender:String = ""
    func getDictionary() -> Parameters {
        let dict:[String: Any] = ["email": email,
                                  "sns_type": 1,
                                  "password": password]
        /*let dict:[String: Any] = ["phone": self.userName,
         "password": self.password]*/
        
        //let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        return dict//jsonData
        
    }
    func getDictionaryForSocialType() -> [String: Any] {
        let dict:[String: Any] = ["email": email,
                                  "sns_type": snsType,
                                  "first_name": firstName,
                                  "last_name": lastName,
                                  "sns_id": snsId,
                                  "profile_pic": profilePicure]
        return dict
        
    }
    
}


struct Registration {
    var firstName: String = ""
    var lastName : String = ""
    var email: String = ""
    var password: String = ""
    var profilePicUrl: String = ""
    var firebaseProfileImageName: String = ""
    var dateOfBirth: String = ""
    var gender:Int?
    func getDictionary() -> [String: Any]? {
        
        var dict:[String: Any] = ["first_name": firstName,
                                  "last_name": lastName,
                                  "email": email,
                                  "password": password,
                                  ]
        
        if profilePicUrl != "" {
            dict["profile_pic"] = profilePicUrl
        }
        if firebaseProfileImageName != "" {
            dict["firebaseProfileImageName"] = firebaseProfileImageName
        }
        
        
        //let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        return dict//jsonData
    }
}

struct UpdateProfile {
    
    var firstName: String = ""
    var lastName : String = ""
    var email: String = ""
    var password: String = ""
    var profilePicUrl: String = ""
    var firebaseProfileImageName: String = ""
    var dateOfBirth: String = ""
    var gender = "FEMALE"
    
    func getDictionary() -> [String: Any]? {
        
        var dict = [String: Any]()
        if firstName != "" {
            dict["first_name"] = firstName
        }
        if lastName != "" {
            dict["last_name"] = lastName
        }
        if email != "" {
            dict["email"] = email
        }
        if password != "" {
            dict["password"] = password
        }
        if profilePicUrl != "" {
            dict["profile_pic"] = profilePicUrl
        }
        if firebaseProfileImageName != "" {
            dict["firebaseProfileImageName"] = firebaseProfileImageName
        }
        if dateOfBirth != ""{
            dict["dob"] = dateOfBirth
        }
        if gender != ""{
            dict["gender"] = gender
        }
        
        print(dict)
        
        //let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        return dict//jsonData
    }
}




struct ForgotPasswordKey {
  var email: String = ""  
  func getDictionary() -> [String: Any]? {
    let dict:[String: Any] = ["email": email]
    
    //let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
    return dict//jsonData
    
  }
  
}


