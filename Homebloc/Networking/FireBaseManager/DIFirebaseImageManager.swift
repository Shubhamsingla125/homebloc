
//
//  DIFirebaseImageManager.swift
//  Sparks
//
//  Created by narinder on 10/01/17.
//  Copyright © 2017 MAC. All rights reserved.
//

var userData = "userData"

import UIKit
import Foundation
import FirebaseStorage
import FirebaseAuth

typealias FirImageUploaderCallback = ((NSMutableDictionary?) -> Void)
typealias FirImageDeleteCallback = ((Bool?) -> Void)
typealias isSavedLocally = ((Bool?) -> Void)


@objc protocol FIRImageManagerDeletgate {
  func didDeleteImage(success:Bool, atIndex:Int)
  func didSavedlocally(sucess:Bool)
}

@objc class DIFirebaseImageManager: NSObject{
  
  var storageRef: FIRStorageReference!
  internal var callback: FirImageUploaderCallback!
  internal var deleteImagecallback: FirImageDeleteCallback!
  
  var DIFIRdelegate: FIRImageManagerDeletgate?
  
  
  override init() {
    storageRef = FIRStorage.storage().reference()
  }
  
  
  func uploadImage(image: UIImage, withQuality value:CGFloat = 1.0, withName:String, completion:@escaping (_ apiResponse: APICallBacks, _ apiUrl: String?, _ error: DIError) -> ())
  {
    
    
    let imageData = UIImageJPEGRepresentation(image, value)
    
    let imagePath = withName
    
    //    if let userID = (UserDefaults.standard.value(forKey: userData) as? NSDictionary)?.value(forKey: "_id") as? String {
    //        print(userID)
    //        imagePath = userID + "/" + "UserPic_" + withName
    //    }
    
    let metadata = FIRStorageMetadata()
    metadata.contentType = "image/png"
    metadata.customMetadata =  ["index": String(describing: index), "contentType": "image/png"]
    
    // Upload file and metadata to the object 'images/mountains.jpg'
    let uploadTask = storageRef.child(imagePath).put(imageData!, metadata: metadata)
    
    // Listen for state changes, errors, and completion of the upload.
    uploadTask.observe(.resume) { snapshot in
      // Upload resumed, also fires when the upload starts
    }
    
    uploadTask.observe(.pause) { snapshot in
      // Upload paused
    }
    
    uploadTask.observe(.progress) { snapshot in
      // Upload reported progress
      let percentComplete = 100.0 * Double(snapshot.progress!.completedUnitCount)
        / Double(snapshot.progress!.totalUnitCount)
      print(percentComplete)
    }
    
    uploadTask.observe(.success) { snapshot in
      // Upload completed successfully
      //Download the the image from url and save it as Data in local directory
      
      completion(.success,(snapshot.metadata?.downloadURL()?.absoluteString), DIError.nilData())
      
      // self.startDownloading(downloadUrl: (snapshot.metadata?.downloadURL()?.absoluteString)!, imageName: imagePath)
    }
    
    uploadTask.observe(.failure) { snapshot in
      if let error = snapshot.error as NSError? {
        completion(.failure, nil, DIError.serverResponseError(error: error))
        
        switch (FIRStorageErrorCode(rawValue: error.code)!) {
          
        case .objectNotFound:
          // File doesn't exist
          break
        case .unauthorized:
          // User doesn't have permission to access file
          break
        case .cancelled:
          // User canceled the upload
          break
          /* ... */
        case .unknown:
          // Unknown error occurred, inspect the server response
          break
        default:
          // A separate error occurred. This is a good place to retry the upload.
          break
        }
      }
    }
  }
  
  
  
  
  //MARK: Custom Functions
  func startDownloading(downloadUrl:String, imageName name:String)  {
    
    //    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
    //
    //      let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    //      let fileURL = documentsURL.appendingPathComponent(name + ".png")
    //      return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    //    }
    
    //    Alamofire.download(downloadUrl, to: destination).downloadProgress(closure: { (progress) in
    //
    //    }).response { response in
    //
    //      if response.error == nil, let LocalimagePath = response.destinationURL?.path {
    //
    //        print(LocalimagePath)
    //
    //        let imageObject = NSMutableDictionary()
    //        imageObject.setValue(downloadUrl, forKey: "FIRimageUrl")
    //        imageObject.setValue(LocalimagePath, forKey: "localImagePath")
    //        imageObject.setValue(name, forKey: "FIRimageName")
    //
    //        self.callback(imageObject)
    //
    //      }else{
    //        print(response.error!)
    //      }
    //    }
  }
  
  
  func removeFromServer(imageName name:String, atIndex:Int?,  completion : @escaping(Bool) -> ()) {
    
   // let imageName = name + ".png"
    //self.deleteImagecallback = reomverCallback
    
    // Create a reference to the file to delete
    let desertRef = storageRef.child(name)
    
    // Delete the file
    desertRef.delete { error in
      
      if let error = error {
        // Uh-oh, an error occurred!
        print(error.localizedDescription)
        // self.deleteImagecallback(false)
        if (error.localizedDescription .contains("does not exist"))
        {
         // self.deletFromDirectory(imageName: imageName, atIndex: atIndex)
        }else
        {
         // self.DIFIRdelegate?.didDeleteImage(success: false, atIndex: atIndex)
          
        }
        
        completion(false)

      } else {
        // File deleted successfully
        print("image deleted successfully")
        completion(true)
       //self.deletFromDirectory(imageName: imageName, atIndex: atIndex)
      }
    }
  }
  
  
  func deletFromDirectory(imageName: String, atIndex:Int ) {
    let imagePath = ""
    //    if let userID = (UserDefaults.standard.value(forKey: Constants.userdefaultKeys.userData) as? NSDictionary)?.value(forKey: "_id") as? String {
    //        print(userID)
    //        imagePath = userID
    //    }
    
    let fileManager = FileManager.default
    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let fileURL = documentsURL.appendingPathComponent(imagePath)
    print(fileURL.path)
    do {
      let filePaths = try fileManager.contentsOfDirectory(atPath: fileURL.path)
      print(filePaths)
      
      if filePaths.count == 0 {
        //if no image found in local directory just send a tag 101 in controller to remove all saved urls from _photoArray and reload collection.
        self.DIFIRdelegate?.didDeleteImage(success: false, atIndex: 101)
      }
      
      for filePath in filePaths {
        if imageName.contains(filePath)
        {
          try fileManager.removeItem(atPath: documentsURL.appendingPathComponent(imageName).path)
          // fire delegation with sucess bool
          self.DIFIRdelegate?.didDeleteImage(success: true, atIndex: atIndex)
        }
      }
    } catch let error as NSError {
      print("Could not clear temp folder: \(error.debugDescription)")
      self.DIFIRdelegate?.didDeleteImage(success: false, atIndex: atIndex)
    }
  }
  
  
  func deletFromDirectory() {
    
    var imagePath = ""
    if let userID = (UserDefaults.standard.value(forKey: userData) as? NSDictionary)?.value(forKey: "_id") as? String {
      print(userID)
      imagePath = userID
    }
    
    let fileManager = FileManager.default
    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let fileURL = documentsURL.appendingPathComponent(imagePath)
    print(fileURL.path)
    do {
      let filePaths = try fileManager.contentsOfDirectory(atPath: fileURL.path)
      print(filePaths)
      
      
      for _ in filePaths
      {
        try fileManager.removeItem(atPath: documentsURL.appendingPathComponent(fileURL.path).path)
        // fire delegation with sucess bool
      }
    } catch let error as NSError {
      print("Could not clear temp folder: \(error.debugDescription)")
    }
    
  }
  
  
  @objc func saveImageIfNeeded(downloadUrl:String, withName fileName:String)  {
    
    //        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
    //
    //            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    //            let fileURL = documentsURL.appendingPathComponent(fileName + ".png")
    //            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
    //        }
    //
    //        Alamofire.download(downloadUrl, to: destination).downloadProgress(closure: { (progress) in
    //
    //        }).response { response in
    //
    //            if response.error == nil, let LocalimagePath = response.destinationURL?.path {
    //
    //                print(LocalimagePath)
    //
    //                guard let shouldUpdate = self.DIFIRdelegate?.didSavedlocally(sucess: true) else {
    //                        return
    //                }
    //
    //
    //            }else{
    //                print(response.error!)
    //                guard let shouldUpdate = self.DIFIRdelegate?.didSavedlocally(sucess: false) else {
    //                    return
    //                }
    //            }
    //        }
  }
  
}


