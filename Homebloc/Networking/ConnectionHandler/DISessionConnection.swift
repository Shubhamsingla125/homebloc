//
//  DISessionConnection.swift
//  BaseProject
//
//  Created by narinder on 28/02/17.
//  Copyright © 2017 openkey. All rights reserved.
//

import Foundation

class DISessionConnection {
  var requestURL: URL
  var postData: Data
//  var requestMethod: HTTPMethod
  var headers: [String: String]
  var timeOutInterval: TimeInterval
  
  var session: URLSession = {
    let config = URLSessionConfiguration.default
    let session = URLSession(configuration: config)
    return session
  }()
  
  class func requestData(with url: URL) -> DISessionConnection {
    let objURLSession = DISessionConnection()
    objURLSession.requestURL = url
    return objURLSession
  }
  
  init() {
    self.postData = Data()
//    self.requestMethod = .post
    self.headers = ["Content-Type": "application/json"]
    self.timeOutInterval = 60
    self.requestURL = URL(string: "http://112.196.27.243:3006")!
  }
  
  func addRequestHeader(_ header: String, value: String) {
    self.headers[header] = value
  }
  
  func startAsyncProcessWithUploadTask(_ completionHandler: @escaping (_ dataResponse: URLResponse?,_ data: Data?, _ error: Error?) -> Void) {
    let request = createRequest()
    printRequest(request: request)
    let uploadTask = session.uploadTask(with: request, from: self.postData, completionHandler: { (data, response, error) in
      
      DispatchQueue.main.async(execute: { () -> Void in
        if error == nil {
          //self.printResponse(data: data!)
          completionHandler(response, data!, nil)
        } else {
          completionHandler(response,nil, error)
        }
      })
    })
    uploadTask.resume()
  }
  
  
  func createRequest() -> URLRequest {
    var request = URLRequest(url: self.requestURL)
//    request.httpMethod = self.requestMethod.rawValue
    request.timeoutInterval = TimeInterval(self.timeOutInterval)
    request.allHTTPHeaderFields = self.headers
    request.httpBody = self.postData
    request.httpShouldHandleCookies = false
    return request
  }
  
  func startAsyncProcessWithDataTask(success: @escaping (_ dataResponse: Data?) -> (), failure: @escaping (_ error: Error) -> ()) -> Void {
    let request = createRequest()
    let task = session.dataTask(with: request) { data, response, error in
      DispatchQueue.main.async(execute: { () -> Void in
        if let error = error {
          failure(error)
        } else {
          self.printResponse(data: data!)
          success(data)
        }
      })
    }
    task.resume()
  }
  
  
  func responseString(data: Data?) -> String? {
    if let hadData = data {
      return String(bytes: hadData, encoding: .utf8)
    }
    return nil
  }
  
  func printResponse(data:Data) {
    if let response = NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary {
      DILog.print(items: "Response from server =================>>>>>>>>")
      DILog.print(items: response)
    }
  }
  
  func printRequest(request:URLRequest) {
    DILog.print(items: "Request send to server =================>>>>>>>>")
    DILog.print(items: "URL: =================>>>>>>>>", request.url ?? "Invalid Url")
    DILog.print(items: "Headers: =================>>>>>>>>", request.allHTTPHeaderFields ?? "No Headers Found")
    //DILog.print(items: "Parameters: =================>>>>>>>>", responseString(data: self.postData)!)
    DILog.print(items: "=================================================")
  }
  
  
}
